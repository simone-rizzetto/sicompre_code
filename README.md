# README #

### What is this repository for? ###

* This is the source code of the SiComPre simulation. 
* Refer to branch 2.01


### SiComPre.exe parameters ###
These are the parameters to execute SiComPre:

* -f [file] = file name of the model
* -mc [integer] = maximum number of protein in a single subvolume
* -gw [integer] = width (measured as number of sub-volumes) of the simulation space (total number of sub-volumes=gw*gh)
* -gh [integer] = heigh (measured as number of sub-volumes) of the simulation space (total number of sub-volumes=gw*gh)
* -c [float] = write the simulation status every "c" simulation time
* -t [float] = maximum simulation time
* -ct [integer] = number of CPU threds used
* -noGPU = do not use GPU (CPU only)

### How do I set up SiComPre? ###

* This simulation can use both GPU+CPU or only CPU therefore to compile this code is necessary CUDA (version 5.0) 
* To run GPU+CPU is required an NVIDIA GPU with up-to-date drivers
* This repository contains a Visual Studio workspace that can be used for compilation in Windows
* To compile this code in Linux there is a separate Makefile

### Files ###

* The main file is "GPGMPComplex_compart.cu(h)" and contains core of the simulation (cuh is the extension for c files that contains CUDA api)
* Parser.cpp(.h) is the code to parse a SiComPre model
* queue.cpp(.h) contains an implementation of a queue necessary to perform some operation during the simulation
* The remaining files are obsolete

### What are the part of the code that are executed in parallel on the GPU or CPU? ###

* All the code executed on a GPU is indicated with "__global__" keyword in fron the function definiation.
* A GPU function (also called kernel) can be execude with GPU_function_name<<<number_of_processes,number_of_threads>>>(list of parameters)
* CPU threads are executed with native windows operation for WIN32 or with pthreads for linux.
* The right thread api is decided at compilation time with "#ifdef _WIN32" directives