
#ifndef COMPLEX_H
#define COMPLEX_H

#include <stdint.h>

//align debug
typedef struct {
    unsigned char r, g, b, a;
} RGBA8;
//align debug end

typedef struct {
	unsigned complex;
	unsigned short specie;
	unsigned short cell;
	unsigned id;
	//bool readyToDiffuse; 
	unsigned short readyToDiffuse; //edited for the new version
	unsigned short degree;
	unsigned short size;
} molecule;

struct molecule_gpu {
	unsigned short cell;
};

//struct site { //__align__(24)
//typedef struct ssite __align__(32) {
struct site{
	unsigned short specie;
	site* bound;
	short specieBounded; //errore intialized to 0, but 0 is actually a specie
	//struct  bound;
	unsigned mol; //id of mol
	unsigned short cell;
} ;

struct site_gpu{
	unsigned short Bounded; 
	unsigned short cell;
};

struct memo_gpu {
	unsigned molID;
	uint8_t val[2];
	//unsigned short val;
};

struct l_site {
	site* s;
	l_site* next;
};

struct rec_t {
	unsigned short cellID;
	unsigned int react;
};


#endif