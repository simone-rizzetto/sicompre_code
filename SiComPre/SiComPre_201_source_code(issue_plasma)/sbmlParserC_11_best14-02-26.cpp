
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sbml/SBMLTypes.h>
#include <string.h>
#include <sbml/SBMLReader.h>
#include <sbml/SBMLDocument.h>
#include <sbml/Model.h>
#include <sbml/UnitDefinition.h>
#include <sbml/units/UnitFormulaFormatter.h>
#include <sbml/units/FormulaUnitsData.h>
#include "sbmlParserC_compart.h"
#include "GPGMPComplex_compart.cuh"
//#define CELLS 8192
#define CELLS 4096
#define MAX_CONC 400

int getIDbyName(const char** lst, const char* s, int n) {
	for (int i=0; i<n; i++) {
		if (strcmp(s, lst[i])==0) return i;
	}
	return -1;
}

int SBML_getNumSpecies(char* filename) {
	SBMLDocument_t *d;
	Model_t *m;
	d = readSBML(filename);
	SBMLDocument_printErrors(d, stdout);
	m = SBMLDocument_getModel(d);
	if (m == NULL)
	{
		printf("No model present.");
		return -1;
	}
	int res = Model_getNumSpecies(m);
	SBMLDocument_free(d);
	return res;
}


int SBML_getNumSpeciesTypesTot(char* filename){
	SBMLDocument_t *d;
	Model_t *m;
	d = readSBML(filename);
	SBMLDocument_printErrors(d, stdout);
	m = SBMLDocument_getModel(d);
	if (m == NULL)
	{
		printf("No model present.");
		return -1;
	}
	unsigned int mol_count=0;
	  for (int i = 0; i < Model_getNumSpeciesTypes(m); i++)
	  {
		Parameter* par = Model_getParameter(m, i*2);
		SpeciesType* st;
		st=Model_getSpeciesType(m, i);
		
		//printf("Specie %s\n", Species_getDerivedUnitDefinition(s));
		par = Model_getParameter(m, i*2+1);
		mol_count+=Parameter_getValue(par);
		//printf("int %d %d\n", mol_count, i);
	  }
	unsigned int res = mol_count;
	//printf(" tot mole uns = %d\n", mol_count);
	SBMLDocument_free(d);
	return res;
}

int SBML_getNumSitesTot(char* filename){
	SBMLDocument_t *d;
	Model_t *m;
	d = readSBML(filename);
	SBMLDocument_printErrors(d, stdout);
	m = SBMLDocument_getModel(d);
	if (m == NULL)
	{
		printf("No model present.");
		return -1;
	}
	unsigned int mol_count=0;
	

	const char** Names;
	Names=(const char**)malloc(sizeof(const char*)*Model_getNumSpeciesTypes(m));
  for (int i = 0; i < Model_getNumSpeciesTypes(m); i++)
  {
	
    SpeciesType* st;
	st=Model_getSpeciesType(m, i);
	
	
	Names[i]=SpeciesType_getId(st);
  }

unsigned int site_count=0;
  for (int i = 0; i < Model_getNumSpecies(m); i++)
  {
    Species* s;
	s=Model_getSpecies(m, i);
	//ud=Species_getDerivedUnitDefinition(s);
	int mol=getIDbyName(Names, Species_getSpeciesType(s), Model_getNumSpeciesTypes(m));
	Parameter* par = Model_getParameter(m, mol*2);
	SpeciesType* st;
	st=Model_getSpeciesType(m, mol);
		
		//printf("Specie %s\n", Species_getDerivedUnitDefinition(s));
	par = Model_getParameter(m, mol*2+1);
	mol_count=Parameter_getValue(par);
	//for (int j=0; j<id_t; j++) count+=abd[j];
	for(int k=0; k<Species_getInitialAmount(s); k++) {
		site_count+=mol_count;
	}

	//count+=abd[getIDbyName(Names, Species_getSpeciesType(s), Model_getNumSpeciesTypes(m))];
  }
unsigned int res=site_count;


	//printf("tot_site uns = %d\n", res);
	SBMLDocument_free(d);
	return res;
}

int SBML_getNumSpeciesTypes(char* filename){
	SBMLDocument_t *d;
	Model_t *m;
	d = readSBML(filename);
	SBMLDocument_printErrors(d, stdout);
	m = SBMLDocument_getModel(d);
	if (m == NULL)
	{
		printf("No model present.");
		return -1;
	}
	
	int res = Model_getNumSpeciesTypes(m);
	SBMLDocument_free(d);

	return res;
}


int SBML_getNumReactions(char* filename) {
	SBMLDocument_t *d;
	Model_t *m;
	d = readSBML(filename);
	SBMLDocument_printErrors(d, stdout);
	m = SBMLDocument_getModel(d);
	if (m == NULL)
	{
		printf("No model present.");
		return -1;
	}
	int res = Model_getNumReactions(m);
	SBMLDocument_free(d);
	return res;
}

int parser(char* filename, site* site_state, molecule* molecule_state, int* v, float* c, float* cu, float* D, int cells, int rr, int ss, char** map, char** mapS, int source) {
	SBMLDocument_t *d;

	int conc=0;
	int conc_count=0;
	int errors=0;
	//filename = argv[1];
	Model_t *m;

	unsigned int level, version;


	d = readSBML(filename);
	//d = readSBMLFromFile("prova.xml");

	SBMLDocument_printErrors(d, stdout);

	m = SBMLDocument_getModel(d);

	level   = SBMLDocument_getLevel  (d);
	version = SBMLDocument_getVersion(d);

	printf("\n");
	printf("File: %s (Level %u, version %u)\n", filename, level, version);
/*
	if (m == NULL)
	{
	printf("No model present.");
	return 1;
	}


	
	d     = readSBML(filename);

	errors  = SBMLDocument_getNumErrors(d);
	errors += SBMLDocument_checkConsistency(d);

	printf( "\n" );
	printf( "        filename: %s\n" , filename     );
	printf( "        error(s): %u\n" , errors       );

	if (errors > 0) {
		SBMLDocument_printErrors(d, stdout);
		return -1;
	}
	d        = readSBML(filename);

	SBMLDocument_printErrors(d, stdout);

	m = SBMLDocument_getModel(d);


	printf("\n");
	printf("File: %s (Level %u, version %u)\n", filename, level, version);
*/
	if (m == NULL)
	{
	printf("No model present.");
	return 1;
	}

	printf("         ");
	printf("  model id: %s\n",  Model_isSetId(m) ? Model_getId(m) : "(empty)");
	printf( "   compartmentTypes: %d\n",  Model_getNumCompartmentTypes   (m) );
	printf( "        specieTypes: %d\n",  Model_getNumSpeciesTypes       (m) );
	printf( "       compartments: %d\n",  Model_getNumCompartments       (m) );
	printf( "            species: %d\n",  Model_getNumSpecies            (m) );
	printf( "         parameters: %d\n",  Model_getNumParameters         (m) );
	printf( "          reactions: %d\n",  Model_getNumReactions          (m) );
	printf( "\n" );

	

	int spectype=Model_getNumSpecies (m);
	//Species_t * sp;

	//UnitDefinition_t * ud;
	const char** Names;
	const char** NameSite;
	Names=(const char**)malloc(sizeof(const char*)*Model_getNumSpeciesTypes(m));
	NameSite=(const char**)malloc(sizeof(const char*)*Model_getNumSpecies(m));
  unsigned int i,j;
  int mol_count=0;
  int site_count=0;
  int* abd=(int*)malloc(sizeof(int)*Model_getNumSpeciesTypes(m));
  int mol_deb=0;
  for (i = 0; i < Model_getNumSpeciesTypes(m); i++)
  {
	Parameter* par = Model_getParameter(m, i*2);
	D[i]=Parameter_getValue(par);
    SpeciesType* st;
	st=Model_getSpeciesType(m, i);
	
	
	Names[i]=SpeciesType_getId(st);
	par = Model_getParameter(m, i*2+1);
	abd[i]=Parameter_getValue(par);
	mol_deb+=abd[i];
	conc=0;
	conc_count=0;
	
	for (int j=0; j<abd[i]; j++, mol_count++) {
		molecule_state[mol_count].id=mol_count;
		molecule_state[mol_count].complex=0;
		molecule_state[mol_count].specie=i;
		molecule_state[mol_count].degree=0;
		molecule_state[mol_count].size=1;
		//molecule_state[mol_count].readyToDiffuse=false;
		molecule_state[mol_count].readyToDiffuse=0; //edit for the new version
		//molecule_state[mol_count].cell=source+conc_count;
		//molecule_state[mol_count].cell=((double)rand()/(double)RAND_MAX)*(CELLS);
		molecule_state[mol_count].cell=((double)rand()/(double)RAND_MAX)*(CELLS-1);
		if (molecule_state[mol_count].cell>=CELLS) printf("error cell %d\n", molecule_state[mol_count].cell);
		if (conc>(Model_getNumSpeciesTypes(m)/CELLS+1)) {
			conc_count++;
			conc=0;
		}
		conc++;
		if (source+conc_count==CELLS) conc_count=0;
	}
	
  }

	int count=0;
	printf("debug2 %d %d\n", mol_count, mol_deb);
  for (i = 0; i < Model_getNumSpecies(m); i++)
  {
    Species* s;
	s=Model_getSpecies(m, i);
	//ud=Species_getDerivedUnitDefinition(s);
	NameSite[i]=Species_getId(s);
	count=0;
	int id_t=getIDbyName(Names, Species_getSpeciesType(s), Model_getNumSpeciesTypes(m));
	for (int j=0; j<id_t; j++) count+=abd[j];
	for(int k=0; k<Species_getInitialAmount(s); k++) {
		for (int j=0; j<abd[id_t]; j++, site_count++) {
			site_state[site_count].bound=NULL;
			site_state[site_count].mol=count+j;
			site_state[site_count].cell=molecule_state[count+j].cell;
			site_state[site_count].specie=i;
		}
	}

	//count+=abd[getIDbyName(Names, Species_getSpeciesType(s), Model_getNumSpeciesTypes(m))];
  }

  printf("debug site_count %d", site_count);
  for (i = 0; i < Model_getNumReactions(m); i++)
  {
    Reaction *r = Model_getReaction(m,i);
      
	
	KineticLaw_t *kl = Reaction_getKineticLaw(r);
	const ASTNode_t * ast = KineticLaw_getMath (kl);
	float cc=1;
	/*for (int k=0; k<KineticLaw_getNumParameters (kl); k++) {
		Parameter_t *par=KineticLaw_getParameter (kl, k);
		cc=cc*Parameter_getValue(par);
	}*/
	
	Parameter_t *par=KineticLaw_getParameter (kl, 0);
	c[i]=Parameter_getValue(par);
	
	par=KineticLaw_getParameter (kl, 1);
	cu[i]=Parameter_getValue(par);


    for (j = 0; j < Reaction_getNumReactants(r) && j<2; j++)
    {
      SpeciesReference_t *sr = Reaction_getReactant(r,j);

		v[i*2+j]=getIDbyName(NameSite, SpeciesReference_getSpecies(sr), spectype);
    }
  }
	
	printf("\n");
	//map[0]=(char*)malloc(sizeof(char)*strlen(Names[0]));
	//map[1]=(char*)malloc(sizeof(char)*strlen(Names[1]));
	//printf("size %d",sizeof(char)*strlen(Names[2]));
	//char* prova;
	//prova=(char*)malloc(6);
	//map[2]=(char*)malloc(sizeof(char)*strlen(Names[2]));
	for (i=0; i<Model_getNumSpeciesTypes(m); i++) {
		map[i]=(char*)malloc(sizeof(char)*strlen(Names[i]));
		strcpy(map[i],Names[i]);
	}

	
	for (i=0; i<spectype; i++) {
		mapS[i]=(char*)malloc(sizeof(char)*strlen(NameSite[i]));
		strcpy(mapS[i],NameSite[i]);
	}

	SBMLDocument_free(d);
	return 0;

}