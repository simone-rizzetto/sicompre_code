/*
 * Simone Rizzetto - Biological Systems Simulation Complex Version
 * Implementation of Gillespie Multi-Particle/Complex on GPGPU using CUDA 
 * 2010-2011

 What's new? This version allow the definition of specie type. It simulate only complexation and decomplexation of
 molecule. It use the speciesType as a whole molecule and a simple specie as binding site. To perform a classic simulation 
 use the previous version of GPU Gillespie Multi-Particle.

 Assumption:
 1) Unbinding rate between 0 and 1

 TODO list:

 Current Problems:
 
 Test:

 */

// Includes
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cuda_runtime_api.h>
#include <time.h>

#include <windows.h>
//#include <shrUtils.h>
//#include <cuPrintf.cu>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <ctype.h>
#include "sbmlParserC_compart.h"
#include "queue.h"
#include "GPGMPComplex_compart.cuh"
#include <helper_cuda.h>


//The macro CUPRINTF is defined for architectures
//with different compute capabilities.
#if __CUDA_ARCH__ < 200 	//Compute capability 1.x architectures
#define CUPRINTF cuPrintf
#else						//Compute capability 2.x architectures
#define CUPRINTF(fmt, ...) printf("[%d, %d]:\t" fmt, \
								blockIdx.y*gridDim.x+blockIdx.x,\
								threadIdx.z*blockDim.x*blockDim.y+threadIdx.y*blockDim.x+threadIdx.x,\
								__VA_ARGS__)
#endif



/*#define SPECIES 36155	
#define PROT_SPECIES 55
#define NUMSITE 599
#define MOLECULES 2941
#define REACTIONS 222	//the reactions number include both complex and decomplex reaction declared in the input file
#define SPECIES 27535	 //nucleous
#define PROT_SPECIES 842
#define NUMSITE 884
#define MOLECULES 17855
#define REACTIONS 2304
#define SPECIES 18931	 //vacuole
#define PROT_SPECIES 281
#define NUMSITE 259
#define MOLECULES 5780
#define REACTIONS 262	
#define SPECIES 32997	 //cytoplasm
#define PROT_SPECIES 815
#define NUMSITE 804
#define MOLECULES 22127
#define REACTIONS 1380*/
//#define SPECIES 57911	 //nucleous collins
//#define PROT_SPECIES 595
//#define MOLECULES 12451 
#define MOLECULES 1000000 //max number of molecules, in 1GB RAM devices could be incresed up to 60000, but it could generate some memory errors
//#define REACTIONS 829


//fixed
//#define CELLS 8192	//# of subvolumes
#define CELLS 4096	//# of subvolumes
#define BLOCKS 128	//a block is executed by a processor, my GPU has 12 processor, so is better choose a multiple of 12
#define THREADS 64	//threads executed in each block, for max occupancy choose a multiple of 32, 32 is the warp size
//#define GRID_W 16	//grid width = 6*8 subvolumes
#define GRID_W 8	//grid width = 6*8 subvolumes
#define GRID_H 8	//grid height = 4*8 subvolumes
#define BLOCK_W 8
#define BLOCK_H 8
#define CPU_THREADS 64
#define MAX_CONC 400

typedef struct MyData1 {
	float tmax;
	int i;
	curandState *devStates;
	site* site_state;
	molecule* molecule_state;
	const int* v;
	const float* c;
	const float* cu;
	const int* abdS;
	float* a;
	int* speciesBounded;
} MYDATA1, *PMYDATA1;

typedef struct MyData2 {
	int** s_speciesAbd;
	int* t_speciesAbd;
	int i;
} MYDATA2, *PMYDATA2;
// Variables
HANDLE Thread1[CPU_THREADS];
HANDLE DMThread[CELLS/8];
//bool noprompt = false;
PMYDATA1 pDataArray[CPU_THREADS];
PMYDATA2 pDataArray2[CPU_THREADS];

PMYDATA1 pDataArrayDM[CELLS/8];

char idCompart[40][20];

site* site_state;  //potrei usare array di array in modo da dividerli per specie!
molecule* molecule_state;
site* d_site_state;
molecule* d_molecule_state;

// Variables




	bool completed[CELLS];
	int tmp_c[CELLS];
	double t[CELLS];
	double u[CELLS],u2[CELLS],tau[CELLS];
	double a0[CELLS];
	int mu[CELLS];



static CRITICAL_SECTION cs_mutex;
static CRITICAL_SECTION cs_sync;
static CRITICAL_SECTION cs_cb;
static CRITICAL_SECTION cs_cu;
//static CRITICAL_SECTION cs_reactCount;
static int sync=0;
static int cb=0;
static int cun=0;
//static int reactCount[REACTIONS];
static long complex_count=0;
int species; //number of species
int tot_molecule;
int tot_site;
int reacts;
int nmol;
//float tm=1.8; //max time of the simulation
float tm=1.8; //max time of the simulation
float l=0.1; //lattice size
int cells; //number of subvolume in the grid
float* c; //vector of rates;
float* cu; //vector of unbounding rates;
float* d_c; //device
float* d_cu; //device
float* d_tau; //device (diffusion)
int* d_n; //devicie (diffusion)
int* d_abdS; //
float* deb; //array used for debug
float* d_deb; //device debug
float* d_degree;
float* d_ts; //device time
float* d_a0; 
//float a[(CELLS/CPU_THREADS)*REACTIONS*2];
//float a[2*REACTIONS*(CELLS/CPU_THREADS)];
float* d_a;
int* d_max_complex;
int* v; //particle used in reaction, example: v[i][j]==-1 the reaction i consumed 1 particle j;
int* d_v; //device
double** rates; //reaction rates
bool displaystep=false; //print intermediate steps in a file
char* inputfile; //sbml file
//int counter[CELLS][PROT_SPECIES];
int** counter;
int resN=0;
//__device__ int complex_count=0;
//__device__ int mutex;
//__device__ float a[CELLS][REACTIONS+MOLECULES]; //10 is the reaction
//__device__ float a[CELLS][REACTIONS*2]; //10 is the reaction
//__shared__ float a[THREADS][REACTIONS];
//__device__ unsigned short int memo[MOLECULES][NUMSITE]; //used for memoization it is too large
//__device__ unsigned short memo[CELLS][MOLECULES][2]; //used for memoization, it is too large
//__device__ unsigned short memo[CELLS/CPU_THREADS][REACTIONS][MOLECULES][2]; //used for memoization, it is too large
//__device__ unsigned short memo[CELLS/CPU_THREADS][64][MOLECULES][2]; //used for memoization, it is too large
unsigned short memo2[CPU_THREADS][MOLECULES][2]; //used for memoization, it is too large
unsigned short** d_molsInLattice;
unsigned short** h_molsInLattice;
int* d_debug;
int* speciesBounded;
int Q[CELLS];
//__device__ int Q2[CELLS];
int* d_Q;
int* d_new_pos;
__device__ int diffused[MOLECULES];
rec_t* rec_gpu;
rec_t* d_rec;
float* g_a;
float* d_a2;
int** StoM;
//int* d_speciesBounded;

//__constant__ int d_speciesBounded[10];

//int* diffused;
//unsigned short memo2[1][MOLECULES][2]; //used for memoization, it is too large
//__device__ int memDecomplex[CELLS][MOLECULES];


// Functions
void Cleanup(void);
void writeState(int, float );
float Exponential(float, float);
__device__ float h(site*, molecule*, float, const int*, int, int , const int*, int, int, int);
float h_CPU(site*, molecule*, float, const int*, int, int , const int*, int);
//__device__ float hu(site*, molecule*, const float*, const int*, int, int , int, const int*);
__device__ float hu(site*, molecule*, float, const int*, int, int , const int*);
float hu_CPU(site*, molecule*, float, const int*, int, int , const int*);
__device__ int CountSiteofSpec(site*, int, int, const int*, int, int, int, memo_gpu*);
__device__ int CountSiteofSpec2(site*, int, int, const int*, int, int, int, memo_gpu*);
int CountSiteofSpec_CPU(site*, int, int, const int*, int, int);
int CountSiteofSpec2_CPU(site*, int, int, const int*, int, int);
__device__ int CountSiteofSpec_u(site*, int, int, int, const int*);
void chooseSite_u(int, int, int*, int*, float, site*, int, const int*);
int Weights(float*, double, int);
int Weights2(double, int);
void updateComplex(molecule*, int, int , int );
void chooseSite(int, int, int*, int*, float, site*, int, const int*, int);
void updateMol(molecule*, site*, int, int, int, int, int, int*);
__device__ void autocomplexation(site*, molecule*, int, const int*, int, int, int, curandState*);
__device__ void updateC(molecule*, int, site*);
//__shared__ float a0[THREADS];
//__shared__ short mu[THREADS];

//__shared__ unsigned short m_inv1[THREADS], m_inv2[THREADS];
void ParseArguments(int, char**);
void writeCompl(char**);
int devs;

// Device code





__global__ void CloseSim(int* s_max_complex, site* s_site_state, molecule* s_molecule_state, int tot_molecule, int tot_site) {
//	int count=0;
	//for (int j=0; j<tot_site; j++) {
		//if (s_site_state[j].bound->bound!=&s_site_state[j]) CUPRINTF("Error during simulation - %d\n", j);
		//if (s_site_state[j].bound!=NULL) count++;
	//}
	//if (count%2!=0) CUPRINTF("Error during simulation!\n");
	//*s_max_complex=complex_count;

	//CUPRINTF("complexes %d\n", *s_max_complex);
}

__global__ void ComplexDegree(float* s_degree, site* s_site_state, molecule* s_molecule_state, int tot_molecule, int tot_site) {
	//int s_degree[complex_count];
//	int tt;
	/*for (int i=0; i<complex_count; i++) {
		s_degree[i]=0;
		tt=0;
		for (int j=0; j<tot_molecule; j++) {
			if (s_molecule_state[j].complex==i) {
				tt++;
				for (int k=0; k<tot_site; k++) {
					if (s_site_state[k].mol==j && s_site_state[k].bound!=NULL) s_degree[i]++;
				}
			}
		}
		if (tt>1) s_degree[i]=(s_degree[i]/(tt*(tt-1)));
	}*/
}
//Direct Method
//__global__ void DM(curandState *state, site* s_domLst, molecule* d_prot_lst,int** s_v, const float* s_c, float tmax, float* s_deb, const int reacts) {

__global__ void binding(curandState *state, float* s_a0, site* s_site_state, molecule* s_molecule_state, const int* v, const float* s_c, const float* s_cu, const float t_max, const int* s_abdS, int i, float* s_a, int tr, int reacts, int tot_molecule) {

	//int id=blockDim.x * blockIdx.x + threadIdx.x;
	//int thr=i*(CELLS/CPU_THREADS);
	int cell=i*(CELLS/CPU_THREADS)+blockIdx.x;
	int real_tr=64*tr+threadIdx.x;
	//*s_a0=h(s_site_state, s_molecule_state, s_c[threadIdx.x], v, threadIdx.x, i, s_abdS);
	//s_a[threadIdx.x*blockIdx.x*2+blockIdx.x]=h(s_site_state, s_molecule_state, s_c[blockIdx.x], v, blockIdx.x, thr+threadIdx.x, s_abdS);
	s_a[blockIdx.x*reacts*2+real_tr*2]=h(s_site_state, s_molecule_state, s_c[real_tr], v, real_tr, cell, s_abdS, blockIdx.x, threadIdx.x, tot_molecule);

	//if (s_a[blockIdx.x*REACTIONS*2+real_tr*2]>0) CUPRINTF("debug reaction r=%d, c=%d\n", real_tr, blockIdx.x*REACTIONS*2+real_tr*2);
	//if ( threadIdx.x==24 && s_a[blockIdx.x*REACTIONS*2+24]>0) CUPRINTF("DEBUG GPU\n");
	//*s_a0+=a[thr+threadIdx.x][blockIdx.x]+1;
	//atomicAdd(s_a0,1);
	//*s_a0+=s_a[threadIdx.x*blockIdx.x*2+blockIdx.x];
	//*s_a0+=s_a[threadIdx.x];
	//*s_a0+=1;
	//if (threadIdx.x==9) *s_a0=memo[i][v[threadIdx.x*2]][0];
	//if (threadIdx.x==9) *s_a0=h(s_site_state, s_molecule_state, s_c[threadIdx.x], v, threadIdx.x, i, s_abdS);
	//if (threadIdx.x==9) *s_a0=s_a[threadIdx.x];
	//if (threadIdx.x==9) CUPRINTF("\t--->a0=%f,a[9]=%f,h=%f\n",*s_a0,s_a[threadIdx.x],h(s_site_state, s_molecule_state, s_c[threadIdx.x], v, threadIdx.x, i, s_abdS));
	//a[threadIdx.x][blockIdx.x]=1;
	
}

void binding_CPU(float* s_a0, site* s_site_state, molecule* s_molecule_state, const int* v, const float* s_c, const float* s_cu, const float t_max, const int* s_abdS, int cell, float* s_a, int j, int i, int cpu_t) {

	//int id=blockDim.x * blockIdx.x + threadIdx.x;
	//int thr=i*(CELLS/CPU_THREADS);
	
	//printf("binding\n");
	s_a[i*reacts*2+j*2]=h_CPU(s_site_state, s_molecule_state, s_c[j], v, j, cell, s_abdS, cpu_t);
	
	//*s_a0+=a[thr+threadIdx.x][blockIdx.x]+1;
	//atomicAdd(s_a0,1);
	//*s_a0+=s_a[j];
	//a[threadIdx.x][blockIdx.x]=1;
	
}
__global__ void unbinding(curandState *state, site* s_site_state, molecule* s_molecule_state, const int* v, const float* s_c, const float* s_cu, const float t_max, const int* s_abdS, int i, float* s_a, int tr, int reacts) {	
	
	//short id=blockDim.x * blockIdx.x + threadIdx.x;
	int cell=i*(CELLS/CPU_THREADS)+blockIdx.x;
	int real_tr=64*tr+threadIdx.x;
	s_a[blockIdx.x*reacts*2+real_tr*2+1]=hu(s_site_state, s_molecule_state, s_cu[real_tr], v, real_tr, cell, s_abdS);
	//if (s_a[blockIdx.x*reacts*2+real_tr*2+1]>0) CUPRINTF("va\n");
}


void unbinding_CPU(site* s_site_state, molecule* s_molecule_state, const int* v, const float* s_c, const float* s_cu, const float t_max, const int* s_abdS, int cell, float* s_a, int j, int i) {	
	
	//short id=blockDim.x * blockIdx.x + threadIdx.x;
	//int cell=i*(CELLS/CPU_THREADS)+blockIdx.x;
	s_a[i*reacts*2+j*2+1]=hu_CPU(s_site_state, s_molecule_state, s_cu[j], v, j, cell, s_abdS);
	
}

__global__ void binding2(float* s_a0, site* s_site_state, molecule* s_molecule_state, const int* v, const float* s_c, const float* s_cu, const float t_max, const int* s_abdS, int i, float* s_a, int tr, int reacts, int tot_molecule, rec_t* s_rec) {

	//int cell=i*(CELLS/CPU_THREADS)+blockIdx.x;
	
	int real_tr=64*tr*512+blockIdx.x*blockDim.x+threadIdx.x;
	if (real_tr>=i ) return;
	int cell=s_rec[real_tr].cellID;
	int react=s_rec[real_tr].react;
	s_a[real_tr*2]=h(s_site_state, s_molecule_state, s_c[react], v, react, cell, s_abdS, blockIdx.x, threadIdx.x, tot_molecule);

	
}
__global__ void unbinding2(site* s_site_state, molecule* s_molecule_state, const int* v, const float* s_c, const float* s_cu, const float t_max, const int* s_abdS, int i, float* s_a, int tr, int reacts, rec_t* s_rec) {	
	
	//short id=blockDim.x * blockIdx.x + threadIdx.x;

	int real_tr=64*tr*512+blockIdx.x*blockDim.x+threadIdx.x;	
	if (real_tr>=i ) return;
	int cell=s_rec[real_tr].cellID;
	int react=s_rec[real_tr].react;
	s_a[real_tr*2+1]=hu(s_site_state, s_molecule_state, s_cu[react], v, react, cell, s_abdS);
	//if (s_a[blockIdx.x*reacts*2+real_tr*2+1]>0) CUPRINTF("va\n");
}


__global__ void Diffusion(curandState *state, site* s_site_state, molecule* s_molecule_state, float* s_tau, int* s_n, float* s_ts, int blocks, bool rest, int cput, unsigned short* s_molInLattice) {
	int mol_ID;
	if (!rest) mol_ID=blockIdx.x*blocks+threadIdx.x;
	else mol_ID=(blocks*CPU_THREADS*cput)+blockIdx.x*blocks+threadIdx.x;
	
	
	int threadIdx=s_molecule_state[mol_ID].cell%THREADS;
	int blockIdx=s_molecule_state[mol_ID].cell/THREADS;

	curand_init(clock(), 1, 0, &state[mol_ID]); //seed, sequence, offset, state
	curandState localState = state[mol_ID];

	int p=mol_ID;
		float u=curand_uniform_double(&localState);
		//CUPRINTF("u %d\n", u)
		//if ((blockIdx.x>=GRID_W || threadIdx.x>=BLOCK_W)&&u<0.125) {
		if ((blockIdx>=GRID_W || threadIdx>=BLOCK_W)&&u<0.250) {
			//diffusion above
			if (threadIdx>=BLOCK_W) {
				s_molecule_state[p].readyToDiffuse=1;
			}
			else {
				s_molecule_state[p].readyToDiffuse=1;
			}
		}

		//else if ((blockIdx.x<GRID_W*(GRID_H-1) || threadIdx.x<BLOCK_W*(BLOCK_H-1)) && u>=0.125 && u<0.25) {
		else if ((blockIdx<GRID_W*(GRID_H-1) || threadIdx<BLOCK_W*(BLOCK_H-1)) && u>=0.25 && u<0.5) {
			//diffusion below
			if (threadIdx<BLOCK_W*(BLOCK_H-1)) {
				s_molecule_state[p].readyToDiffuse=2;
			}
			else {
				s_molecule_state[p].readyToDiffuse=2;
			}
		}

		//else if ((blockIdx.x%GRID_W!=0 || threadIdx.x%BLOCK_W!=0) && u>=0.25 && u<0.375) {
		else if ((blockIdx%GRID_W!=0 || threadIdx%BLOCK_W!=0) && u>=0.5 && u<0.75) {
			//diffusion left
			if (threadIdx%BLOCK_W!=0) {
				s_molecule_state[p].readyToDiffuse=3;
			}
			else {
				s_molecule_state[p].readyToDiffuse=3;
			}
		}

		else if ((blockIdx%GRID_W!=GRID_W-1 || threadIdx%BLOCK_W!=BLOCK_W-1) && u>=0.75) {
			//diffusion right
			if (threadIdx%BLOCK_W!=BLOCK_W-1) {
				s_molecule_state[p].readyToDiffuse=4;
			}
			else {
				s_molecule_state[p].readyToDiffuse=4;
			}
		}
		
}

__global__ void reset(int tot_molecules) {
	int thr=blockIdx.x*blockDim.x+threadIdx.x;
	for (int i=0; i<100; i++) {
		if (100*thr+i<tot_molecules) {
			diffused[100*thr+i]=0;
	//test
			//new_pos[mol_ID]=move;
		}
	}


}
__global__ void debug_kernel(int tot_molecules, int* new_pos,int* debug, molecule* s_molecule_state, int diff_k) {
	//if ((threadIdx.x==10 || threadIdx.x==11 || threadIdx.x==12)) cuPrintf("thr = %d, block=%d\n", threadIdx.x, blockIdx.x);
	//int diff_k=0;
	/*int thr=blockIdx.x*blockDim.x+threadIdx.x;
	int row=blockIdx.x*2;
	if (diff_k>=3) row++;
	int column=threadIdx.x*3;
	column+=(diff_k%3);
	if (row%4==2||row%4==3) {
		if (column==BLOCK_W*GRID_W) 
			column=0;
		else column=(column+1);
	}
	if (diff_k<3 && blockIdx.x==21 && (threadIdx.x==10 || threadIdx.x==11 || threadIdx.x==12)) cuPrintf("row = %d, column=%d\n", row, column);
	if (column>=BLOCK_W*GRID_W) return;
	int CellID=BLOCK_W*GRID_W*row+column;
	if (CellID==2722 || CellID==2723 || CellID==2724 ) cuPrintf("fatti GPU\n");
	//debug[0]=1;
	new_pos[CellID]=1;
	for (int i=0; i<tot_molecules; i++) {
		if (s_molecule_state[i].cell==CellID) {
			debug[i]=1;
		}
		//cuPrintf("debug = %d - molecule_cell = %d\n", debug[i],s_molecule_state[i].cell);
	}*/


}
__global__ void CheckDiffSite(int* s_rec, int* s_v, int s_spec, int s_cell, int m_cell, int tot_reactions, int rest) {
	int id=blockIdx.x*blockDim.x+threadIdx.x;
	if (rest>0) id+=rest;
	if (s_v[id*2]==s_spec || s_v[id*2+1]==s_spec) {
		s_rec[s_cell*tot_reactions+id]=1;
		s_rec[m_cell*tot_reactions+id]=1;
	}
}
__global__ void Diffusion_GPU(curandState *state, molecule* s_molecule_state, int* Q, int tot_molecule, int tot_site, int diff_k, int* new_pos, int* debug, int c, float* s_rnd) {
	
	//int blo=blockIdx.x+16*c;
	//int column=threadIdx.x*3;
	int row=blockIdx.x*2;
	if (diff_k>=3) row++;
	int column=threadIdx.x*3;
	column+=(diff_k%3);
	if (row%4==2||row%4==3) 
		if (column==BLOCK_W*GRID_W) 
			column=0;
		else column=(column+1);
	if (column>=BLOCK_W*GRID_W) return;
	int CellID=BLOCK_W*GRID_W*row+column;
	//cuPrintf("OK %d\n", CellID);
	//int row=floor((float)CellID/(BLOCK_W*GRID_W))*2;
	//int s_column=diff_k%3;
	//if (row%2==1) s_column=(s_column+1)%3;
    //int CellID=blockIdx.x*blockDim.x+threadIdx.x;
	int move=0;
	//for (int i=0; i<tot_molecule; i++) {
	//	if (s_molecule_state[i].cell==CellID) diffused[i]=0;
	//}
	
	//curand_init(clock(), CellID, 0, &state[CellID]); //seed, sequence, offset, state
	//curandState localState = state[CellID];
	int res=0;
	for (int mol_ID=0; mol_ID<tot_molecule; mol_ID++) {
		if (s_molecule_state[mol_ID].cell==CellID) {
			if (diffused[mol_ID]==0) {
				diffused[mol_ID]=1;
				//float u=curand_uniform_double(&localState);	
				//float u2=curand_uniform_double(&localState);
				float u=s_rnd[mol_ID*2];
				float u2=s_rnd[mol_ID*2+1];
				int size=s_molecule_state[mol_ID].size;
				int complex=s_molecule_state[mol_ID].complex;
				/*if (s_molecule_state[mol_ID].complex!=0) {
					for (int i=0; i<tot_molecule; i++) {
						if (s_molecule_state[i].complex==complex && i!=mol_ID) {
							//diffuse=s_molecule_state[i].readyToDiffuse;
							size++;
							diffused[i]=1;
						}
					}
				}
			*/
				//debug[mol_ID]=2;
				//if ((blockIdx.x>=GRID_W || threadIdx.x>=BLOCK_W)&&u<0.250) {
				
				//new_pos[mol_ID]=-1;
				if (CellID>=BLOCK_W*GRID_W && u<=0.250) {
					//diffusion above
					
					//if (threadIdx.x>=BLOCK_W) {
						move=CellID-BLOCK_W*GRID_W;
						
						if (u2<(((float)(MAX_CONC-(Q[move]+size)))/MAX_CONC)) {
							new_pos[mol_ID]=move;
							//res=atomicAdd(&Q2[move],size);
							//res=atomicAdd(&Q2[CellID],(size*-1));
							Q[move]+=size;
							Q[CellID]-=size;
							new_pos[mol_ID]=move;
							//updateMol(s_molecule_state, s_site_state, move, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex, tot_molecule, tot_site);
						}
						//else new_pos[mol_ID]=-1;
				}
				
				//else if ((blockIdx.x<GRID_W*(GRID_H-1) || threadIdx.x<BLOCK_W*(BLOCK_H-1)) && u>=0.125 && u<0.25) {
				//else if ((blockIdx.x<GRID_W*(GRID_H-1) || threadIdx.x<BLOCK_W*(BLOCK_H-1)) && u>=0.25 && u<0.5) {
				else if ((CellID<CELLS-BLOCK_W*GRID_W) && u>0.25 && u<=0.5) {
					//diffusion below
						move=CellID+BLOCK_W*GRID_W;
						if (u2<(((float)(MAX_CONC-(Q[move]+size)))/MAX_CONC)) {
							new_pos[mol_ID]=move;
							//res=atomicAdd(&Q2[move],size);
							//res=atomicAdd(&Q2[CellID],(size*-1));
							Q[move]+=size;
							Q[CellID]-=size;
							new_pos[mol_ID]=move;
							//new_pos[mol_ID]=-1;
							//updateMol(s_molecule_state, s_site_state, move, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex, tot_molecule, tot_site);
						}
						//else new_pos[mol_ID]=-1;
				}

				//else if ((blockIdx.x%GRID_W!=0 || threadIdx.x%BLOCK_W!=0) && u>=0.25 && u<0.375) {
				//else if ((blockIdx.x%GRID_W!=0 || threadIdx.x%BLOCK_W!=0) && u>=0.5 && u<0.75) {
				else if (((CellID%(GRID_W*BLOCK_W))!=0) && u>0.5 && u<=0.75) {
					//diffusion left
						move=CellID-1;
						if (u2<(((float)(MAX_CONC-(Q[move]+size)))/MAX_CONC)) {
							new_pos[mol_ID]=move;
							//res=atomicAdd(&Q2[move],size);
							//res=atomicAdd(&Q2[CellID],(size*-1));
							Q[move]+=size;
							Q[CellID]-=size;
							new_pos[mol_ID]=move;
							//new_pos[mol_ID]=-1;
							//updateMol(s_molecule_state, s_site_state, CellID-1, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex, tot_molecule, tot_site);
						}
						//else new_pos[mol_ID]=-1;
				}

				else if (((CellID%(GRID_W*BLOCK_W))!=(GRID_W*BLOCK_W-1)) && u>0.75) {
					//diffusion right
						move=CellID+1;
						if (u2<(((float)(MAX_CONC-(Q[move]+size)))/MAX_CONC)) {
							new_pos[mol_ID]=move;
							//res=atomicAdd(&Q2[move],size);
							//res=atomicAdd(&Q2[CellID],(size*-1));
							Q[move]+=size;
							Q[CellID]-=size;
							new_pos[mol_ID]=move;
							//new_pos[mol_ID]=-1;
							//updateMol(s_molecule_state, s_site_state, CellID+1, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex, tot_molecule, tot_site);
						}
						//else new_pos[mol_ID]=-1;
				}
				//else new_pos[mol_ID]=-1;
				
			}
		}
	}
		
}
/*
void Diffusion_CPU(site* s_site_state, molecule* s_molecule_state, float* s_tau, int* s_n, float* s_ts, int blocks, bool rest, int cput) {
	//int mol_ID;
	//if (!rest) mol_ID=blockIdx.x*blocks+threadIdx.x;
	//else mol_ID=(blocks*CPU_THREADS*cput)+blockIdx.x*blocks+threadIdx.x;
	int move=0;
	for (int i=0; i<tot_molecule; i++) diffused[i]=0;
	for (int i=0; i<CELLS; i++) {
		Q[i]=0;
	}
	//newest diffusion
	for (int i=0; i<tot_molecule; i++) {
		Q[s_molecule_state[i].cell]++;
	}
	//newest diffusion ends
	for (int mol_ID=0; mol_ID<tot_molecule; mol_ID++) {
		if (diffused[mol_ID]==0) {
			diffused[mol_ID]=1;
			int threadIdx=s_molecule_state[mol_ID].cell%THREADS;
			int blockIdx=s_molecule_state[mol_ID].cell/THREADS;
			
			//curand_init(clock(), 1, 0, &state[mol_ID]); //seed, sequence, offset, state
			//curandState localState = state[mol_ID];
			
			int CellID=molecule_state[mol_ID].cell;
			int p=mol_ID;
				double u=rand() / double(RAND_MAX);
				double u2=rand() / double(RAND_MAX);
				//CUPRINTF("u %d\n", u)
				//if ((blockIdx.x>=GRID_W || threadIdx.x>=BLOCK_W)&&u<0.125) {
				if ((blockIdx>=GRID_W || threadIdx>=BLOCK_W)&&u<0.250) {
					//diffusion above
					
					if (threadIdx>=BLOCK_W) {
						move=CellID-BLOCK_W;
						if (u2<(100-Q[move]/100))
							s_molecule_state[p].readyToDiffuse=1;
							Q[move]++;
							updateMol(s_molecule_state, site_state, CellID-BLOCK_W, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex);
					}
					else {
						move=CellID-(GRID_W-1)*THREADS-BLOCK_W;
						if (u2<(100-Q[move]/100))
							s_molecule_state[p].readyToDiffuse=1;
							Q[move]++;
							updateMol(s_molecule_state, site_state, CellID-(GRID_W-1)*THREADS-BLOCK_W, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex);
					}
				}

				//else if ((blockIdx.x<GRID_W*(GRID_H-1) || threadIdx.x<BLOCK_W*(BLOCK_H-1)) && u>=0.125 && u<0.25) {
				else if ((blockIdx<GRID_W*(GRID_H-1) || threadIdx<BLOCK_W*(BLOCK_H-1)) && u>=0.25 && u<0.5) {
					//diffusion below
					if (threadIdx<BLOCK_W*(BLOCK_H-1)) {
						move=CellID+BLOCK_W;
						if (u2<(100-Q[move]/100))
							s_molecule_state[p].readyToDiffuse=2;
							Q[move]++;
							updateMol(s_molecule_state, site_state, CellID+BLOCK_W, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex);
					}
					else {
						move=CellID+(GRID_W-1);
						if (u2<(100-Q[move]/100))
							s_molecule_state[p].readyToDiffuse=2;
							Q[move]++;
							updateMol(s_molecule_state, site_state, CellID+(GRID_W-1)*THREADS+BLOCK_W, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex);
					}
				}

				//else if ((blockIdx.x%GRID_W!=0 || threadIdx.x%BLOCK_W!=0) && u>=0.25 && u<0.375) {
				else if ((blockIdx%GRID_W!=0 || threadIdx%BLOCK_W!=0) && u>=0.5 && u<0.75) {
					//diffusion left
					if (threadIdx%BLOCK_W!=0) {
						move=CellID-1;
						if (u2<(100-Q[move]/100))
							s_molecule_state[p].readyToDiffuse=3;
							Q[move]++;
							updateMol(s_molecule_state, site_state, CellID-1, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex);
					}
					else {
						move=CellID-(BLOCK_H-1)*BLOCK_W-1;
						if (u2<(100-Q[move]/100))
							s_molecule_state[p].readyToDiffuse=3;
							Q[move]++;
							updateMol(s_molecule_state, site_state, CellID-(BLOCK_H-1)*BLOCK_W-1, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex);
					}
				}

				else if ((blockIdx%GRID_W!=GRID_W-1 || threadIdx%BLOCK_W!=BLOCK_W-1) && u>=0.75) {
					//diffusion right
					if (threadIdx%BLOCK_W!=BLOCK_W-1) {
						move=CellID+1;
						if (u2<(100-Q[move]/100))
							s_molecule_state[p].readyToDiffuse=4;
							Q[move]++;
							updateMol(s_molecule_state, site_state, CellID+1, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex);
					}
					else {
						move=CellID+(BLOCK_H-1)*BLOCK_W+1;
						if (u2<(100-Q[move]/100))
							s_molecule_state[p].readyToDiffuse=4;
							Q[move]++;
							updateMol(s_molecule_state, site_state, CellID+(BLOCK_H-1)*BLOCK_W+1, s_molecule_state[mol_ID].id, s_molecule_state[mol_ID].complex);
					}
				}
		}
	}
		
}*/

DWORD WINAPI DMpart2(LPVOID lpParam) {
	long t1,t2;
	int debug;
	double diff;
	PMYDATA1 pDataArray1 = (PMYDATA1)lpParam;
	int cel=pDataArray1->i*(CELLS/8);
	for (int i=pDataArray1->i*(CELLS/8); i<(pDataArray1->i+1)*(CELLS/8); i++) {
		if (!completed[i]) {
				a0[i]=0;
				for (int j=0; j<reacts; j++) {
					if (g_a[i*reacts*2+j*2]<-0.1 || g_a[i*reacts*2+j*2]>0.1) {
						a0[i]+=g_a[i*reacts*2+j*2]; 
						a0[i]+=g_a[i*reacts*2+j*2+1];
					}
				}
				
				if (a0[i]<0.001 && a0[i]>-0.001) completed[i]=true;
				else {
					//printf("debug1\n");
					u[i]=rand() / double(RAND_MAX);
					u2[i]=rand() / double(RAND_MAX);
					tau[i]=Exponential(a0[i],u[i]);
					if (tau[i]+t[i]<=pDataArray1->tmax) {
						//mu[i]=Weights(pDataArray1->a,a0[i]*u2[i], i);
						mu[i]=Weights2(a0[i]*u2[i], i);
						if (mu[i]>=reacts*2 || mu<0) printf("ERROR mu=%d a0=%f tau=%f\n", mu[i],a0[i],tau[i]);
						if (mu[i]==-1) completed[i]=true;
						else if (mu[i]%2!=0) {
							mu[i]=(mu[i]-1)/2;
							
							//EnterCriticalSection( &cs_cu );
							//		cun++;
							//LeaveCriticalSection( &cs_cu );
							int s1=0;
							int s2=0;
							u[i]=rand() / double(RAND_MAX);
							chooseSite_u(pDataArray1->v[mu[i]*2], pDataArray1->v[mu[i]*2+1], &s1, &s2, u[i], pDataArray1->site_state, i, pDataArray1->abdS);
							if (pDataArray1->site_state[s1].cell!=i || pDataArray1->site_state[s2].cell!=i) {
								printf("ERROR CHOOSE SITE U\n");
								exit(0);
							}
							if (pDataArray1->site_state[s1].specieBounded==-1 || pDataArray1->site_state[s2].specieBounded==-1 || pDataArray1->site_state[s1].bound==NULL || pDataArray1->site_state[s2].bound==NULL) printf("error not bounded\n");
							int debug0=0;
							for (int m=0; m<tot_molecule; m++) {
								if (pDataArray1->molecule_state[m].id==pDataArray1->site_state[s1].mol || pDataArray1->molecule_state[m].id==pDataArray1->site_state[s2].mol) {
									if (pDataArray1->molecule_state[m].degree==0) {
										printf("error unbinding impossible: reaction %d, mol %d s1 = %d, s2 = %d, specieBounded1 = %d, specieBounded2 = %d, specie1=%d, specie2=%d\n", mu, m, s1, s2, pDataArray1->site_state[s1].specieBounded, pDataArray1->site_state[s2].specieBounded, pDataArray1->site_state[s1].specie, pDataArray1->site_state[s2].specie);
										printf("mol id %d, %d, m=%d\n", pDataArray1->site_state[s1].mol, pDataArray1->site_state[s2].mol, m);
										printf("v[mu*2]=%d, v[mu*2+1]=%d\n", v[mu[i]*2], v[mu[i]*2+1]);
								
									}
									pDataArray1->molecule_state[m].degree--;
									debug0++;
								}
							}
							if (debug0!=2) printf("ERROR debug 0\n", mu);
							pDataArray1->site_state[s1].bound=NULL;
							pDataArray1->site_state[s2].bound=NULL;
							pDataArray1->site_state[s1].specieBounded=-1;
							pDataArray1->site_state[s2].specieBounded=-1;
							bool bounded1=false;
							bool bounded2=false;
							//bounded1=true;
							//bounded2=true;
							//for (int s=0; s<tot_site && (!bounded1||!bounded2); s++) {
							//	if (pDataArray1->site_state[s].mol==pDataArray1->site_state[s1].mol && pDataArray1->site_state[s].specieBounded!=-1) bounded1=true;
							//	if (pDataArray1->site_state[s].mol==pDataArray1->site_state[s2].mol && pDataArray1->site_state[s].specieBounded!=-1) bounded2=true;

							//}
							int ind=0;
							int s;
							while (StoM[pDataArray1->site_state[s1].mol][ind]!=-1) {
								s=StoM[pDataArray1->site_state[s1].mol][ind];
								if (pDataArray1->site_state[s].specieBounded!=-1) bounded1=true;
								ind++;
							}
							ind=0;
							while (StoM[pDataArray1->site_state[s2].mol][ind]!=-1) {
								s=StoM[pDataArray1->site_state[s2].mol][ind];
								if (pDataArray1->site_state[s].specieBounded!=-1) bounded2=true;
								ind++;
							}
							int complex=pDataArray1->molecule_state[pDataArray1->site_state[s1].mol].complex;
							int debug1=0;
							int debug2=0;
							if (bounded1) {
								for (int m=0; m<tot_molecule; m++) {
									if (pDataArray1->molecule_state[m].id==pDataArray1->site_state[s1].mol) {
										complex=pDataArray1->molecule_state[m].complex;
									}
								}
								debug1++;
								EnterCriticalSection( &cs_mutex );
									complex_count++;
									tmp_c[i]=complex_count;
								LeaveCriticalSection( &cs_mutex );
								//bfs starting from s1.mol
								t1=clock();
								debug=0;
								queue* Q;
								queue* M;
								Q=(queue*)malloc(sizeof(queue));
								M=(queue*)malloc(sizeof(queue));
								init(Q); //wrong, should create one queue for every variable;
								initM(M);
								int curmol;
								enqueue(Q, pDataArray1->site_state[s1].mol); 
								//enqueueM(M, pDataArray1->site_state[s1].mol); //mark it
								molecule_state[pDataArray1->site_state[s1].mol].complex=tmp_c[i];
								while(!emptyp(Q)) {
									if (dequeue(Q, &curmol)==FAIL) printf("error queue\n");
									//pDataArray1->molecule_state[curmol].complex=tmp_c[i];
									ind=0;
									int s;
									while (StoM[curmol][ind]!=-1) {
									//for (s=0; s<tot_site; s++) {
										s=StoM[curmol][ind];
										debug++;
										if (pDataArray1->site_state[s].mol==curmol && pDataArray1->site_state[s].specieBounded!=-1) {
										//if (pDataArray1->site_state[s].specieBounded!=-1) {
											//if not marked
											//if (!inQueue(M, pDataArray1->site_state[s].bound->mol)) {
											if (pDataArray1->molecule_state[pDataArray1->site_state[s].bound->mol].complex!=tmp_c[i]) {
												//mark it
												//enqueueM(M, pDataArray1->site_state[s].bound->mol);
												enqueue(Q, pDataArray1->site_state[s].bound->mol);
												pDataArray1->molecule_state[pDataArray1->site_state[s].bound->mol].complex=tmp_c[i];
											}

										}
										ind++;
									}
								}
								deleteM(M);
								deleteM(Q);
								free(Q);
								free(M);
								t2=clock();						
								diff = (((float)t2 - (float)t1) / 1000000.0F ) * 1000;   
								//printf("bfs %f %d\n",diff, debug); 
								int new_size=0;
								for (int m=0; m<tot_molecule; m++) {
									if (pDataArray1->molecule_state[m].complex==tmp_c[i]) new_size++;
								}
								for (int m=0; m<tot_molecule; m++) {
									if (pDataArray1->molecule_state[m].complex==tmp_c[i]) pDataArray1->molecule_state[m].size=new_size;
								}
								if (debug>200000) {
									printf("new size = %d - mu=%d - \n", new_size, mu[i]);
								}
						
							}
							else {
								debug2++;
								for (int m=0; m<tot_molecule; m++) {
									if (pDataArray1->molecule_state[m].id==pDataArray1->site_state[s1].mol) {
										pDataArray1->molecule_state[m].complex=0;
										pDataArray1->molecule_state[m].size=1;
									}
								}
							}
							if (bounded2) {
								for (int m=0; m<tot_molecule; m++) {
									if (pDataArray1->molecule_state[m].id==pDataArray1->site_state[s2].mol) {
										complex=pDataArray1->molecule_state[m].complex;
									}
								}
								debug1++;
								EnterCriticalSection( &cs_mutex );
								 // lock
									complex_count++;
									tmp_c[i]=complex_count;
								LeaveCriticalSection( &cs_mutex );
								debug=0;
								t1=clock();
								queue* Q;
								queue* M;
								Q=(queue*)malloc(sizeof(queue));
								M=(queue*)malloc(sizeof(queue));
								init(Q);
								initM(M);
								int curmol;
								enqueue(Q, pDataArray1->site_state[s2].mol);
								molecule_state[pDataArray1->site_state[s2].mol].complex=tmp_c[i];
								//enqueueM(M, pDataArray1->site_state[s2].mol); //mark it
								while(!emptyp(Q)) {
									if (dequeue(Q, &curmol)==FAIL) printf("error queue\n");
									//pDataArray1->molecule_state[curmol].complex=tmp_c[i];
									int ind=0;
									int s;
									while (StoM[curmol][ind]!=-1) {
									//for (s=0; s<tot_site; s++) {
										s=StoM[curmol][ind];
										debug++;
										if (pDataArray1->site_state[s].mol==curmol && pDataArray1->site_state[s].specieBounded!=-1) {
										//if (pDataArray1->site_state[s].specieBounded!=-1) {
											//if not marked
											//if (!inQueue(M, pDataArray1->site_state[s].bound->mol)) {
											if (pDataArray1->molecule_state[pDataArray1->site_state[s].bound->mol].complex!=tmp_c[i]) {
												//mark it
												//enqueueM(M, pDataArray1->site_state[s].bound->mol);
												enqueue(Q, pDataArray1->site_state[s].bound->mol);
												pDataArray1->molecule_state[pDataArray1->site_state[s].bound->mol].complex=tmp_c[i];
											}

										}
										ind++;
									}
								}
								deleteM(M);
								deleteM(Q);
								free(Q);
								free(M);
								t2=clock();						
								diff = (((float)t2 - (float)t1) / 1000000.0F ) * 1000;   
								//printf("bfs %f %d\n",diff,debug); 
								int new_size=0;
								for (int m=0; m<tot_molecule; m++) {
									if (pDataArray1->molecule_state[m].complex==tmp_c[i]) new_size++;
								}
								for (int m=0; m<tot_molecule; m++) {
									if (pDataArray1->molecule_state[m].complex==tmp_c[i]) pDataArray1->molecule_state[m].size=new_size;
								}
								if (debug>200000) {
									printf("new size = %d - mu=%d - \n", new_size, mu[i]);
								}
							}
							else {
								debug2++;
								for (int m=0; m<tot_molecule; m++) {
									if (pDataArray1->molecule_state[m].id==pDataArray1->site_state[s2].mol) {
										pDataArray1->molecule_state[m].complex=0;
										pDataArray1->molecule_state[m].size=1;
									}
								}
					
							}
							/*int debug3=0;
							int debug4=0;
							int debug5=0;
							for (int m=0; m<tot_molecule; m++) {
								if (pDataArray1->molecule_state[m].complex!=0) {
									if (pDataArray1->molecule_state[pDataArray1->site_state[s1].mol].complex==pDataArray1->molecule_state[m].complex) debug3++;
									if (pDataArray1->molecule_state[pDataArray1->site_state[s2].mol].complex==pDataArray1->molecule_state[m].complex) debug4++;
									if (complex==pDataArray1->molecule_state[m].complex) debug5++;
								}
							}
							if (debug3==1 || debug4==1) {
								printf("\t>>>>>>>ERROR complex 1, m1=%d, m2=%d, complex1=%d, complex2=%d, degree1=%d, degree2=%d\n", 
									pDataArray1->site_state[s1].mol, pDataArray1->site_state[s2].mol,
									pDataArray1->molecule_state[pDataArray1->site_state[s1].mol].complex, pDataArray1->molecule_state[pDataArray1->site_state[s2].mol].complex,
									pDataArray1->molecule_state[pDataArray1->site_state[s1].mol].degree, pDataArray1->molecule_state[pDataArray1->site_state[s2].mol].degree);
								printf("s1.specieBounded=%d\n, s2.specieBounded=%d, debug3=%d, debug4=%d\n", pDataArray1->site_state[s1].specieBounded, pDataArray1->site_state[s2].specieBounded, debug3, debug4);
								printf("size of old complex %d\n", debug5);
								int totb1=0;
								int totb2=0;
								printf("totb1=%d,totb2=%d\n",totb1, totb2);
								exit(0);
							}
					
							for  (int x=0; x<tot_site; x++) {
								//if (pDataArray1->site_state[x].bound!=NULL && pDataArray1->site_state[x].cell>=pDataArray1->i*(CELLS/8) && pDataArray1->site_state[x].cell<(pDataArray1->i+1)*(CELLS/8)) {
								if (pDataArray1->site_state[x].bound!=NULL && pDataArray1->site_state[x].cell==i) {
									if (pDataArray1->molecule_state[pDataArray1->site_state[x].mol].complex!=pDataArray1->molecule_state[pDataArray1->site_state[x].bound->mol].complex) {
										printf("erorr complex DMpart2UN %d - %d\n", pDataArray1->site_state[x].cell, pDataArray1->site_state[x].bound->cell);
										printf("erorr complex DMpart2UN %d - %d - %d - %d\n", pDataArray1->molecule_state[pDataArray1->site_state[x].mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[x].bound->mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[x].mol].complex, pDataArray1->molecule_state[pDataArray1->site_state[x].bound->mol].complex);
										printf("current cell = %d\n", i);
										printf("debug1 %d - debug2 %d\n", debug1, debug2);
										printf("s1 = %d s2 = %d cur = %d\n", s1, s2,x);
										printf("m1 = %d m2 = %d\n", pDataArray1->site_state[x].mol, pDataArray1->site_state[x].bound->mol);
										printf("m1c = %d m2c = %d\n", pDataArray1->site_state[s1].mol, pDataArray1->site_state[s2].mol);
										exit(0);
									}
								}
							}*/
						}
						else {
							mu[i]=mu[i]/2;
							if (mu[i]>=reacts || mu[i]<0) printf("ERROR mu=%d\n", mu[i]);
							if (g_a[i*reacts*2+mu[i]*2]==0) printf("error binding with propensity 0 in reaction %d\n", mu[i]);
							//EnterCriticalSection( &cs_cb );
							//		cb++;
							//LeaveCriticalSection( &cs_cb );
							int s1=0;
							int s2=0;
							u[i]=rand() / double(RAND_MAX);
							chooseSite(pDataArray1->v[mu[i]*2], pDataArray1->v[mu[i]*2+1], &s1, &s2, u[i], pDataArray1->site_state, i, pDataArray1->abdS, mu[i]);
					
							if (pDataArray1->site_state[s1].cell!=pDataArray1->site_state[s2].cell) {
								printf(">>>>>>>>>ERROR BINDING BETWEEN DIFFERENT CELLS\n");
							}
							pDataArray1->site_state[s1].bound=&pDataArray1->site_state[s2];
							pDataArray1->site_state[s2].bound=&pDataArray1->site_state[s1];
							pDataArray1->site_state[s2].specieBounded=pDataArray1->site_state[s1].specie;
							pDataArray1->site_state[s1].specieBounded=pDataArray1->site_state[s2].specie;
							EnterCriticalSection( &cs_mutex );
								// lock
								complex_count++;
								tmp_c[i]=complex_count;
							LeaveCriticalSection( &cs_mutex );
							int m_inv1=pDataArray1->site_state[s1].mol;
							int m_inv2=pDataArray1->site_state[s2].mol;
							if (pDataArray1->molecule_state[m_inv1].cell!=i || pDataArray1->molecule_state[m_inv1].cell!=i) {
								printf("writing on the wrong cell\n");
							}
							int debug1=0;
							int debug2=0;
							int debug_a=0;
							int debug_b=0;
							int debug_c=0;
							int debug_d=0;
							//int debug_e[2];
							int new_size=0;
							pDataArray1->molecule_state[m_inv1].degree++;
							if (pDataArray1->molecule_state[m_inv1].complex!=0) {
								updateComplex(pDataArray1->molecule_state, pDataArray1->molecule_state[m_inv1].complex, i, tmp_c[i]);
								debug_a++;
							}
							else {
								pDataArray1->molecule_state[m_inv1].complex=tmp_c[i];
								debug_b++;	
								debug_c=pDataArray1->molecule_state[m_inv1].complex;
								debug_d=pDataArray1->molecule_state[m_inv2].complex;	
								if (pDataArray1->molecule_state[m_inv1].complex!=tmp_c[i]) {
									printf(">complex changedA\n");	
								}			
							}

							pDataArray1->molecule_state[m_inv2].degree++;
							if (pDataArray1->molecule_state[m_inv2].complex!=0) {
								updateComplex(pDataArray1->molecule_state, pDataArray1->molecule_state[m_inv2].complex, i, tmp_c[i]);
								debug_a++;
							}
							else {
								pDataArray1->molecule_state[m_inv2].complex=tmp_c[i];
								debug_b++;	
								debug_c=pDataArray1->molecule_state[m_inv2].complex;
								debug_d=pDataArray1->molecule_state[m_inv2].complex;			
								if (pDataArray1->molecule_state[m_inv2].complex!=tmp_c[i]) {
									printf("complex changedA\n");	
								}
							}
							if (pDataArray1->molecule_state[m_inv2].complex!=tmp_c[i] || pDataArray1->molecule_state[m_inv1].complex!=tmp_c[i]) {
								printf("complex changed\n");	
							}
							/*for (int m=0, short f=0; m<tot_molecule && f<2; m++) {
								if (pDataArray1->molecule_state[m].id==m_inv1 || pDataArray1->molecule_state[m].id==m_inv2) {
									f++;
									pDataArray1->molecule_state[m].degree++;
									debug1++;
									debug2=pDataArray1->molecule_state[m].degree;
									if (pDataArray1->molecule_state[m].complex!=0) {
										updateComplex(pDataArray1->molecule_state, pDataArray1->molecule_state[m].complex, i, tmp_c[i]);
										debug_a++;
										//debug_c=m;
									}
									else {
										pDataArray1->molecule_state[m].complex=tmp_c[i];
										debug_b++;
										//debug_e[f]=m;
										//debug_d=m;
										
										if (f==1) {
											debug_c=pDataArray1->molecule_state[m].complex;
										} else if (f==2) {
											debug_d=pDataArray1->molecule_state[m].complex;
											
											if (pDataArray1->molecule_state[m_inv2].complex!=tmp_c[i]) {
												printf("complex changedA\n");	
											}
										}
									}
									
									if (pDataArray1->molecule_state[m].degree!=debug2) {
										printf("ERROR binding1 %d\n", pDataArray1->molecule_state[m].degree);
										exit(0);
									}
							
								}
							}*/
							for (int m=0; m<tot_molecule; m++) {
								if (pDataArray1->molecule_state[m].complex==tmp_c[i]) {
									new_size++;
								}
							}
							for (int m=0; m<tot_molecule; m++) {
								if (pDataArray1->molecule_state[m].complex==tmp_c[i]) {
									pDataArray1->molecule_state[m].size=new_size;
								}
							}
							
							if (pDataArray1->molecule_state[m_inv2].complex!=tmp_c[i] || pDataArray1->molecule_state[m_inv1].complex!=tmp_c[i]) {
								printf("complex changedB\n");	
							}
							if ((pDataArray1->molecule_state[pDataArray1->site_state[s2].mol].degree == 0 ) || (pDataArray1->molecule_state[pDataArray1->site_state[s1].mol].degree == 0 )) {
								printf("ERROR binding 3\n");
							}
							
							/*for  (int x=0; x<tot_site; x++) {
								//if (pDataArray1->site_state[x].bound!=NULL && pDataArray1->site_state[x].cell>=pDataArray1->i*(CELLS/8) && pDataArray1->site_state[x].cell<(pDataArray1->i+1)*(CELLS/8)) {
								if (pDataArray1->site_state[x].bound!=NULL && pDataArray1->site_state[x].cell==i) {
									if (pDataArray1->molecule_state[pDataArray1->site_state[x].mol].complex!=pDataArray1->molecule_state[pDataArray1->site_state[x].bound->mol].complex) {
										printf("erorr complex DMpart2 %d - %d\n", pDataArray1->site_state[x].cell, pDataArray1->site_state[x].bound->cell);
										printf("erorr complex DMpart2 %d - %d - %d - %d\n", pDataArray1->molecule_state[pDataArray1->site_state[x].mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[x].bound->mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[x].mol].complex, pDataArray1->molecule_state[pDataArray1->site_state[x].bound->mol].complex);
										printf("current cell = %d\n", i);
										printf("s1 = %d s2 = %d cur = %d\n", s1, s2,x);
										printf("m1 = %d m2 = %d\n", pDataArray1->site_state[x].mol, pDataArray1->site_state[x].bound->mol);
										//printf("d1=%d, d2=%d, d3=%d, d4=%d, de1=%d, de2=%d\n", debug_a, debug_b, debug_c, debug_d, debug_e[0], debug_e[1]);
										printf("d1=%d, d2=%d, d3=%d, d4=%d\n", debug_a, debug_b, debug_c, debug_d);
										printf("current compl = %d size %d\n", tmp_c[i], new_size);
										exit(0);
									}
								}
							}*/
						
						}
						/*for  (int x=0; x<tot_site; x++) {
							//if (pDataArray1->site_state[x].bound!=NULL && pDataArray1->site_state[x].cell>=pDataArray1->i*(CELLS/8) && pDataArray1->site_state[x].cell<(pDataArray1->i+1)*(CELLS/8)) {
							if (pDataArray1->site_state[x].bound!=NULL && pDataArray1->site_state[x].cell==i) {
								if (pDataArray1->molecule_state[pDataArray1->site_state[x].mol].complex!=pDataArray1->molecule_state[pDataArray1->site_state[x].bound->mol].complex) {
									printf("erorr complex DMpart2a %d - %d\n", pDataArray1->site_state[x].cell, pDataArray1->site_state[x].bound->cell);
									printf("erorr complex DMpart2a %d - %d - %d - %d\n", pDataArray1->molecule_state[pDataArray1->site_state[x].mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[x].bound->mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[x].mol].complex, pDataArray1->molecule_state[pDataArray1->site_state[x].bound->mol].complex);
									printf("current cell = %d\n", i);
									exit(0);
								}
							}
						}*/
						//for (int i=0; i<nspec; i++) {
							//speciesAbd[CellID][i]+=v[mu][i];
						//}
						
					}
					//printf("t %f, tau %f\n", t[i], tau[i]);
					t[i]+=tau[i];
					if (t[i]>pDataArray1->tmax) completed[i]=true;

				}
				
			}
	}
	
						/*	for  (int i=0; i<tot_site; i++) {
								if (pDataArray1->site_state[i].bound!=NULL && pDataArray1->site_state[i].cell>=pDataArray1->i*(CELLS/8) && pDataArray1->site_state[i].cell<(pDataArray1->i+1)*(CELLS/8)) {
									if (pDataArray1->molecule_state[pDataArray1->site_state[i].mol].complex!=pDataArray1->molecule_state[pDataArray1->site_state[i].bound->mol].complex) {
										printf("erorr complex DMpart2b %d - %d\n", pDataArray1->site_state[i].cell, pDataArray1->site_state[i].bound->cell);
										printf("erorr complex DMpart2b %d - %d - %d - %d\n", pDataArray1->molecule_state[pDataArray1->site_state[i].mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[i].bound->mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[i].mol].complex, pDataArray1->molecule_state[pDataArray1->site_state[i].bound->mol].complex);
										exit(0);
									}
								}
							}*/
	return 0;
}
DWORD WINAPI DM(LPVOID lpParam) {

	PMYDATA1 pDataArray1 = (PMYDATA1)lpParam;
	
	DWORD dwThrdId;
	//pDataArrayDM
	//int species=10;
	//int reactions=10;
	long t1, t2;
	srand ( time(NULL) );
	int** rec = (int**)malloc(sizeof(int*)*CELLS);
	
	for (int i=0; i<CELLS; i++) {
		rec[i]=(int*)malloc(sizeof(int)*reacts);
	}
	bool c=false;
	for (int i=0; i<CELLS; i++) {
		mu[i]=-2;
		t[i]=0;
		completed[i]=false;
	}
	float* a0_debug=(float*)malloc(sizeof(float));
	while (!c) {
		int tot_rec=0;
		t1=clock();
		for (int i=0; i<CELLS; i++) {
			for (int j=0; j<reacts; j++) {
				rec[i][j]=0;
			}
		}

		for (int i=0; i<CELLS; i++) {
			if (mu[i]!=-2 && !completed[i]) {
				
				for (int j=0; j<reacts; j++) {

					if ((v[mu[i]*2]==v[j*2]||v[mu[i]*2]==v[j*2+1])||(v[mu[i]*2+1]==v[j*2]||v[mu[i]*2+1]==v[j*2+1])) {
						tot_rec++;
						//binding_CPU(a0_debug, pDataArray1->site_state, pDataArray1->molecule_state, pDataArray1->v, pDataArray1->c, pDataArray1->cu, t[i], pDataArray1->abdS, pDataArray1->i*(CELLS)+i, pDataArray1->a, j, i, pDataArray1->i);
						//unbinding_CPU(pDataArray1->site_state, pDataArray1->molecule_state, pDataArray1->v, pDataArray1->c, pDataArray1->cu, t[i], pDataArray1->abdS, pDataArray1->i*(CELLS)+i, pDataArray1->a, j, i);
						
						//g_a[((pDataArray1->i)*(CELLS)+i)*reacts*2+j*2]=pDataArray1->a[i*reacts*2+j*2];
						//g_a[((pDataArray1->i)*(CELLS/CPU_THREADS)+i)*reacts*2+j*2+1]=pDataArray1->a[i*reacts*2+j*2+1];
						
						
						rec[(pDataArray1->i)*(CELLS)+i][j]=1;
						rec[(pDataArray1->i)*(CELLS)+i][j]=1;

					}
					
				}
			}
		}
		if (tot_rec<1000) {
			
			for (int i=0; i<CELLS; i++) {
				if (mu[i]!=-2 && !completed[i]) {
				
					for (int j=0; j<reacts; j++) {

						if ((v[mu[i]*2]==v[j*2]||v[mu[i]*2]==v[j*2+1])||(v[mu[i]*2+1]==v[j*2]||v[mu[i]*2+1]==v[j*2+1])) {
							
							binding_CPU(a0_debug, pDataArray1->site_state, pDataArray1->molecule_state, pDataArray1->v, pDataArray1->c, pDataArray1->cu, t[i], pDataArray1->abdS, pDataArray1->i*(CELLS)+i, pDataArray1->a, j, i, pDataArray1->i);
							unbinding_CPU(pDataArray1->site_state, pDataArray1->molecule_state, pDataArray1->v, pDataArray1->c, pDataArray1->cu, t[i], pDataArray1->abdS, pDataArray1->i*(CELLS)+i, pDataArray1->a, j, i);
						
							g_a[((pDataArray1->i)*(CELLS)+i)*reacts*2+j*2]=pDataArray1->a[i*reacts*2+j*2];
							g_a[((pDataArray1->i)*(CELLS/CPU_THREADS)+i)*reacts*2+j*2+1]=pDataArray1->a[i*reacts*2+j*2+1];
						

						}
					
					}
				}
			}
		}
		else {
		 cudaMemcpy(d_site_state, site_state, sizeof(site)*tot_site, cudaMemcpyHostToDevice);
		 cudaMemcpy(d_molecule_state, molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyHostToDevice);

			rec_t* rec_gpu2=(rec_t*)malloc(sizeof(rec_t)*tot_rec);
			int recs=0;
			for (int i=0; i<CELLS; i++) {
				for (int j=0; j<reacts; j++) {
					if (rec[(pDataArray1->i)*(CELLS)+i][j]==1) {
						rec_gpu2[recs].cellID=(pDataArray1->i)*(CELLS)+i; 
						rec_gpu2[recs].react=j;
						recs++;
					}
				}
			}
			int tr;
			rec_t* d_rec2;
			float* d_a3;
			cudaMalloc((void**)&d_rec2, sizeof(rec_t)*tot_rec) ;
			cudaMemcpy(d_rec2, rec_gpu2, sizeof(rec_t)*tot_rec, cudaMemcpyHostToDevice) ;
			cudaMalloc((void**)&d_a3, sizeof(float)*tot_rec*2 ) ;
			int tot_kernel=floor((float)(tot_rec/(512*64)));
			int threads_r=tot_rec-(tot_kernel*512*64);
			for (tr=0; tr<tot_kernel; tr++) {
				binding2<<<512,64>>> (d_a0, d_site_state, d_molecule_state, d_v, d_c, d_cu, t[0], d_abdS, tot_rec, d_a3, tr, reacts, tot_molecule,d_rec2);
				unbinding2<<<512,64>>> (d_site_state, d_molecule_state, d_v, d_c, d_cu, t[0], d_abdS, tot_rec, d_a3, tr, reacts,d_rec2);	
				cudaThreadSynchronize();
			}
			if (threads_r>0) {
				//int nThr=floor((float)threads_r/512);
				binding2<<<512,64>>> (d_a0, d_site_state, d_molecule_state, d_v, d_c, d_cu, t[0], d_abdS, tot_rec, d_a3, tr, reacts, tot_molecule, d_rec2);
				unbinding2<<<512,64>>> (d_site_state, d_molecule_state, d_v, d_c, d_cu, t[0], d_abdS, tot_rec, d_a3, tr, reacts, d_rec2);
				//if (nThr*512-threads_r>0) {
					
				//}
			}  
			//cudaThreadSynchronize();
			float* thr_a2 = (float*)malloc(sizeof(float)*tot_rec*2);
			cudaMemcpy(thr_a2, d_a3, sizeof(float)*tot_rec*2, cudaMemcpyDeviceToHost) ;
			cudaFree(d_rec2);
			cudaFree(d_a3);
			float sum_debug=0;
			for (int d=0; d<tot_rec; d++) {
			//	if (g_a[rec_gpu2[d].cellID*reacts*2+rec_gpu2[d].react*2]!=thr_a2[d*2]) printf("debug message: error CPU DM\n");
			//	if (g_a[rec_gpu2[d].cellID*reacts*2+rec_gpu2[d].react*2+1]!=thr_a2[d*2+1]) printf("debug message: error CPU DM\n");
				g_a[rec_gpu2[d].cellID*reacts*2+rec_gpu2[d].react*2]=thr_a2[d*2];
				g_a[rec_gpu2[d].cellID*reacts*2+rec_gpu2[d].react*2+1]=thr_a2[d*2+1];
				pDataArray1->a[rec_gpu2[d].cellID*reacts*2+rec_gpu2[d].react*2]=thr_a2[d*2];
				pDataArray1->a[rec_gpu2[d].cellID*reacts*2+rec_gpu2[d].react*2+1]=thr_a2[d*2+1];
			}
			free(thr_a2);
			free(rec_gpu2);
		
		
		}
		
		t2 = clock();   
		
		/*for  (int i=0; i<tot_site; i++) {
			if (pDataArray1->site_state[i].bound!=NULL) {
				if (pDataArray1->molecule_state[pDataArray1->site_state[i].mol].complex!=pDataArray1->molecule_state[pDataArray1->site_state[i].bound->mol].complex) {
					printf("erorr complexDM1 %d - %d\n", pDataArray1->site_state[i].cell, pDataArray1->site_state[i].bound->cell);
					printf("erorr complexDM1 %d - %d - %d - %d\n", pDataArray1->molecule_state[pDataArray1->site_state[i].mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[i].bound->mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[i].mol].complex, pDataArray1->molecule_state[pDataArray1->site_state[i].bound->mol].complex);
					exit(0);
				}
			}
		}*/
		float diff = (((float)t2 - (float)t1) / 1000000.0F ) * 1000;   
		printf("prop %f\n",diff); 
		t1 = clock();   
		//printf("tot rec DM %d\n", tot_rec);
		for (int i=0; i<8; i++) {
			
			pDataArrayDM[i] = (PMYDATA1)malloc(sizeof(MYDATA1));
				//if( pDataArray[i] == NULL )
			if( pDataArray[0] == NULL )
			{
				ExitProcess(2);
			}
			pDataArrayDM[i]->site_state=pDataArray[0]->site_state;
			pDataArrayDM[i]->molecule_state=pDataArray[0]->molecule_state;
			pDataArrayDM[i]->v=pDataArray[0]->v;
			pDataArrayDM[i]->c=pDataArray[0]->c;
			pDataArrayDM[i]->cu=pDataArray[0]->cu;
			pDataArrayDM[i]->abdS=pDataArray[0]->abdS;
			pDataArrayDM[i]->speciesBounded=pDataArray[0]->speciesBounded;
			pDataArrayDM[i]->tmax=pDataArray1->tmax;
			pDataArrayDM[i]->i=i;
			//pDataArrayDM[i]->a=(float*)malloc(sizeof(float)*reacts*2*(CELLS/8));
			//for (int d=0; d<(CELLS/8)*reacts*2;d++) pDataArrayDM[i]->a[d]=g_a[i*(CELLS/8)*reacts*2+d]; 
			DMThread[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)DMpart2, pDataArrayDM[i], 0, &dwThrdId);
		}
		DWORD dwRet=WaitForMultipleObjects(8,DMThread,TRUE,INFINITE); 
		//for (int i=0; i<CELLS; i++) {
		//	
		//}
		switch(dwRet) {
        case WAIT_OBJECT_0 + 0:
            //wprintf(L"Events was signaled...\n");
            break;
        case WAIT_TIMEOUT:
            wprintf(L"The waiting is timed out...\n");
        break;
        default:
            wprintf(L"Waiting failed, error %d...\n", GetLastError());
            ExitProcess(0);
		}      
		
		t2 = clock();   
		
		/*for  (int i=0; i<tot_site; i++) {
			if (pDataArray1->site_state[i].bound!=NULL) {
				if (pDataArray1->molecule_state[pDataArray1->site_state[i].mol].complex!=pDataArray1->molecule_state[pDataArray1->site_state[i].bound->mol].complex) {
					printf("erorr complexDM2 %d - %d\n", pDataArray1->site_state[i].cell, pDataArray1->site_state[i].bound->cell);
					printf("erorr complexDM2 %d - %d - %d - %d\n", pDataArray1->molecule_state[pDataArray1->site_state[i].mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[i].bound->mol].cell, pDataArray1->molecule_state[pDataArray1->site_state[i].mol].complex, pDataArray1->molecule_state[pDataArray1->site_state[i].bound->mol].complex);
					exit(0);
				}
			}
		}*/
		diff = (((float)t2 - (float)t1) / 1000000.0F ) * 1000;   
		printf("bind/unbind %f\n",diff); 
		
		c=true;
		for (int i=0; i<CELLS; i++) {
			if (t[i]>pDataArray1->tmax) {
				completed[i]=true;
				//printf("%d in %d completed\n", i,pDataArray1->i );
			}
			c=c&&completed[i];
		}

	}
	//printf("%d is over\n", pDataArray1->i);
	
	for (int i=0; i<CELLS; i++) {
		free(rec[i]);
	}
	free(rec);
	free(a0_debug);
	free(pDataArray1->a);
	return 0; 
}




DWORD WINAPI DM2(LPVOID lpParam) {
	//int** s_speciesAbd,int** s_v, const float* s_c, float tmax, float* s_deb, int i
	PMYDATA1 pDataArray1 = (PMYDATA1)lpParam;
	//int species=10;
	//int reactions=10;
	int tmp_c;
	srand ( time(NULL) );
	/*int** rec = (int**)malloc(sizeof(int*)*CELLS);
	for (int i=0; i<CELLS; i++) {
		rec[i]=(int*)malloc(sizeof(int)*reacts);
	}*/
	double t=0;
	double u,u2,tau;
	//s_deb[0]=t_max;
	double a0;
	int mu=-2;
	//if (pDataArray1->i==0) printf("debug0\n");
	float* a0_debug=(float*)malloc(sizeof(float));
	for (int i=0; i<CELLS/CPU_THREADS; i++) {
		t=0;
		//if (pDataArray1->i==0) printf("debug1\n");
		while (t<pDataArray1->tmax) {
			//if (pDataArray1->i==0) printf("debug2\n");
			/* cudaMemcpy(a, d_a, sizeof(float)*CELLS*REACTIONS*2, cudaMemcpyDeviceToHost) );
			a0=0;
			int cell=pDataArray1->i*(CELLS/CPU_THREADS)+i;
			for(int j=0; j<REACTIONS; j++) {
				a0+=a[cell*REACTIONS*2+j];
			}*/
			//binding<<<CELLS/CPU_THREADS,REACTIONS>>> (pDataArray1->devStates, pDataArray1->site_state, pDataArray1->molecule_state, pDataArray1->v, pDataArray1->c, pDataArray1->cu, pDataArray1->tmax, pDataArray1->abdS,pDataArray1->i);
			//binding<<<1,1>>> (pDataArray1->devStates, pDataArray1->site_state, pDataArray1->molecule_state, pDataArray1->v, pDataArray1->c, pDataArray1->cu, pDataArray1->tmax, pDataArray1->abdS,pDataArray1->i);
			//unbinding for all cell*reactions
			//cudaThreadSynchronize());
			//unbinding<<<CELLS/CPU_THREADS,REACTIONS>>> (pDataArray1->devStates, pDataArray1->site_state, pDataArray1->molecule_state, pDataArray1->v, pDataArray1->c, pDataArray1->cu, pDataArray1->tmax, pDataArray1->abdS,pDataArray1->i);
			//cudaThreadSynchronize());
			if (mu!=-2) {
				//int tot_rec=0;
				
				//for (int i=0; i<CELLS; i++) {
					//for (int j=0; j<reacts; j++) {
					//	rec[(pDataArray1->i)*(CELLS/CPU_THREADS)+i][j]=0;
					//}
				//}
				for (int j=0; j<reacts; j++) {
					//if (mu==(j/2)|| mu==(j/2+REACTIONS)) {}
					//if ((v[mu*2]==v[j]||v[mu*2]==v[j+1])||(v[mu*2+1]==v[j]||v[mu*2+1]==v[j+1])) {

					if ((v[mu*2]==v[j*2]||v[mu*2]==v[j*2+1])||(v[mu*2+1]==v[j*2]||v[mu*2+1]==v[j*2+1])) {
						//tot_rec++;
						binding_CPU(a0_debug, pDataArray1->site_state, pDataArray1->molecule_state, pDataArray1->v, pDataArray1->c, pDataArray1->cu, t, pDataArray1->abdS, pDataArray1->i*(CELLS/CPU_THREADS)+i, pDataArray1->a, j, i, pDataArray1->i);
						unbinding_CPU(pDataArray1->site_state, pDataArray1->molecule_state, pDataArray1->v, pDataArray1->c, pDataArray1->cu, t, pDataArray1->abdS, pDataArray1->i*(CELLS/CPU_THREADS)+i, pDataArray1->a, j, i);
						
						g_a[((pDataArray1->i)*(CELLS/CPU_THREADS)+i)*reacts*2+j*2]=pDataArray1->a[i*reacts*2+j*2];
						g_a[((pDataArray1->i)*(CELLS/CPU_THREADS)+i)*reacts*2+j*2+1]=pDataArray1->a[i*reacts*2+j*2+1];
						
						//rec[(pDataArray1->i)*(CELLS/CPU_THREADS)+i][j]=1;
						//rec[(pDataArray1->i)*(CELLS/CPU_THREADS)+i][j]=1;
					}
					
				}
			/*
				rec_t* rec_gpu2=(rec_t*)malloc(sizeof(rec_t)*tot_rec);
				int recs=0;
				for (int j=0; j<reacts; j++) {
					if (rec[(pDataArray1->i)*(CELLS/CPU_THREADS)+i][j]==1) {
						rec_gpu2[recs].cellID=(pDataArray1->i)*(CELLS/CPU_THREADS)+i; 
						rec_gpu2[recs].react=j;
						recs++;
					}
				}

					int tr;
					rec_t* d_rec2;
					float* d_a3;
					cudaMalloc((void**)&d_rec2, sizeof(rec_t)*tot_rec) ;
					cudaMemcpy(d_rec2, rec_gpu2, sizeof(rec_t)*tot_rec, cudaMemcpyHostToDevice) ;
					cudaMalloc((void**)&d_a3, sizeof(float)*tot_rec*2 ) ;
					int tot_kernel=floor((float)(tot_rec/(512*64)));
					int threads_r=tot_rec-(tot_kernel*512*64);
					for (tr=0; tr<tot_kernel; tr++) {
						binding2<<<512,64>>> (d_a0, d_site_state, d_molecule_state, d_v, d_c, d_cu, t, d_abdS, tot_rec, d_a3, tr, reacts, tot_molecule,d_rec2);
						unbinding2<<<512,64>>> (d_site_state, d_molecule_state, d_v, d_c, d_cu, t, d_abdS, tot_rec, d_a3, tr, reacts,d_rec2);	
						cudaThreadSynchronize();
					}
					if (threads_r>0) {
						//int nThr=floor((float)threads_r/512);
						binding2<<<512,64>>> (d_a0, d_site_state, d_molecule_state, d_v, d_c, d_cu, t, d_abdS, tot_rec, d_a3, tr, reacts, tot_molecule, d_rec2);
						unbinding2<<<512,64>>> (d_site_state, d_molecule_state, d_v, d_c, d_cu, t, d_abdS, tot_rec, d_a3, tr, reacts, d_rec2);
						//if (nThr*512-threads_r>0) {
					
						//}
					}  
					cudaThreadSynchronize();
					float* thr_a2 = (float*)malloc(sizeof(float)*tot_rec*2);
					cudaMemcpy(thr_a2, d_a3, sizeof(float)*tot_rec*2, cudaMemcpyDeviceToHost) ;
					cudaFree(d_rec2);
					cudaFree(d_a3);
					float sum_debug=0;
					for (int d=0; d<tot_rec; d++) {
						//if (g_a[rec_gpu[d].cellID*reacts*2+rec_gpu[d].react*2]!=thr_a2[d*2]) printf("debug message: error CPU DM\n");
						//if (g_a[rec_gpu[d].cellID*reacts*2+rec_gpu[d].react*2+1]!=thr_a2[d*2+1]) printf("debug message: error CPU DM\n");
					}
					free(thr_a2);
					free(rec_gpu2);*/
			}
			a0=0;
			for (int j=0; j<reacts; j++) {
				if (pDataArray1->a[i*reacts*2+j*2]<-0.1 || pDataArray1->a[i*reacts*2+j*2]>0.1) {
					a0+=pDataArray1->a[i*reacts*2+j*2]; //errore non conta gli unbind
					a0+=pDataArray1->a[i*reacts*2+j*2+1];
				}
			}
			//if (a0==0) break;
			//if (pDataArray1->i==0) printf("debug3\n");
			if (a0<0.001 && a0>-0.001) break;
			//if (pDataArray1->i==0) printf("debug4\n");
			//printf("a0=%f\n", a0);
			u=rand() / double(RAND_MAX);
			u2=rand() / double(RAND_MAX);
			tau=Exponential(a0,u);
			//printf("tau = %f\n", tau);
			if (tau+t<=pDataArray1->tmax) {
				mu=Weights(pDataArray1->a,a0*u2, i);
				if (mu>=reacts*2 || mu<0) printf("ERROR mu=%d a0=%f tau=%f\n", mu,a0,tau);
				if (mu==-1) break;
				if (mu%2!=0) {
					mu=(mu-1)/2;
					EnterCriticalSection( &cs_cu );
							cun++;
					LeaveCriticalSection( &cs_cu );
					int s1=0;
					int s2=0;
					u=rand() / double(RAND_MAX);
					chooseSite_u(pDataArray1->v[mu*2], pDataArray1->v[mu*2+1], &s1, &s2, u, pDataArray1->site_state, pDataArray1->i*(CELLS/CPU_THREADS)+i, pDataArray1->abdS);
					if (pDataArray1->site_state[s1].specieBounded==-1 || pDataArray1->site_state[s2].specieBounded==-1 || pDataArray1->site_state[s1].bound==NULL || pDataArray1->site_state[s2].bound==NULL) printf("error not bounded\n");
					int debug0=0;
					for (int m=0; m<tot_molecule; m++) {
						if (pDataArray1->molecule_state[m].id==pDataArray1->site_state[s1].mol || pDataArray1->molecule_state[m].id==pDataArray1->site_state[s2].mol) {
							if (pDataArray1->molecule_state[m].degree==0) {
								printf("error unbinding impossible: reaction %d, mol %d s1 = %d, s2 = %d, specieBounded1 = %d, specieBounded2 = %d, specie1=%d, specie2=%d\n", mu, m, s1, s2, pDataArray1->site_state[s1].specieBounded, pDataArray1->site_state[s2].specieBounded, pDataArray1->site_state[s1].specie, pDataArray1->site_state[s2].specie);
								printf("mol id %d, %d, m=%d\n", pDataArray1->site_state[s1].mol, pDataArray1->site_state[s2].mol, m);
								printf("v[mu*2]=%d, v[mu*2+1]=%d\n", v[mu*2], v[mu*2+1]);
								
							}
							pDataArray1->molecule_state[m].degree--;
							debug0++;
						}
					}
					if (debug0!=2) printf("ERROR debug 0\n", mu);
					pDataArray1->site_state[s1].bound=NULL;
					pDataArray1->site_state[s2].bound=NULL;
					pDataArray1->site_state[s1].specieBounded=-1;
					pDataArray1->site_state[s2].specieBounded=-1;
					bool bounded1=false;
					bool bounded2=false;
					for (int s=0; s<tot_site && (!bounded1||!bounded2); s++) {
						if (pDataArray1->site_state[s].mol==pDataArray1->site_state[s1].mol && pDataArray1->site_state[s].specieBounded!=-1) bounded1=true;
						if (pDataArray1->site_state[s].mol==pDataArray1->site_state[s2].mol && pDataArray1->site_state[s].specieBounded!=-1) bounded2=true;

					}
					int complex=pDataArray1->molecule_state[pDataArray1->site_state[s1].mol].complex;
					if (bounded1) {
						for (int m=0; m<tot_molecule; m++) {
							if (pDataArray1->molecule_state[m].id==pDataArray1->site_state[s1].mol) {
								complex=pDataArray1->molecule_state[m].complex;
							}
						}
						EnterCriticalSection( &cs_mutex );
							complex_count++;
							tmp_c=complex_count;
						LeaveCriticalSection( &cs_mutex );
						//bfs starting from s1.mol
						queue* Q;
						queue* M;
						Q=(queue*)malloc(sizeof(queue));
						M=(queue*)malloc(sizeof(queue));
						init(Q); //wrong, should create one queue for every variable;
						initM(M);
						int curmol;
						enqueue(Q, pDataArray1->site_state[s1].mol); 
						enqueueM(M, pDataArray1->site_state[s1].mol); //mark it
						while(!emptyp(Q)) {
							if (dequeue(Q, &curmol)==FAIL) printf("error queue\n");
							pDataArray1->molecule_state[curmol].complex=tmp_c;
							int ind=0;
							int s;
							while (StoM[curmol][ind]!=-1) {
								s=StoM[curmol][ind];
								if (pDataArray1->site_state[s].mol==curmol && pDataArray1->site_state[s].specieBounded!=-1) {
									//if not marked
									if (!inQueue(M, pDataArray1->site_state[s].bound->mol)) {
										//mark it
										enqueueM(M, pDataArray1->site_state[s].bound->mol);
										enqueue(Q, pDataArray1->site_state[s].bound->mol);
									}

								}
								ind++;
							}
						}
						deleteM(M);
						int new_size=0;
						for (int m=0; m<tot_molecule; m++) {
							if (pDataArray1->molecule_state[m].complex==tmp_c) new_size++;
						}
						for (int m=0; m<tot_molecule; m++) {
							if (pDataArray1->molecule_state[m].complex==tmp_c) pDataArray1->molecule_state[m].size=new_size;
						}
						
					}
					else {
						
						for (int m=0; m<tot_molecule; m++) {
							if (pDataArray1->molecule_state[m].id==pDataArray1->site_state[s1].mol) {
								pDataArray1->molecule_state[m].complex=0;
								pDataArray1->molecule_state[m].size=1;
							}
						}
					}
					if (bounded2) {
						for (int m=0; m<tot_molecule; m++) {
							if (pDataArray1->molecule_state[m].id==pDataArray1->site_state[s2].mol) {
								complex=pDataArray1->molecule_state[m].complex;
							}
						}
						EnterCriticalSection( &cs_mutex );
						 // lock
							complex_count++;
							tmp_c=complex_count;
						LeaveCriticalSection( &cs_mutex );
						
						queue* Q;
						queue* M;
						Q=(queue*)malloc(sizeof(queue));
						M=(queue*)malloc(sizeof(queue));
						init(Q);
						initM(M);
						int curmol;
						enqueue(Q, pDataArray1->site_state[s2].mol);
						enqueueM(M, pDataArray1->site_state[s2].mol); //mark it
						while(!emptyp(Q)) {
							if (dequeue(Q, &curmol)==FAIL) printf("error queue\n");
							pDataArray1->molecule_state[curmol].complex=tmp_c;
							int ind=0;
							int s;
							while (StoM[curmol][ind]!=-1) {
								s=StoM[curmol][ind];
								if (pDataArray1->site_state[s].mol==curmol && pDataArray1->site_state[s].specieBounded!=-1) {
									//if not marked
									if (!inQueue(M, pDataArray1->site_state[s].bound->mol)) {
										//mark it
										enqueueM(M, pDataArray1->site_state[s].bound->mol);
										enqueue(Q, pDataArray1->site_state[s].bound->mol);
									}

								}
								ind++;
							}
						}
						deleteM(M);
						int new_size=0;
						for (int m=0; m<tot_molecule; m++) {
							if (pDataArray1->molecule_state[m].complex==tmp_c) new_size++;
						}
						for (int m=0; m<tot_molecule; m++) {
							if (pDataArray1->molecule_state[m].complex==tmp_c) pDataArray1->molecule_state[m].size=new_size;
						}
					}
					else {
						
						for (int m=0; m<tot_molecule; m++) {
							if (pDataArray1->molecule_state[m].id==pDataArray1->site_state[s2].mol) {
								pDataArray1->molecule_state[m].complex=0;
								pDataArray1->molecule_state[m].size=1;
							}
						}
					
					}
					
					
					//debug
					int debug3=0;
					int debug4=0;
					int debug5=0;
					for (int m=0; m<tot_molecule; m++) {
						if (pDataArray1->molecule_state[m].complex!=0) {
							if (pDataArray1->molecule_state[pDataArray1->site_state[s1].mol].complex==pDataArray1->molecule_state[m].complex) debug3++;
							if (pDataArray1->molecule_state[pDataArray1->site_state[s2].mol].complex==pDataArray1->molecule_state[m].complex) debug4++;
							if (complex==pDataArray1->molecule_state[m].complex) debug5++;
						}
					}
					if (debug3==1 || debug4==1) {
						printf("\t>>>>>>>ERROR complex 1, m1=%d, m2=%d, complex1=%d, complex2=%d, degree1=%d, degree2=%d\n", 
							pDataArray1->site_state[s1].mol, pDataArray1->site_state[s2].mol,
							pDataArray1->molecule_state[pDataArray1->site_state[s1].mol].complex, pDataArray1->molecule_state[pDataArray1->site_state[s2].mol].complex,
							pDataArray1->molecule_state[pDataArray1->site_state[s1].mol].degree, pDataArray1->molecule_state[pDataArray1->site_state[s2].mol].degree);
						printf("s1.specieBounded=%d\n, s2.specieBounded=%d, debug3=%d, debug4=%d\n", pDataArray1->site_state[s1].specieBounded, pDataArray1->site_state[s2].specieBounded, debug3, debug4);
						printf("size of old complex %d\n", debug5);
						int totb1=0;
						int totb2=0;
						printf("totb1=%d,totb2=%d\n",totb1, totb2);
						exit(0);
					}
				}
				else {
					mu=mu/2;
					if (mu>=reacts || mu<0) printf("ERROR mu=%d\n", mu);
					if (pDataArray1->a[i*reacts*2+mu*2]==0) printf("error binding with propensity 0 in reaction %d\n", mu);
					EnterCriticalSection( &cs_cb );
							cb++;
					LeaveCriticalSection( &cs_cb );
					int s1=0;
					int s2=0;
					u=rand() / double(RAND_MAX);
					chooseSite(pDataArray1->v[mu*2], pDataArray1->v[mu*2+1], &s1, &s2, u, pDataArray1->site_state, pDataArray1->i*(CELLS/CPU_THREADS)+i, pDataArray1->abdS, mu);
					
							if (pDataArray1->site_state[s1].cell!=pDataArray1->site_state[s2].cell) {
								printf(">>>>>>>>>ERROR BINDING BETWEEN DIFFERENT CELLS\n");
							}
					if (s1==0 || s2==0) {
						printf("error p=%f s1=%d s2=%d u=%f cell=%d time=%f\n", pDataArray1->a[i*reacts*2+mu*2], s1, s2, u, pDataArray1->i*(CELLS/CPU_THREADS)+i, t);
						printf("h = %f\n", h_CPU(pDataArray1->site_state, pDataArray1->molecule_state, pDataArray1->c[mu], pDataArray1->v, mu, pDataArray1->i*(CELLS/CPU_THREADS)+i, pDataArray1->abdS, pDataArray1->i));
						printf("a0 = %f\n", a0);
						printf("g_a = %f\n", g_a[(pDataArray1->i*(CELLS/CPU_THREADS)+i)*reacts*2+mu*2]);
					}
					pDataArray1->site_state[s1].bound=&pDataArray1->site_state[s2];
					pDataArray1->site_state[s2].bound=&pDataArray1->site_state[s1];
					pDataArray1->site_state[s2].specieBounded=pDataArray1->site_state[s1].specie;
					pDataArray1->site_state[s1].specieBounded=pDataArray1->site_state[s2].specie;
					EnterCriticalSection( &cs_mutex );
						// lock
						complex_count++;
						tmp_c=complex_count;
					LeaveCriticalSection( &cs_mutex );
					int m_inv1=pDataArray1->site_state[s1].mol;
					int m_inv2=pDataArray1->site_state[s2].mol;
					int debug1=0;
					int debug2=0;
					int new_size=0;
					for (int m=0, short f=0; m<tot_molecule && f<2; m++) {
						if (pDataArray1->molecule_state[m].id==m_inv1 || pDataArray1->molecule_state[m].id==m_inv2) {
							f++;
							pDataArray1->molecule_state[m].degree++;
							debug1++;
							debug2=pDataArray1->molecule_state[m].degree;
							if (pDataArray1->molecule_state[m].complex!=0) {
								updateComplex(pDataArray1->molecule_state, pDataArray1->molecule_state[m].complex, pDataArray1->i*(CELLS/CPU_THREADS)+i, tmp_c);
							}
							else {
								pDataArray1->molecule_state[m].complex=tmp_c;
							}
							if (pDataArray1->molecule_state[m].degree!=debug2) {
								printf("ERROR binding1 %d\n", pDataArray1->molecule_state[m].degree);
								exit(0);
							}
							
						}
					}
					for (int m=0; m<tot_molecule; m++) {
						if (pDataArray1->molecule_state[m].complex==tmp_c) {
							new_size++;
						}
					}
					for (int m=0; m<tot_molecule; m++) {
						if (pDataArray1->molecule_state[m].complex==tmp_c) {
							pDataArray1->molecule_state[m].size=new_size;
						}
					}
					if (debug1!=2) {
						printf("ERROR binding2 %d\n", debug1);
						exit(0);
					}
					if ((pDataArray1->molecule_state[pDataArray1->site_state[s2].mol].degree == 0 ) || (pDataArray1->molecule_state[pDataArray1->site_state[s1].mol].degree == 0 )) {
						printf("ERROR binding 3\n");
					}
					
					
				}
				//for (int i=0; i<nspec; i++) {
					//speciesAbd[CellID][i]+=v[mu][i];
				//}
			}
			t+=tau;
			//printf("a0=%f\n", a0);
			//t+=0.000006;
		} 
	}
	//binding<<<CELLS/CPU_THREADS,REACTIONS>>> (pDataArray1->devStates, pDataArray1->site_state, pDataArray1->molecule_state, pDataArray1->v, pDataArray1->c, pDataArray1->cu, pDataArray1->tmax, pDataArray1->abdS,pDataArray1->i);
	/*
	EnterCriticalSection( &cs_sync );
	sync++;
	LeaveCriticalSection( &cs_sync );*/
	free(pDataArray1->a);
	return 0; 

}


// Host code
int main(int argc, char** argv)
{
	time_t seconds;
	time_t last;
	printf("size of site %d\n", sizeof(site));
	int num_devices, device;
	checkCudaErrors(cudaGetDeviceCount(&num_devices));
	InitializeCriticalSection(&cs_mutex);
	InitializeCriticalSection(&cs_sync);
	InitializeCriticalSection(&cs_cb);
	InitializeCriticalSection(&cs_cu);

	
strcpy(idCompart[0],"nucleous");
strcpy(idCompart[1],"cytoplasm");
strcpy(idCompart[2],"ER");
strcpy(idCompart[3],"extracellular space");
strcpy(idCompart[4],"Golgi1");
strcpy(idCompart[5],"Golgi2");
strcpy(idCompart[6],"Golgi3");
strcpy(idCompart[7],"Vesicles1");
strcpy(idCompart[8],"Vesicles2");
strcpy(idCompart[9],"Vesicles3");
strcpy(idCompart[10],"Mitochrondria1");
strcpy(idCompart[11],"Mitochrondria2");
strcpy(idCompart[12],"Mitochrondria3");
strcpy(idCompart[13],"Mitochrondria4");
strcpy(idCompart[14],"Mitochrondria5");
strcpy(idCompart[15],"Mitochrondria6");
strcpy(idCompart[16],"Mitochrondria7");
strcpy(idCompart[17],"Mitochrondria8");
strcpy(idCompart[18],"Mitochrondria9");
strcpy(idCompart[19],"Mitochrondria10");
strcpy(idCompart[20],"Peroxisome1");
strcpy(idCompart[21],"Peroxisome2");
strcpy(idCompart[22],"Peroxisome3");
strcpy(idCompart[23],"Peroxisome4");
strcpy(idCompart[24],"Peroxisome5");
strcpy(idCompart[25],"Peroxisome6");
strcpy(idCompart[26],"Peroxisome7");
strcpy(idCompart[27],"Peroxisome8");
strcpy(idCompart[28],"Peroxisome9");
strcpy(idCompart[29],"Peroxisome10");
strcpy(idCompart[30],"Lysosome1");
strcpy(idCompart[31],"Lysosome2");
strcpy(idCompart[32],"Lysosome3");
strcpy(idCompart[33],"Lysosome4");
strcpy(idCompart[34],"Lysosome5");
strcpy(idCompart[35],"Lysosome6");
strcpy(idCompart[36],"Lysosome7");
strcpy(idCompart[37],"Lysosome8");
strcpy(idCompart[38],"Lysosome9");
strcpy(idCompart[39],"Lysosome10");

	srand(time(NULL));

    int max_multiprocessors = 0, max_device = 0;
    for (device = 0; device < num_devices; device++) {
		  cudaDeviceProp properties;
		  cudaGetDeviceProperties(&properties, device);
		  printf("Device ID:\t%d\n", device);
		  printf("Device Name:\t%s\n", properties.name );
		  printf("Global memory:\t%d\n", properties.totalGlobalMem );
		  printf("Constant memory:\t%d\n", properties.totalConstMem );
		  printf("Warp size:\t%d\n", properties.warpSize );
    }
	//scanf("%d", &max_device);
	
	devs=0;
    printf("Initializing simulation...\n");
	inputfile="collins_nucleous_function.xml"; //GO_peroxisome.xml prot.xml GO_golgi GO_mitochondrion GO_golgi apc golgi_withPFX Vacuole_noPFX.xml
	
    ParseArguments(argc, argv);
	printf("Selected device:\t%d\n", devs);
	cudaSetDevice(devs);
	reacts=0;
	int nsite=0;
	nmol=0;
	tot_site=SBML_getNumSitesTot(inputfile);
	tot_molecule=4;
	nsite=SBML_getNumSpecies(inputfile);
	tot_molecule=SBML_getNumSpeciesTypesTot(inputfile);
	nmol=SBML_getNumSpeciesTypes(inputfile);
	reacts=SBML_getNumReactions(inputfile);
    //printf("tot sites=%d\n", SBML_getNumSitesTot(inputfile));
	curandState *devStates;

	// Initialize variables

    //size_t sizeParticles = SPECIES * sizeof(site);
	//eq=(eventQueue*)malloc(sizeQueue);
	//totParticles=(int*)malloc(sizeParticles);
	printf("nmol = %d\n", nmol);
	printf("tot_molecules = %d - tot_site=%d\n", tot_molecule,tot_site);
	site_state=(site*)malloc(sizeof(site)*tot_site);
	molecule_state=(molecule*)malloc(sizeof(molecule)*tot_molecule);
	counter=(int**)malloc(sizeof(int*)*CELLS);
	for (int cell=0; cell<CELLS; cell++) counter[cell]=(int*)malloc(sizeof(int)*nmol);
	printf("Site state size: %d B\nMolecule state size %d B\n", sizeof(site)*tot_site, sizeof(molecule)*tot_molecule);
	printf("#Sites: %d\t#Molecules: %d\n", tot_site, tot_molecule);
	//speciesAbd=(site_list**)malloc(sizeof(site_list*)*CELLS);
	//h_speciesAbd=(site_list**)malloc(sizeof(site_list*)*CELLS);
	v=(int*)malloc(sizeof(int)*reacts*2);
	c=(float*)malloc(sizeof(float)*reacts);
	cu=(float*)malloc(sizeof(float)*reacts);
	deb=(float*)malloc(sizeof(float)*tot_site);
	//h_molsInLattice=(unsigned short*)malloc(sizeof(unsigned short)*CELLS);
	//a=(float*)malloc(sizeof(float)*CELLS*REACTIONS*2);


	for (int j=0; j<reacts*2; j++) {
		v[j]=0;
		j++;
		v[j]=0;
	}
	
	v[0]=1; v[1]=2; v[2]=3; v[3]=4;   //la prima reazione � tra le specie 1 e 2, la seconda tra la 3 e la 4

	for (int i=0; i<reacts; i++) {
		c[i]=1;
	}


	//debug
	/*for (int i=0; i<reacts; i++) {
		reactCount[i]=0;
	}*/

	float* D = (float*)malloc(sizeof(float)*nmol);
	for (int i=0; i<nmol; i++) {
		D[i]=0.00001;
	}
	//D[0]=2;
	char** map;
	char** mapS;
	map=(char**)malloc(sizeof(char*)*nmol);
	mapS=(char**)malloc(sizeof(char*)*tot_site); //da cambiare in nsite
	
	if (parser(inputfile, site_state, molecule_state, v, c, cu, D, CELLS, reacts, tot_site, map, mapS, CELLS/2-THREADS*GRID_W/2)<0) exit(0);
	
/*	for (int i=0; i<tot_molecule; i++) {
		printf("Molecule id %d specie %s\n", molecule_state[i].id, map[molecule_state[i].specie]);
	}
	
	for (int j=0; j<tot_site; j++) {
		printf("Site id %d specie %s molecule %d\n", j, mapS[site_state[j].specie], site_state[j].mol);
	}

	for (int j=0; j<reacts; j++) {
		//printf("Reaction %d species %s - %s, binding %f, unbounding %f\n", j, mapS[v[j*2]], mapS[v[j*2+1]], c[j], cu[j]);
		printf("Reaction %d species %d - %d, binding %f, unbounding %f\n", j, v[j*2], v[j*2+1], c[j], cu[j]);
	}

*/
	float t_sim=0;
	float t_end=tm;
	float* ts; //time of the next diffusion event
	float* a0;
	clock_t t1, t2;
	ts=(float*)malloc(sizeof(float));
	a0=(float*)malloc(sizeof(float));
	*ts=0;
	int* n=(int*)malloc(sizeof(int)*nmol);
	float* tau=(float*)malloc(sizeof(float)*nmol);
	int dim=2; //dimension
	for (int i=0; i<nmol; i++) {
		n[i]=1;
		//D[i]=0.3;
	}
	for (int i=0; i<nmol; i++) {
		tau[i]=l*l/(2*dim*D[i]);
	}
	*ts=tau[0];
	for (int i=0; i<nmol; i++) {
		if (tau[i]<*ts) *ts=tau[i];
	}

	//count species
	int* t_abdS=(int*)malloc(sizeof(int)*(nsite));
	int* abdS=(int*)malloc(sizeof(int)*(nsite+1));
	for (int i=0; i<nsite; i++) {
		t_abdS[i]=0;
		abdS[i]=0;
	}
	abdS[nsite]=0;
	for (int i=0; i<tot_site; i++) {
		t_abdS[site_state[i].specie]++;
	}
	//int bits=(tot_site/32)+1; //bits needed to store one bit for each site
	speciesBounded=(int*)malloc(10*sizeof(int));
	for (int i=0; i<10; i++) {
		speciesBounded[i]=0;
	}
	//int test=(int)pow(2.0f,(float)(80000%32));
	//if ((speciesBounded[80000/32] & test) == 0) printf("speciesBounded\n");
	int countS=0;
	int x=0;
	for (x=0; x<nsite; x++) {
		abdS[x]=countS;
		countS+=t_abdS[x];
	}
	abdS[x]=countS;
	StoM = (int**)malloc(sizeof(int*)*tot_molecule);
	int* tots = (int*)malloc(sizeof(int)*tot_molecule);
	for (int i=0; i<tot_molecule; i++) {
		tots[i]=0;
	}
	for (int i=0; i<tot_site; i++) {
		tots[site_state[i].mol]++;
	}
	int maxstom=0;
	for (int i=0; i<tot_molecule; i++) {
		StoM[i]=(int*)malloc(sizeof(int)*(tots[i]+1));
		int ind=0;
		/*for (int s=0; s<tot_site; s++) {
			if (site_state[s].mol==i) {
				StoM[i][ind]=s;
				ind++;
			}
		}*/
		StoM[i][ind]=-1;
		//printf("mol %d\n", i);
	}
	
	for (int i=0; i<tot_molecule; i++) {
		tots[i]=0; //now i use this array to store the current index
	}
	for (int s=0; s<tot_site; s++) {
		StoM[site_state[s].mol][tots[site_state[s].mol]]=s;
		tots[site_state[s].mol]++;
	}
	
	for (int i=0; i<tot_molecule; i++) {
		StoM[i][tots[i]]=-1;
		if (tots[i]>maxstom) maxstom=tots[i];
	}
	//printf("maximum number of site per molecule %d\n", maxstom);
	//for (int i=0; i<nsite+1; i++) {
	//	printf("%d: %d\n", i, abdS[i]);
	//}
	*a0=0;
	for (int i=0; i<tot_site; i++) {
		site_state[i].specieBounded=-1;
	}
	
	//diffused=(int*)malloc(sizeof(int)*tot_molecule);
	int* debug=(int*)malloc(sizeof(int)*tot_molecule);
	int* new_position=(int*)malloc(sizeof(int)*tot_molecule);
	//cudaMalloc((void **)&devStates, CELLS * sizeof(curandState)));
	float* d_rndArray;
	float* rndArray = (float*)malloc(sizeof(float)*tot_molecule*2);
	cudaMalloc((void **)&d_debug, tot_molecule * sizeof(int));
	cudaMalloc((void **)&d_new_pos, tot_molecule * sizeof(int));
	cudaMalloc((void **)&devStates, CELLS * sizeof(curandState));
	cudaMalloc((void **)&d_Q, CELLS * sizeof(int));
     cudaMalloc((void**)&d_v, sizeof(int)*reacts*2) ;
     cudaMalloc((void**)&d_c, sizeof(float)*reacts) ;
     cudaMalloc((void**)&d_cu, sizeof(float)*reacts) ;
     cudaMalloc((void**)&d_n, sizeof(int)*nmol) ;
     cudaMalloc((void**)&d_tau, sizeof(float)*nmol) ;
     cudaMalloc((void**)&d_abdS, sizeof(int)*(nsite+1)) ;
     cudaMalloc((void**)&d_ts, sizeof(float)) ;
     cudaMalloc((void**)&d_max_complex, sizeof(float)) ;
	 cudaMalloc((void**)&d_site_state, sizeof(site)*tot_site) ;
	 cudaMalloc((void**)&d_molecule_state, sizeof(molecule)*tot_molecule) ;
	 cudaMalloc((void**)&d_a0, sizeof(float)) ;
	 cudaMalloc((void**)&d_rndArray, sizeof(float)*tot_molecule*2) ;
	// cudaMalloc((void**)&d_a, sizeof(float)*reacts*2*(CELLS/CPU_THREADS)) );
	 cudaMalloc((void**)&d_a, sizeof(float)*reacts*(CELLS/CPU_THREADS)*2) ;
	// cudaMalloc((void**)&d_molsInLattice, sizeof(unsigned short)*CELLS ));
	
	 cudaMemcpy(d_site_state, site_state, sizeof(site)*tot_site, cudaMemcpyHostToDevice) ;
	 cudaMemcpy(d_molecule_state, molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyHostToDevice) ;

	 cudaMemcpy(d_v, v, sizeof(int)*reacts*2, cudaMemcpyHostToDevice) ;
	 cudaMemcpy(d_c, c, sizeof(float)*reacts, cudaMemcpyHostToDevice) ;
	 cudaMemcpy(d_cu, cu, sizeof(float)*reacts, cudaMemcpyHostToDevice) ;
	 cudaMemcpy(d_tau, tau, sizeof(float)*nmol, cudaMemcpyHostToDevice) ;
	 cudaMemcpy(d_abdS, abdS, sizeof(int)*(nsite+1), cudaMemcpyHostToDevice) ;
	 cudaMemcpy(d_n, n, sizeof(int)*nmol, cudaMemcpyHostToDevice) ;
	 cudaMemcpy(d_ts, ts, sizeof(float), cudaMemcpyHostToDevice) ;
	 cudaMemcpy(d_a0, a0, sizeof(float), cudaMemcpyHostToDevice) ;
	// cudaMemcpy(d_molsInLattice, h_molsInLattice, sizeof(unsigned short)*CELLS, cudaMemcpyHostToDevice) );
	// cudaMemcpy(d_a, a, sizeof(float)*reacts*2*CELLS, cudaMemcpyHostToDevice) );
    // cudaMalloc((void**)&d_speciesBounded, 16000*sizeof(int)) );
	
	//cudaPrintfInit();


	//check wrong id mol
	for (int i=0; i<tot_molecule; i++) {
		if (i!=molecule_state[i].id) printf("sfasati %d - %d\n", i, molecule_state[i].id);
	}

	FILE *pF2;
	pF2 = fopen("resultCOMPLEX.out", "w"); 
	FILE *pF3;
	pF3 = fopen("complex_structure.out", "w"); 

	printf("BLOCKS %d\n", BLOCKS);
	printf("Maximum simulation time %f\n", tm);
	printf("Attenzione, la simulazione potrebbe richiedere diverse ore, nelle quali il pc rimarr� inutilizzabile.\nPremere INVIO per continuare\n");
	
    fflush( stdout);
	getchar();
	seconds = time (NULL);
    printf("Performing simulation...\n");
	//size_t shr_mem = sizeof(int)*nspec*reacts;
	writeState(tot_molecule, t_sim);
	int write=0;
	int* max_complex;
	max_complex=(int*)malloc(sizeof(int));
	
	int** rec = (int**)malloc(sizeof(int*)*CELLS);
	for (int i=0; i<CELLS; i++) {
		rec[i]=(int*)malloc(sizeof(int)*reacts);
	}
	int thrs=CELLS/BLOCKS;
	int thrs2=tot_molecule/BLOCKS;
	DWORD dwThrdId;
	//cutilCheckMsg("Memory error");			
	//while (t_sim<t_end) {
	bool quit=false;
	int history[40];
	for (int i=0; i<40; i++) {
		history[i]=0;
	}
	double old_ts=0;
	int tot_rec=0;
	g_a=(float*)malloc(sizeof(float)*CELLS*reacts*2);
	bool first=true;
	t1=clock();
	
	int start=round(double(rand() / ( RAND_MAX))*5);
	while (!quit && (*ts<tm)) {
		last = time (NULL);
		 cudaMemcpy(d_site_state, site_state, sizeof(site)*tot_site, cudaMemcpyHostToDevice) ;
		 cudaMemcpy(d_molecule_state, molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyHostToDevice) ;
		// cudaMemcpyToSymbol("d_speciesBounded", speciesBounded, 10*sizeof(int)) );
		//inner loop
		float t=0;
		float t_max=*ts-t_sim;
		
		double tot_propensity_binding=0;	
		double tot_propensity_unbinding=0;
		//float sum;
		float* a0_debug=(float*)malloc(sizeof(float));
//		float a_debug[REACTIONS];
		float* thr_a;
		sync=0;
		//for (int s=0; s<SPECIES; s++) {
		//	if ((site_state[s].specie==v[24*2])||(site_state[s].specie==v[24*2+1])) printf("right cell = %d\n", site_state[s].cell);
		//}
		int perTr=floor((double)(reacts/64));
		int threads_r=reacts-(64*perTr);
		int tr;
		if (!first) {
			printf("step>1 %d\n",tot_rec);
			
			 cudaMalloc((void**)&d_rec, sizeof(rec_t)*tot_rec) ;
			 cudaMemcpy(d_rec, rec_gpu, sizeof(rec_t)*tot_rec, cudaMemcpyHostToDevice) ;
			 cudaMalloc((void**)&d_a2, sizeof(float)*tot_rec*2 ) ;
			int tot_kernel=floor((float)(tot_rec/(512*64)));
			threads_r=tot_rec-(tot_kernel*512*64);
			printf("tot_kernel=%d - threads_r=%d\n",tot_kernel,threads_r);
			t1 = clock();  
			for (tr=0; tr<tot_kernel; tr++) {
				binding2<<<512,64>>> (d_a0, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, tot_rec, d_a2, tr, reacts, tot_molecule,d_rec);
				unbinding2<<<512,64>>> (d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, tot_rec, d_a2, tr, reacts,d_rec);	
				cudaThreadSynchronize();
			}
			printf("debug 1\n");
			if (threads_r!=0) {
				//int nThr=floor((float)threads_r/512);
				binding2<<<512,64>>> (d_a0, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, tot_rec, d_a2, tr, reacts, tot_molecule, d_rec);
				unbinding2<<<512,64>>> (d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, tot_rec, d_a2, tr, reacts, d_rec);
				//if (nThr*512-threads_r>0) {
					
				//}
			}  
			cudaThreadSynchronize();
			t2 = clock();   

			float diff = (((float)t2 - (float)t1) / 1000000.0F ) * 1000;   
			printf("%f\n",diff); 
			t1=clock();
			
			//cutilCheckMsg("DM2 error");
			//printf("debug 2\n");
			//printf("copying back propensities\n");
			float* thr_a2 = (float*)malloc(sizeof(float)*tot_rec*2);
			 cudaMemcpy(thr_a2, d_a2, sizeof(float)*tot_rec*2, cudaMemcpyDeviceToHost) ;
			//printf("debug3\n");
			 cudaFree(d_rec);
			 cudaFree(d_a2);
			float sum_debug=0;
			for (int d=0; d<tot_rec; d++) {
				//g_a[rec_gpu[d].cellID*reacts*2+rec_gpu[d].react*2]+=thr_a2[d*2];
				//g_a[rec_gpu[d].cellID*reacts*2+rec_gpu[d].react*2+1]+=thr_a2[d*2+1];
				//sum_debug+=thr_a2[d*2];
				//if (thr_a2[d*2]>0) printf("debug %f %f %d %d\n", thr_a2[d*2], thr_a2[d*2],rec_gpu[d].cellID,rec_gpu[d].react);
				g_a[rec_gpu[d].cellID*reacts*2+rec_gpu[d].react*2]=thr_a2[d*2];
				g_a[rec_gpu[d].cellID*reacts*2+rec_gpu[d].react*2+1]=thr_a2[d*2+1];
				/*float h_tmp=h_CPU(site_state, molecule_state, c[rec_gpu[d].react], v, rec_gpu[d].react, rec_gpu[d].cellID, abdS, 0);
				if (thr_a2[d*2]!=h_tmp) {
					printf("error gpu_h %f and cpu_h disagree %f\n", thr_a2[d*2], h_tmp);
					exit(0);
				}*/
			}
			free(thr_a2);
			//free(rec_gpu);
			//for (int i=0; i<CPU_THREADS; i++) {
				//pDataArray[i] = (PMYDATA1)malloc(sizeof(MYDATA1));
			pDataArray[0] = (PMYDATA1)malloc(sizeof(MYDATA1));
				//if( pDataArray[i] == NULL )
			if( pDataArray[0] == NULL )
				{
					ExitProcess(2);
				}
				pDataArray[0]->tmax=*ts-t_sim;
				pDataArray[0]->devStates=devStates;
				pDataArray[0]->site_state=site_state;
				pDataArray[0]->molecule_state=molecule_state;
				pDataArray[0]->v=v;
				pDataArray[0]->c=c;
				pDataArray[0]->cu=cu;
				pDataArray[0]->abdS=abdS;
				pDataArray[0]->speciesBounded=speciesBounded;
				//thr_a=(float*)malloc(sizeof(float)*reacts*(CELLS/CPU_THREADS)*2);
				pDataArray[0]->i=0;
				pDataArray[0]->a=(float*)malloc(sizeof(float)*reacts*2*(CELLS));
				//for (int d=0; d<(CELLS)*reacts*2;d++) pDataArray[i]->a[d]=g_a[i*(CELLS)*reacts*2+d]; 
				for (int d=0; d<(CELLS)*reacts*2;d++) pDataArray[0]->a[d]=g_a[d]; 
				Thread1[0] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)DM, pDataArray[0], 0, &dwThrdId);
			//}
				for (int d=0; d<CELLS*reacts*2; d+=2) {
					if (g_a[d]>0) {
						tot_propensity_binding+=g_a[d];
						//if (thr_a[d+1]>0) printf("unbinding > 0\n");
					}
					if (g_a[d+1]>0) {
						
						tot_propensity_unbinding+=g_a[d+1];
					}
				}
		}
		else {
		//printf("threads_r=%d, trs=%d, blocks=%d, debug=%f\n", threads_r, (REACTIONS/64), CELLS/CPU_THREADS,REACTIONS-(64*(REACTIONS/64)));
			//first=false;
			threads_r=reacts-(64*perTr);
			for (int i=0; i<CPU_THREADS; i++) {
				//binding<<<CELLS/CPU_THREADS,REACTIONS>>> (devStates, d_a0, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, i, d_a);
				//unbinding<<<CELLS/CPU_THREADS,REACTIONS>>> (devStates, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, i, d_a);
				//binding<<<CELLS/CPU_THREADS,64>>> (devStates, d_a0, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, i, d_a, 0);
				//unbinding<<<CELLS/CPU_THREADS,64>>> (devStates, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, i, d_a, 0);
				
				for (tr=0; tr<perTr; tr++) {
					binding<<<CELLS/CPU_THREADS,64>>> (devStates, d_a0, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, i, d_a, tr, reacts, tot_molecule);
					unbinding<<<CELLS/CPU_THREADS,64>>> (devStates, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, i, d_a, tr, reacts);
					cudaThreadSynchronize();
				}
				if (threads_r!=0) {
					binding<<<CELLS/CPU_THREADS,threads_r>>> (devStates, d_a0, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, i, d_a, tr, reacts, tot_molecule);
					unbinding<<<CELLS/CPU_THREADS,threads_r>>> (devStates, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, i, d_a, tr, reacts);
				}
				//cudaThreadSynchronize());
				//unbinding
				//unbinding<<<CELLS/CPU_THREADS,REACTIONS>>> (devStates, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, i, d_a);
				
				//cutilCheckMsg("DM1 error");
				pDataArray[i] = (PMYDATA1)malloc(sizeof(MYDATA1));
				if( pDataArray[i] == NULL )
				{
					ExitProcess(2);
				}
				pDataArray[i]->i=i;
				pDataArray[i]->tmax=*ts-t_sim;
				pDataArray[i]->devStates=devStates;
				pDataArray[i]->site_state=site_state;
				pDataArray[i]->molecule_state=molecule_state;
				pDataArray[i]->v=v;
				pDataArray[i]->c=c;
				pDataArray[i]->cu=cu;
				pDataArray[i]->abdS=abdS;
				pDataArray[i]->speciesBounded=speciesBounded;
				thr_a=(float*)malloc(sizeof(float)*reacts*(CELLS/CPU_THREADS)*2);
				cudaThreadSynchronize();
				 cudaMemcpy(thr_a, d_a, sizeof(float)*reacts*(CELLS/CPU_THREADS)*2, cudaMemcpyDeviceToHost);
				/* DEBUG ----->
				for (int k=0; k<CELLS/CPU_THREADS; k++) {
					sum=0;
					*a0_debug=0;
					for (int j=0; j<REACTIONS; j++) {
						binding_CPU(a0_debug, site_state, molecule_state, v, c, cu, *ts-t_sim, abdS, i*(CELLS/CPU_THREADS)+k, a_debug, j);
					}
					
					for (int j=0; j<reacts; j++) {
						sum+=a_debug[j];
					}
					if (sum>0) {
						for (int j=0; j<reacts; j++) {
							//sum+=a[j];
							if (a_debug[j]!=a[k*REACTIONS*2+j*2]) {
								printf("errore%f - %f\n",a_debug[j],a[k*REACTIONS+j]);
							}
						}
					}
				}
				FINE DEBUG ----> */

				//start thread
				//DM host side
				
				for (int d=0; d<(CELLS/CPU_THREADS)*reacts*2;d++) {
					if (first) g_a[i*(CELLS/CPU_THREADS)*reacts*2+d]=thr_a[d];
					if (g_a[i*(CELLS/CPU_THREADS)*reacts*2+d]!=thr_a[d] && !first) {
						printf("old %f new %f\n", g_a[i*(CELLS/CPU_THREADS)*reacts*2+d], thr_a[d]);
						int react2=d%((CELLS/CPU_THREADS));
						int react=floor((float)react2/2);
						int cell=i*(CELLS/CPU_THREADS)+floor((float)d/(reacts*2));
						if (react2%2==0) printf("binding cell %d react %d\n", cell, react);
						else printf("unbinding cell %d react %d\n", cell, react);
						bool ce=false;
						for (int i=0; i<tot_rec; i++) {
							if (rec_gpu[i].cellID==cell && rec_gpu[i].react==react) ce=true;
						}
						if (!ce) printf("reaction doesn't exist\n");
					}
				}
				for (int d=0; d<(CELLS/CPU_THREADS)*reacts*2; d+=2) {
					if (thr_a[d]>0) {
						tot_propensity_binding+=thr_a[d];
						//if (thr_a[d+1]>0) printf("unbinding > 0\n");
					}
					if (thr_a[d+1]>0) {
						
						tot_propensity_unbinding+=thr_a[d+1];
					}
				}
				/*int tmp_cell=-1;
				for (int d=0; d<(CELLS/CPU_THREADS); d++) {
					if (thr_a[d*(REACTIONS*2)+24]>0) {
						printf("cell = %d\n", i*(CELLS/CPU_THREADS)+d);
						tmp_cell=i*(CELLS/CPU_THREADS)+d;
					}
				}
				for (int s=0; s<SPECIES; s++) {
					if (site_state[s].cell==tmp_cell && (site_state[s].specie==v[24*2] || site_state[s].specie==v[24*2+1])) printf("c'�\n");
				}*/
				
				pDataArray[i]->a=thr_a;
				Thread1[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)DM2, pDataArray[i], 0, &dwThrdId);

			}
		}
				if (!first) 
					free(rec_gpu);
		printf("tot propensity binding = %f unbinding = %f\n", tot_propensity_binding, tot_propensity_unbinding);
			//printf("tot prop = %f\n", tot_propensity);
		//WaitForMultipleObjects(CELLS,Thread1,TRUE,INFINITE); //should be CPU_THREADS
		DWORD  dwRet;
		if (first) dwRet=WaitForMultipleObjects(CPU_THREADS,Thread1,TRUE,INFINITE); //should be CPU_THREADS
		else dwRet=WaitForMultipleObjects(1,Thread1,TRUE,INFINITE);
		first=false;
		switch(dwRet) {
        case WAIT_OBJECT_0 + 0:
            //wprintf(L"Events was signaled...\n");
            break;
        case WAIT_TIMEOUT:
            wprintf(L"The waiting is timed out...\n");
        break;
        default:
            wprintf(L"Waiting failed, error %d...\n", GetLastError());
            ExitProcess(0);
		}      
		
			t2 = clock();   

			float diff = (((float)t2 - (float)t1) / 1000000.0F ) * 1000;   
			printf("%f\n",diff); 
		//printf("debug reactions = %d\n", debug_reactions);
		 cudaMemcpy(a0, d_a0, sizeof(float), cudaMemcpyDeviceToHost) ;
		 cudaMemcpy(d_molecule_state, molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyHostToDevice) ;
			
		
		// cudaMemcpy(site_state, d_site_state, sizeof(site)*tot_site, cudaMemcpyDeviceToHost) );
		
		//t+=0.000006;
		
		

		if (write%10==0) {
			//cudaMemcpy(molecule_state, d_molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyDeviceToHost));
			// cudaMemcpy(max_complex, d_max_complex, sizeof(int), cudaMemcpyDeviceToHost) );
		}
		float tb=0;
		float tu=0;
		for (int d=0; d<CELLS*reacts*2; d+=2) {
			if (g_a[d]>0) {
				tb+=g_a[d];
				//if (thr_a[d+1]>0) printf("unbinding > 0\n");
			}
			if (g_a[d+1]>0) {
				
				tu+=g_a[d+1];
			}
		}
		printf("debug %f %f\n", tb,tu); 
		//cutilCheckMsg("DM error");

		printf("DM time: %d seconds\n", time(NULL)-last);
		/*for (int i=0; i<tot_site; i++) {
			if (site_state[i].specieBounded!=-1) {
				if (molecule_state[site_state[i].mol].degree==0) {
					printf("ERROR degree0 %d %d\n", i, site_state[i].mol);
				}
			}
		}	*/	
		last = time (NULL);
		t_sim=*ts;
		//*ts+=0.015;
		printf("diffusion occurs at time: %f\n", *ts);
		int blocks=floor((double)(tot_molecule/CPU_THREADS));
		//printf("blocks=%d\n",blocks);
		int cput=1;
		while (blocks>512) {
			blocks=blocks/2;
			cput=cput*2;
		}
/*
		for (int i=0; i<tot_molecule; i++) {
			if (i!=molecule_state[i].id) {
				printf("ERROR mol index != id \n");
			}
		}
		for  (int i=0; i<tot_site; i++) {
			if (site_state[i].bound!=NULL) {
				if (molecule_state[site_state[i].mol].complex!=molecule_state[site_state[i].bound->mol].complex) {
					printf(">erorr complex %d - %d\n", site_state[i].cell, site_state[i].bound->cell);
					printf(">erorr complex %d - %d - %d - %d\n", molecule_state[site_state[i].mol].cell, molecule_state[site_state[i].bound->mol].cell, molecule_state[site_state[i].mol].complex, molecule_state[site_state[i].bound->mol].complex);
				}
			}
		}*/
		
		for (int m=0; m<tot_molecule; m++) {
			//h_molsInLattice[moleculte_state[m].cell]++;
		}

		// cudaMemcpy(d_molsInLattice, h_molsInLattice, sizeof(unsigned short)*CELLS, cudaMemcpyHostToDevice) );
		//Diffusion<<<CPU_THREADS*cput, blocks>>>(devStates, d_site_state, d_molecule_state, d_tau, d_n, d_ts, blocks, false, cput,d_molsInLattice);
		
		//cutilCheckMsg("Diffusion error 1");
		//Diffusion<<<1, tot_molecule-(blocks*CPU_THREADS*cput)>>>(devStates, d_site_state, d_molecule_state, d_tau, d_n, d_ts, blocks, true, 1);
		//printf(" debug extra=%d blocks=%d\n", MOLECULES-(blocks*CPU_THREADS),blocks);
		//diffusion for each molecule
		//copy molecule -> device to host
		for (int i=0; i<CELLS; i++) {
			Q[i]=0;
		}
		//newest diffusion
		for (int i=0; i<tot_molecule; i++) {
			Q[molecule_state[i].cell]++;
			new_position[i]=-1;
		}
		for (int i=0; i<tot_molecule; i++) {
			if (molecule_state[i].cell>=4096) printf("error early > 4096 %d\n", molecule_state[i].cell);
		}
		// cudaMemcpy(d_site_state, site_state, sizeof(site)*tot_site, cudaMemcpyHostToDevice) );
		int bls=tot_molecule/(100*128);
		bls+=1;
		reset<<<bls,128>>>(tot_molecule);
		//for (int i=0; i<tot_molecule; i++) debug[i]=-1;
		 cudaMemcpy(d_molecule_state, molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyHostToDevice) ;
		 cudaMemcpy(d_Q, Q, sizeof(int)*CELLS, cudaMemcpyHostToDevice) ;
		 cudaMemcpy(d_new_pos, new_position, sizeof(int)*tot_molecule, cudaMemcpyHostToDevice) ;
		 cudaMemcpy(d_debug, debug, sizeof(int)*tot_molecule, cudaMemcpyHostToDevice) ;


		/* 	
		for (int i=1; i<=complex_count; i++) {
			int tcell=-1;
			for (int j=0; j<tot_molecule; j++) {
				if (molecule_state[j].complex==i) {
					if (tcell!=molecule_state[j].cell && tcell==-1) tcell=molecule_state[j].cell;
					else if (tcell!=molecule_state[j].cell) {
						printf("ERROR complex not in the same cell! %d - %d - %d\n", tcell, molecule_state[j].cell, i);
						exit(0);
					}
				}
			}
		}*/

		//Diffusion_CPU(site_state, molecule_state, tau, n, ts, blocks, true, 1);
		
		/*for (int diff_k=0; diff_k<6; diff_k++) {
			debug_kernel<<<32, 22>>>(tot_molecule, d_new_pos,d_debug, d_molecule_state, diff_k);
			cudaThreadSynchronize());
cudaPrintfDisplay(stdout, true);
		}
			debug_kernel<<<32, 32>>>(tot_molecule, d_new_pos,d_debug, d_molecule_state, 0);
			cudaThreadSynchronize());
			cudaError_t errG = cudaGetLastError();
if (errG != cudaSuccess) 
    printf("Error: %s\n", cudaGetErrorString(errG));
cudaPrintfDisplay(stdout, true);
	cudaPrintfEnd();
		//printf("tot molecules = %d\n", tot_molecule);
		 cudaMemcpy(molecule_state, d_molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyDeviceToHost) );
		 cudaMemcpy(new_position, d_new_pos, sizeof(int)*tot_molecule, cudaMemcpyDeviceToHost) );
		 cudaMemcpy(debug, d_debug, sizeof(int)*tot_molecule, cudaMemcpyDeviceToHost) );
		//printf("debug 0 = %d\n", debug[0]);*/
		//for (int i=0; i<tot_molecule; i++) {
			//if (debug[i]!=-1) {
				//if (debug[i]>=4096 || debug[i]<0) printf("ERROR new pos = %d - old pos = %d debug = %d\n",new_position[i],molecule_state[i].cell,debug[i]);
				//if (debug[i]!=1) printf("ERROR new pos = %d - old pos = %d debug = %d\n",new_position[i],molecule_state[i].cell,debug[i]);
				//updateMol(molecule_state, site_state, new_position[i], molecule_state[i].id, molecule_state[i].complex, tot_molecule, tot_site);
				
			//}
		//}
		//for (int i=0; i<CELLS; i++) {
			//if (new_position[i]!=1) printf("in cell %d - %d\n", i, new_position[i]);
		//}
		int left=0;
		int right=0;
		int up=0;
		int down=0;
		int leftb=0;
		int rightb=0;
		int upb=0;
		int downb=0;
		
		for  (int i=0; i<tot_site; i++) {
			if (site_state[i].bound!=NULL) {
				if (site_state[i].cell!=site_state[i].bound->cell) printf("erorr binding between different cells %d - %d\n", site_state[i].cell, site_state[i].bound->cell);
			}
		}
		//cudaThreadSynchronize());
		t1 = clock();
		
		srand ( time(NULL) );
		for (int i=0; i<tot_molecule*2; i++) {
			rndArray[i]=rand() / double(RAND_MAX);
			//printf("rand %f\n", rndArray[i]);
			/*if (rndArray[i]<=0.25) upb++;
			else if (rndArray[i]<=0.5) downb++;
			else if (rndArray[i]<=0.75) leftb++;
			else rightb++;*/
		}
		start++;
		start=start%6;
		//printf("\tleft=%d\n\tright=%d\n\tup=%d\n\tdown=%d\n",leftb,rightb,upb,downb);
		cudaMemcpy(d_rndArray, rndArray, sizeof(float)*tot_molecule*2, cudaMemcpyHostToDevice) ;
		for (int diff_k=start, int diff_c=0; diff_c<6; diff_k++, diff_c++) {
		//for (int diff_k=5; diff_k>=0; diff_k--) {
			diff_k=diff_k%6;
			for (int i=0; i<CELLS; i++) {
				Q[i]=0;
			}
			//newest diffusion
			for (int i=0; i<tot_molecule; i++) {
				Q[molecule_state[i].cell]++;
			}
			 cudaMemcpy(d_Q, Q, sizeof(int)*CELLS, cudaMemcpyHostToDevice) ;
			//Diffusion_GPU<<<64, 64>>>(devStates, d_site_state, d_molecule_state, d_tau, d_n, d_ts, blocks, false, cput, d_Q, tot_molecule, tot_site);
			Diffusion_GPU<<<32, 22>>>(devStates, d_molecule_state, d_Q, tot_molecule, tot_site, diff_k, d_new_pos, d_debug, 0, d_rndArray);
			//Diffusion_GPU<<<16, 22>>>(devStates, d_molecule_state, d_Q, tot_molecule, tot_site, diff_k, d_new_pos, d_debug, 0);
			//cudaThreadSynchronize());
			//Diffusion_GPU<<<16, 22>>>(devStates, d_molecule_state, d_Q, tot_molecule, tot_site, diff_k, d_new_pos, d_debug, 1);
			cudaThreadSynchronize();
			cudaMemcpy(new_position, d_new_pos, sizeof(int)*tot_molecule, cudaMemcpyDeviceToHost) ;
		
			for (int i=0; i<tot_molecule; i++) {
				if (new_position[i]!=-1) {
					if (molecule_state[i].cell==new_position[i]-1) left++;
					if (molecule_state[i].cell==new_position[i]+1) right++;
					if (molecule_state[i].cell<new_position[i]-1) up++;
					if (molecule_state[i].cell>new_position[i]+1) down++;
					//printf("diffusion\n");
					//if (new_position[i]>=4096) printf("new pos = %d - old pos = %d debug = %d\n",new_position[i],molecule_state[i].cell,debug[i]);
					updateMol(molecule_state, site_state, new_position[i], molecule_state[i].id, molecule_state[i].complex, tot_molecule, tot_site, new_position);
					
				}
			}
		}



		t2 = clock();   

		 diff = (((float)t2 - (float)t1) / 1000000.0F ) * 1000;   
		 printf("%f\n",diff);  
		 
		//printf("\tleft=%d\n\tright=%d\n\tup=%d\n\tdown=%d\n",left,right,up,down);
		// cudaMemcpy(site_state, d_site_state, sizeof(site)*tot_site, cudaMemcpyDeviceToHost) );
		// cudaMemcpy(molecule_state, d_molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyDeviceToHost) );
		 cudaMemcpy(new_position, d_new_pos, sizeof(int)*tot_molecule, cudaMemcpyDeviceToHost) ;
		 cudaMemcpy(debug, d_debug, sizeof(int)*tot_molecule, cudaMemcpyDeviceToHost) ;
		// cudaMemcpy(Q, d_Q, sizeof(int)*CELLS, cudaMemcpyDeviceToHost) );
		

		/* 
		for  (int i=0; i<tot_site; i++) {
			if (site_state[i].bound!=NULL) {
				if (site_state[i].cell!=site_state[i].bound->cell) printf(">erorr binding between different cells %d - %d\n", site_state[i].cell, site_state[i].bound->cell);
			}
		}*/
		//debug
		
		//int test[CELLS];
		/*
		for (int i=0; i<CELLS; i++) {
			//test[i]=0;
			if (Q[i]>=60) printf("max conc exceeded in %d\n", i);
		}
		
		for (int i=0; i<CELLS; i++) {
			if (test[i]!=0) printf("test %d\n", test[i]);
		}
		for (int diff_k=0; diff_k<6; diff_k++) {
			for (int i=0; i<32; i++) {
				for (int j=0; j<22; j++) {
						int row=i*2;
						if (diff_k>=3) row++;
						int column=j*3;
						column+=(diff_k%3);
						if (row%4==2||row%4==3) 
							if (column==BLOCK_W*GRID_W) 
								column=0;
							else column=(column+1);
							if (!(column>=BLOCK_W*GRID_W)) { 
								int CellID=BLOCK_W*GRID_W*row+column;
								test[CellID]++;
								if (CellID<0 || CellID>4096) printf("cellid=%d, row=%d, column=%d\n", CellID,row,column);
								if (CellID==2722 ||CellID==2723 || CellID==2724 ) printf("fatti\n");
							}
				}
			}
		}
		for (int i=0; i<CELLS; i++) {
			//if (test[i]!=1) printf("test %d\n", test[i]);
		}*/

		/*for (int i=0; i<tot_molecule; i++) {
			if (new_position[i]!=-1) {
				//if (new_position[i]>=4096) printf("new pos = %d - old pos = %d debug = %d\n",new_position[i],molecule_state[i].cell,debug[i]);
				updateMol(molecule_state, site_state, new_position[i], molecule_state[i].id, molecule_state[i].complex, tot_molecule, tot_site, new_position);
				
			}
		}*/
		 

		 t1=clock();
		for (int i=0; i<CELLS; i++) {
			for (int j=0; j<reacts; j++) {
				rec[i][j]=0;
			}
		}
		t2 = clock();   
		 diff = (((float)t2 - (float)t1) / 1000000.0F ) * 1000;   
		 printf(">%f\n",diff);   
		 t1=clock();
		tot_rec=0;
		/*int* grec;
		cudaMalloc((void**)&grec, sizeof(int)*reacts*CELLS) ;
		int* recb=(int*)malloc(sizeof(int)*reacts*CELLS);
		for (int i=0; i<reacts*CELLS; i++) {
			recb[i]=0;
		}*/
		for (int j=0; j<reacts; j++) {
			for (int i=abdS[v[j*2]]; i<abdS[v[j*2]+1]; i++) {
			//for(int i=0; i<tot_site; i++) {
				if ((v[j*2]==site_state[i].specie || v[j*2+1]==site_state[i].specie) && (site_state[i].cell!=molecule_state[site_state[i].mol].cell)) {
					rec[site_state[i].cell][j]=1;
					rec[molecule_state[site_state[i].mol].cell][j]=1;
					//printf("entro\n");
					//site_state[i].cell=molecule_state[site_state[i].mol].cell;
				}
			}
			for (int i=abdS[v[j*2+1]]; i<abdS[v[j*2+1]+1]; i++) {
				if ((v[j*2]==site_state[i].specie || v[j*2+1]==site_state[i].specie) && (site_state[i].cell!=molecule_state[site_state[i].mol].cell)) {
					rec[site_state[i].cell][j]=1;
					rec[molecule_state[site_state[i].mol].cell][j]=1;
					//printf("entro\n");
					//site_state[i].cell=molecule_state[site_state[i].mol].cell;
				}
			}
		}
		//cudaMemcpy(recb, grec, sizeof(int)*reacts*CELLS, cudaMemcpyHostToDevice) ;
		for (int i=0; i<tot_site; i++) {
			if (site_state[i].cell!=molecule_state[site_state[i].mol].cell) {
				/*for (int j=0; j<reacts; j++) {  //questo pezzo � troppo lento!!!
					if (v[j*2]==site_state[i].specie || v[j*2+1]==site_state[i].specie) {
						if (rec[site_state[i].cell][j]!=1 || rec[molecule_state[site_state[i].mol].cell][j]!=1) {
							printf("\t\t\t>>>>ERROR %d - %d - %d\n", i, j, rec[site_state[i].cell][j]);
							return -1;
						}
						//rec[site_state[i].cell][j]=1;
						//rec[molecule_state[site_state[i].mol].cell][j]=1;
						//tot_rec+=2;
					}
				}*/
				
				/*int tots=floor((float)(reacts/(64)));
				int rest=reacts-(tots*64);
				CheckDiffSite<<<64,tots>>>(grec,d_v,site_state[i].specie,site_state[i].cell,molecule_state[site_state[i].mol].cell, reacts, 0);
				CheckDiffSite<<<1,rest>>>(grec,d_v,site_state[i].specie,site_state[i].cell,molecule_state[site_state[i].mol].cell, reacts, tots);
				cudaThreadSynchronize();*/
				//if (molecule_state[site_state[i].mol].cell>4096) printf(" error > 4096\n %d old %d", molecule_state[site_state[i].mol].cell, site_state[i].cell);
				
					/*if (site_state[i].cell==molecule_state[site_state[i].mol].cell-1) left++;
					if (site_state[i].cell==molecule_state[site_state[i].mol].cell+1) right++;
					if (site_state[i].cell<molecule_state[site_state[i].mol].cell-1) up++;
					if (site_state[i].cell>molecule_state[site_state[i].mol].cell+1) down++;*/
				site_state[i].cell=molecule_state[site_state[i].mol].cell;
				//printf("diffusion site %d\n", i);
			}
		}
		
		/*
		for  (int i=0; i<tot_site; i++) {
			if (site_state[i].bound!=NULL) {
				if (molecule_state[site_state[i].mol].complex!=molecule_state[site_state[i].bound->mol].complex) {
					printf(">>erorr complex %d - %d\n", site_state[i].cell, site_state[i].bound->cell);
					printf(">>erorr complex %d - %d - %d - %d\n", molecule_state[site_state[i].mol].cell, molecule_state[site_state[i].bound->mol].cell, molecule_state[site_state[i].mol].complex, molecule_state[site_state[i].bound->mol].complex);
				}
			}
		}
		for (int i=1; i<=complex_count; i++) {
			int tcell=-1;
			for (int j=0; j<tot_molecule; j++) {
				if (molecule_state[j].complex==i) {
					if (tcell!=molecule_state[j].cell && tcell==-1) tcell=molecule_state[j].cell;
					else if (tcell!=molecule_state[j].cell) {
						printf(">ERROR complex not in the same cell! %d - %d - %d\n", tcell, molecule_state[j].cell, i);
						exit(0);
					}
				}
			}
		}
		for  (int i=0; i<tot_site; i++) {
			if (site_state[i].bound!=NULL) {
				if (site_state[i].cell!=site_state[i].bound->cell) {
					printf(">>erorr binding between different cells %d - %d\n", site_state[i].cell, site_state[i].bound->cell);
					printf(">>erorr binding between different cells %d - %d - %d - %d\n", molecule_state[site_state[i].mol].cell, molecule_state[site_state[i].bound->mol].cell, molecule_state[site_state[i].mol].complex, molecule_state[site_state[i].bound->mol].complex);
				}
			}
		}*/
		//printf("\tleft=%d\n\tright=%d\n\tup=%d\n\tdown=%d\n",left,right,up,down);
		//copy back grac
		//cudaMemcpy(recb, grec, sizeof(int)*reacts*CELLS, cudaMemcpyDeviceToHost) ;
		//cudaFree(grec);
		/*for (int i=0; i<CELLS; i++) {
			for (int j=0; j<reacts; j++) {
				if (recb[i*reacts+j]>1) printf("%d\n",recb[i*reacts+j]);
				if (recb[i*reacts+j]!=rec[i][j]) {
					printf("\t\t>>>>>>>>ERROR\n%d - %d\n", i,j);
		t2 = clock();   
		 diff = (((float)t2 - (float)t1) / 1000000.0F ) * 1000;   
		 printf(">>%f\n",diff); 
					return -1;
				}
			}
		}
		return -1;*/
		t2 = clock();   
		 diff = (((float)t2 - (float)t1) / 1000000.0F ) * 1000;   
		 printf(">>%f\n",diff);   
		 t1=clock();
		for (int i=0; i<CELLS; i++) {
			for (int j=0; j<reacts; j++) {
				tot_rec+=rec[i][j];
			}
		}
		rec_gpu=(rec_t*)malloc(sizeof(rec_t)*tot_rec);
		int recs=0;
		for (int i=0; i<CELLS; i++) {
			for (int j=0; j<reacts; j++) {
				if (rec[i][j]==1) {
					rec_gpu[recs].cellID=i;
					rec_gpu[recs].react=j;
					recs++;
				}
			}
		}
		t2 = clock();   
		 diff = (((float)t2 - (float)t1) / 1000000.0F ) * 1000;   
		 printf(">>>%f\n",diff);   
		printf("total recs=%d\n",recs);
		//cutilCheckMsg("Diffusion error 2" );
		//cudaThreadSynchronize());
		// cudaMemcpy(molecule_state, d_molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyDeviceToHost) );
		
		//cutilCheckMsg("Diffusion error 3");
	

		//debug mol 1 diffusion
		printf("diffusion mol 1 = %d\n", molecule_state[0].cell);

		for (int i=0; i<nmol; i++) {
			n[i]++;
			if (tau[i]*n[i]>*ts) *ts=tau[i]*n[i];
		}
		//diffusion for ready complexes
		//copy molecule -> host to device
		//copy state -> host to device
		/*if (write%10==0) {
			writeState(tot_molecule, t_sim);
			if (write>0) fprintf(pF2, ",");
			else fprintf(pF2, "{{");
			int tot_degree=0;
			for (int j=0; j<tot_molecule; j++) {
				tot_degree+=molecule_state[ine j].degree;
			}
			fprintf(pF2, "%d", tot_degree);

		}
		write++;*/
		//Diffusion<<<24, 64>>>();
		/*
		for (int i=0; i<SPECIES; i++) {
			if (site_state[i].bound!=NULL) binding++;
		}*/



		for (int i=0; i<tot_site; i++) {
			if (site_state[i].specieBounded!=-1) {
				if (molecule_state[site_state[i].mol].degree==0) {
					printf("ERROR degree0 %d %d\n", i, site_state[i].mol);
				}
			}
		}

		for (int i=0; i<tot_molecule; i++) {
			if (i!=molecule_state[i].id) printf("sfasati %d - %d\n", i, molecule_state[i].id);
		}
		
		int molCompl=0;
		int binding=0;
		for (int i=0; i<tot_molecule; i++) {
			molCompl+=molecule_state[i].degree;
			if (molecule_state[i].degree>100) {
				printf("error degree i=%d degree = %d\n", i, molecule_state[i].degree);
				return 0;
			}
			if (molecule_state[i].cell >= 4096 || molecule_state[i].cell < 0) {
				printf("ERROR %d %d\n", molecule_state[i].cell, i);
				return 0;
			}
		}

		
		for (int i=0; i<CELLS; i++) {
			Q[i]=0;
		}
		//newest diffusion
		for (int i=0; i<tot_molecule; i++) {
			Q[molecule_state[i].cell]++;
		}
		int maxConc=0;
		for (int c=0; c<CELLS;c++) {
			if (Q[c]>maxConc) maxConc=Q[c];
		}
		printf("max conc=%d\n", maxConc);
		printf("mol.compl = %d\n", molCompl);
		//printf("site.compl = %d\n", binding);
		printf("cb = %d\n", cb);
		printf("cun = %d\n", cun);
		int avg=0;
		int avg2=0;
		for (int i=38; i>=0; i--) {
			history[i+1]=history[i];
			avg+=history[i+1];
		}
		for (int i=20; i>=0; i--) {
			avg2+=history[i+1];
		}
		history[0]=molCompl;
		//printf("avg = %d\n", avg);
		avg+=history[0];
		avg=avg/40;
		int err=(history[0]*3)/1000;
		int err2=(history[0]*30)/1000;
		printf("avg+err=%d avg-err=%d err=%d avg=%d\n", avg+err, avg-err, err, avg);
		if (avg2+err2>molCompl && avg2-err2<molCompl) {
			if (old_ts+0.1<*ts) {
				writeCompl(map); //steady state
				old_ts=*ts;
			}
		}
		if (*ts>(tm-0.4)) printf("prova\n");
		if ((*ts>=(tm-0.4)) && (old_ts==0)) {
			writeCompl(map); //steady state
			old_ts=tm-0.4;
		}
		if ((*ts>=(tm-0.2)) && (old_ts==(tm-0.4))) {
			writeCompl(map); //steady state
			old_ts=*ts;
		}

		if (avg+err>molCompl && avg-err<molCompl) quit=true; //steady state
		printf("Diff time: %d seconds\n", time(NULL)-last);

		for (int i=0; i<tot_molecule; i++) {
			if (molecule_state[i].size>maxConc) printf("size %d - compl %d - mol %d - cell %d\n", molecule_state[i].size, molecule_state[i].complex, i, molecule_state[i].cell);
			if (molecule_state[i].complex==0 && molecule_state[i].size!=1) printf("error SIZE\n");
		}
		/*for (int i=0; i<tot_molecule; i++) {
			int ind=0;
			int j;
			while (StoM[i][ind]!=-1) {
				j=StoM[i][ind];
				if (site_state[j].cell!=molecule_state[i].cell && site_state[j].mol==i) printf("error cell property violated on %s - %s!\t%d - %d\n", map[molecule_state[i].specie], mapS[site_state[j].specie], molecule_state[i].cell, site_state[j].cell);
				ind++;
			}
		}
		for  (int i=0; i<tot_site; i++) {
			if (site_state[i].bound!=NULL) {
				if (site_state[i].cell!=site_state[i].bound->cell) printf(">>>error binding between different cells %d - %d\n", site_state[i].cell, site_state[i].bound->cell);
			}
		}*/
		// cudaMemcpy(ts, d_ts, sizeof(float), cudaMemcpyDeviceToHost) );
		//*ts+=0.0025;
		//cutilCheckMsg("Diffusion error");
	}


//cudaPrintfDisplay(stdout, true);
	//cudaPrintfEnd();

    //cutilCheckMsg("kernel error");
//#ifdef _DEBUG
 //    cudaThreadSynchronize() );
//#endif


	//CloseSim<<<1,1>>>(d_max_complex, d_site_state, d_molecule_state, tot_molecule, tot_site);
	
	// cudaMemcpy(site_state, d_site_state, sizeof(site)*tot_site, cudaMemcpyDeviceToHost) );
	// cudaMemcpy(molecule_state, d_molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyDeviceToHost) );

    printf("Simulation ended, analyzing output...\n");

	// cudaMemcpy(max_complex, d_max_complex, sizeof(int), cudaMemcpyDeviceToHost) );
	//*max_complex=1;
	printf("#complex %d\n", complex_count);
	printf("Final abundances\n");


	float* degree=(float*)malloc(sizeof(float)*(complex_count+1));
	for (int i=0; i<=complex_count; i++) {
		degree[i]=0;
	}

    // cudaMalloc((void**)&d_degree, sizeof(float)*(*max_complex)) );
	// cudaMemcpy(d_degree, degree, sizeof(float)*(*max_complex), cudaMemcpyHostToDevice) );
	//ComplexDegree<<<1,1>>>(d_degree, d_site_state, d_molecule_state, tot_molecule, tot_site);
	//cudaThreadSynchronize());
	// cudaMemcpy(degree, d_degree, sizeof(float)*(*max_complex), cudaMemcpyDeviceToHost) );
	writeState(tot_molecule, t_sim);

    for (int j=0; j<tot_molecule; j++) {
		//printf("molecule %d: %s %d\n", molecule_state[j].id, map[molecule_state[j].specie], molecule_state[j].complex);
	}

	
	for (int i=0; i<=complex_count; i++) {
		fprintf(pF2, "#%d\n", i);
		int tcell=0;
		for (int j=0; j<tot_molecule; j++) {
			if (molecule_state[j].complex==i) {
				if (i!=0) degree[i]=1.0;
				fprintf(pF2, "%s - %d - %d\n", map[molecule_state[j].specie], molecule_state[j].degree, j);
				
				if (tcell!=molecule_state[j].complex && tcell==0) tcell=i;
				else if (tcell!=molecule_state[j].complex) printf("ERROR complex not in the same cell!\n");
			}
		}
		fprintf(pF2, "%f\n", degree[i]);
	}

	fflush(pF2);
	
	for (int s1=0; s1<tot_site; s1++) {
		if (site_state[s1].specieBounded>=0) {
			fprintf(pF3, "%s_%d,%s_%d,%d\n", map[molecule_state[site_state[s1].bound->mol].specie], site_state[s1].bound->mol, map[molecule_state[site_state[s1].mol].specie], site_state[s1].mol, molecule_state[site_state[s1].mol].complex);
		}
	}
	
	


	/*for (int i=0; i<tot_molecule; i++) {
		for (int j=0; j<tot_site; j++) {
			if (site_state[j].cell!=molecule_state[i].cell && site_state[j].mol==i) printf("error cell property violated on %s - %s!\t%d - %d\n", map[molecule_state[i].specie], mapS[site_state[j].specie], molecule_state[i].cell, site_state[j].cell);
		}
	}

	printf("\n");

	for (int j=0; j<tot_site; j++) {
		//printf("site %d: %s %d %d %d\n", j, mapS[site_state[j].specie], site_state[j].mol, (site_state[j].bound!=NULL?1:0), site_state[j].cell);
	}*/
	printf("bindings: %d - unbindings: %d\n", cb, cun);
	printf("Execution time: %d seconds\n", time(NULL)-seconds);
	fprintf(pF2, "Execution time: %d seconds\n", time(NULL)-seconds);
	fflush(pF2);
	fclose(pF2);
	fflush(pF3);
	fclose(pF3);
	//debug
	for (int i=0; i<reacts; i++) {
		//printf("%d: %d\n", i, reactCount[i]);
	}
    Cleanup();
	//exit(0);
}

void Cleanup(void)
{
	DeleteCriticalSection(&cs_mutex);
	DeleteCriticalSection(&cs_sync);
	DeleteCriticalSection(&cs_cb);
	DeleteCriticalSection(&cs_cu);
    // Free device memory
   /* if (d_speciesAbd)
        cudaFree(d_speciesAbd);

    if (d_c)
        cudaFree(d_c);

    if (d_v)
        cudaFree(d_v);
    // Free host memory
	
	for (int i=0; i<CELLS; i++) {
		if (h_speciesAbd[i])
			free(h_speciesAbd[i]);
	}
	
	for (int i=0; i<REACTIONS; i++) {
		if (h_v[i]) free(h_v[i]);
	}*/

    checkCudaErrors( cudaThreadExit() );
    

    fflush( stdout);
    printf("\nPress ENTER to exit...\n");
    fflush( stdout);
    fflush( stderr);
    getchar();

    exit(0);
}

float Exponential(float lambda, float u) {
	return ((-1)/lambda)*logf(u);
}

//__device__ int Weights(float* a, int CellID) {
int Weights(float* a, double n, int cell) {
	//int mu;
	float a1;
		//,a2;
	//for (mu=0; mu<10; mu++) {
		a1=0;
		//for (int j=0; j<REACTIONS+MOLECULES; j++) {
		for (int j=0; j<reacts*2; j++) {
			if (j==0) a1=0;
			else a1+=a[cell*reacts*2+j-1];
			//if (a1<=a0[threadIdx.x]) {
			if (a1<=n) {
				//a2=a1+a[j];
				//for (int j=0; j<mu; j++) a2+=a[j];
				//if (a0[CellID]<a2) {
				//if (a0[threadIdx.x]<a1+a[j]) {
				if (n<a1+a[cell*reacts*2+j]) {
					//printf ("a1:%f, a0:%f, a2:%f\nmu:%d, a[mu]:%f\n", a1, a0, a2, j, a[j]);
					return j;
				}
			}
		}
	//}
	//no more reactions possible!
	return -1;
}

int Weights2(double n, int cell) {
	//int mu;
	float a1;
		//,a2;
	//for (mu=0; mu<10; mu++) {
		a1=0;
		//for (int j=0; j<REACTIONS+MOLECULES; j++) {
		for (int j=0; j<reacts*2; j++) {
			if (j==0) a1=0;
			else a1+=g_a[cell*reacts*2+j-1];
			//if (a1<=a0[threadIdx.x]) {
			if (a1<=n) {
				//a2=a1+a[j];
				//for (int j=0; j<mu; j++) a2+=a[j];
				//if (a0[CellID]<a2) {
				//if (a0[threadIdx.x]<a1+a[j]) {
				if (n<a1+g_a[cell*reacts*2+j]) {
					//printf ("a1:%f, a0:%f, a2:%f\nmu:%d, a[mu]:%f\n", a1, a0, a2, j, a[j]);
					return j;
				}
			}
		}
	//}
	//no more reactions possible!
	return -1;
}

__device__ float h(site* s_site_state, molecule* s_molecule_state, float c, const int* v, int i, int CellID, const int* abdS, int block, int cur_tr, int tot_molecules) {
	/*
	dimensione accettabile REACTIONS*2*CELLS 
	memo[MOLECULES][NUMSITE]; fatto all'inizio 

	*/
	//int order=0;
	float p=0; //propensity
	//int tmp=0;
	//errore dopo la prima reazione rimane a 0!!!
	//int deb=0;
	//int deb2=0;
	//bool debug=false;
	//if (i==1 && CellID==578) CUPRINTF("debug: %d %d\n", v[i*2], v[i*2+1]);
	//int uno=0;
	//int due=0;
	/*for (int j=0; j<MOLECULES; j++) {
		if (s_molecule_state[j].cell==CellID) {
			memo[CellID][j][0]=CountSiteofSpec(s_site_state, v[i*2], CellID, tot_site, j, abdS);
			//if (i==1 && memo[CellID][j][0]>0) uno++;
			memo[CellID][j][1]=CountSiteofSpec(s_site_state, v[i*2+1], CellID, tot_site, j, abdS);
			//if (i==1 && memo[CellID][j][1]>0) due++;
		} else {
			memo[CellID][j][0]=0;
			memo[CellID][j][1]=0;
		}
	}*/
	
	__shared__ memo_gpu memos[64*21];
	for (int j=0; j<21; j++) {
		memos[cur_tr*21+j].molID=0;
		memos[cur_tr*21+j].val[0]=0;
		memos[cur_tr*21+j].val[1]=0;
	}

	/*for (int j=0; j<tot_molecules; j++) {
		if (s_molecule_state[j].cell==CellID) memo[block][cur_tr][j][0]=0;
	}*/

	unsigned int tmp1=CountSiteofSpec(s_site_state, v[i*2], CellID, abdS, 0, cur_tr, block, memos);
	
	if (tmp1!=0) {
		/*for (int j=0; j<tot_molecules; j++) {
			memo[block][cur_tr][j][1]=0;
		}*/
		unsigned int tmp2=CountSiteofSpec2(s_site_state, v[i*2+1], CellID, abdS, 1, cur_tr, block, memos);
		if (tmp2!=0) {
			/*for (int j=0; j<tot_molecules; j++) {
				if (memo[block][cur_tr][j][0]!=0) {
					if (s_molecule_state[j].cell==CellID) {
						p+=memo[block][cur_tr][j][0]*(tmp2-memo[block][cur_tr][j][1]);
					}
				}
			}*/
			for (int j=0; j<21 && (memos[cur_tr*21+j].val[0]!=0 || (memos[cur_tr*21+j].val[1]!=0)); j++) {  //efficient
				//if (memos[cur_tr*42+j].val[0]!=0) {
					//if (s_molecule_state[memos[cur_tr*42+j].molID].cell==CellID) { //penso di poterlo rimuovere
						p+=memos[cur_tr*21+j].val[0]*(tmp2-memos[cur_tr*21+j].val[1]);
					//}
				//}
			}
		}
	}


	if (p<=0) p=0;
	//else p=p*c;
	else p=p*c*100;
	return p;
}



float h_CPU(site* s_site_state, molecule* s_molecule_state, float c, const int* v, int i, int CellID, const int* abdS, int cpu_t) {
	/*
	dimensione accettabile REACTIONS*2*CELLS 
	memo[MOLECULES][NUMSITE]; fatto all'inizio 

	*/
	//int order=0;
	float p=0; //propensity
	//int tmp=0;
	//errore dopo la prima reazione rimane a 0!!!
	//int deb=0;
	//int deb2=0;
	//bool debug=false;
	//if (i==1 && CellID==578) CUPRINTF("debug: %d %d\n", v[i*2], v[i*2+1]);
	//int uno=0;
	//int due=0;
	/*for (int j=0; j<MOLECULES; j++) {
		if (s_molecule_state[j].cell==CellID) {
			memo[CellID][j][0]=CountSiteofSpec(s_site_state, v[i*2], CellID, tot_site, j, abdS);
			//if (i==1 && memo[CellID][j][0]>0) uno++;
			memo[CellID][j][1]=CountSiteofSpec(s_site_state, v[i*2+1], CellID, tot_site, j, abdS);
			//if (i==1 && memo[CellID][j][1]>0) due++;
		} else {
			memo[CellID][j][0]=0;
			memo[CellID][j][1]=0;
		}
	}*/

	//printf("memo\n");
	
	
	for (int j=0; j<tot_molecule; j++) {
		//if (s_molecule_state[j].cell==CellID) memo2[cpu_t][j][0]=0;
		memo2[cpu_t][j][0]=0;
		//printf("molecule cell %d\n", s_molecule_state[j].cell);
		//memo[CellID][j][1]=0;
	}
	unsigned int tmp1=CountSiteofSpec_CPU(s_site_state, v[i*2], CellID, abdS, 0, cpu_t);
	
	
	if (tmp1!=0) {
		for (int j=0; j<tot_molecule; j++) {
			memo2[cpu_t][j][1]=0;
		}
		unsigned int tmp2=CountSiteofSpec2_CPU(s_site_state, v[i*2+1], CellID, abdS, 1, cpu_t);
		if (tmp2!=0) {
			for (int j=0; j<tot_molecule; j++) {
					//if (s_molecule_state[j].cell==CellID && s_molecule_state[j].degree<6) {
				if (memo2[cpu_t][j][0]!=0) {
					if (s_molecule_state[j].cell==CellID) {
						p+=memo2[cpu_t][j][0]*(tmp2-memo2[cpu_t][j][1]);
						//p+=__mul24(memo[CellID][j][0],(tmp2-memo[CellID][j][1]));
					}
				}
			}
		}
	}
	
	if (p<=0) p=0;
//	else p=p*c;
	else p=p*c*100;
	return p;
}

//__device__ float hu(site* s_site_state, molecule* s_molecule_state, const float* c, const int* v, int i, int CellID, const int* abdS) {
__device__ float hu(site* s_site_state, molecule* s_molecule_state, float c, const int* v, int i, int CellID, const int* abdS) {
	/*
	float p=1;
	bool found;
	bool never=true;
	unsigned short int k;
	
	for (int j=0; j<SPECIES; j++) {
		if (s_site_state[j].mol==i && s_site_state[j].bound!=NULL) {
			//CUPRINTF("debug2!\n");
			never=false;
			found=false;
			for (k=0; k<REACTIONS*2 && !found; k+=2) {
				if (v[k]==s_site_state[j].specie && v[k+1]==s_site_state[j].bound->specie) found=true;
			}
			p*=c[k];
		}
	}
	if (never) {
		p=0;
		//CUPRINTF("debug!\n");
	}
	
	return p*10;*/
	float p=0; //propensity
	bool debug=false;

	for (int  j=abdS[v[i*2]]; j<abdS[v[i*2]+1]; j++) {
		//if (s_site_state[j].cell==CellID && s_site_state[j].bound!=NULL) { //wrong
		if (s_site_state[j].cell==CellID && s_site_state[j].specieBounded!=-1) { 
			if (s_site_state[j].specieBounded==v[i*2+1]) {
				p++;
			}
			//if (i==12) CUPRINTF("debug! %d\n", s_site_state[j].bound->specie);
		}
	}

	if (p<=0) p=0;
	else p=p*c;
	//return p*10; 
	return p*0.5; 
	//return 0;
}


float hu_CPU(site* s_site_state, molecule* s_molecule_state, float c, const int* v, int i, int CellID, const int* abdS) {
	/*
	float p=1;
	bool found;
	bool never=true;
	unsigned short int k;
	
	for (int j=0; j<SPECIES; j++) {
		if (s_site_state[j].mol==i && s_site_state[j].bound!=NULL) {
			//CUPRINTF("debug2!\n");
			never=false;
			found=false;
			for (k=0; k<REACTIONS*2 && !found; k+=2) {
				if (v[k]==s_site_state[j].specie && v[k+1]==s_site_state[j].bound->specie) found=true;
			}
			p*=c[k];
		}
	}
	if (never) {
		p=0;
		//CUPRINTF("debug!\n");
	}
	
	return p*10;*/

	float p=0; //propensity
	bool debug=false;

	for (int  j=abdS[v[i*2]]; j<abdS[v[i*2]+1]; j++) {
		//if (s_site_state[j].cell==CellID && s_site_state[j].bound!=NULL) {
		if (s_site_state[j].cell==CellID && s_site_state[j].specieBounded!=-1) {
			if (s_site_state[j].specieBounded==v[i*2+1]) p++;
		}
	}

	if (p<=0) p=0;
	else p=p*c;
	//return p*10; 
	return p*0.5; 
	//return 0;
}

__device__ int CountSiteofSpec(site* s_site_state, int i, int CellID, const int* abdS, int uno, int r, int block, memo_gpu* memos) {
	//int order=0;
	//int count=0; //propensity
	/*//site* lst = domLst;
	//if (abdS[i]>1500 || abdS[i+1]>1500) CUPRINTF("%d\n", abdS[i+1]);
	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
	//for (int k=0; k<tot_site ; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].mol==mol) count++;
	}
	return count;

	for (int j=0; j<MOLECULES; j++) {
		memo[CellID][j][uno]=0;
	}
	*/
	
	bool inserted=false;
	int mol;

	unsigned int count=0; 
	for (int k=abdS[i], int kcounter=pow(2.0f,(abdS[i]%32)); k<abdS[i+1] ; k++, kcounter=kcounter*2) {
		kcounter=kcounter%32;

		if (s_site_state[k].specieBounded==-1 && s_site_state[k].cell==CellID) {
		//if ((speciesBounded[(k/32)] & kcounter)==0) {
			//if (s_site_state[k].cell==CellID) {
				inserted=false;
				mol=s_site_state[k].mol;
				for (int j=0; j<21 && inserted==false; j++) {   //efficient
					if (memos[r*21+j].val[1]==0 && memos[r*21+j].val[0]==0) {
						inserted=true;
						memos[r*21+j].molID=mol;
						memos[r*21+j].val[uno]++;
					}
					else if (memos[r*21+j].molID==mol) {
						inserted=true;
						memos[r*21+j].val[uno]++;
					}
				}
				//memo[block][r][s_site_state[k].mol][uno]++;
				count++;
			//}
		}
	}
	return count;
}

int CountSiteofSpec_CPU(site* s_site_state, int i, int CellID, const int* abdS, int uno, int cpu_t) {
	//int order=0;
	//int count=0; //propensity
	/*//site* lst = domLst;
	//if (abdS[i]>1500 || abdS[i+1]>1500) CUPRINTF("%d\n", abdS[i+1]);
	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
	//for (int k=0; k<tot_site ; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].mol==mol) count++;
	}
	return count;

	for (int j=0; j<MOLECULES; j++) {
		memo[CellID][j][uno]=0;
	}
	*/
	unsigned int count=0; 
	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
		if (s_site_state[k].specieBounded==-1 && s_site_state[k].cell==CellID) {
			memo2[cpu_t][s_site_state[k].mol][uno]++;
			count++;
		}
	}
	return count;
}
__device__ int CountSiteofSpec2(site* s_site_state, int i, int CellID,const int* abdS, int uno, int r, int block, memo_gpu* memos) {
	//int order=0;
	unsigned int count=0; //propensity
	/*//site* lst = domLst;
	//if (abdS[i]>1500 || abdS[i+1]>1500) CUPRINTF("%d\n", abdS[i+1]);
	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
	//for (int k=0; k<tot_site ; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].mol==mol) count++;
	}
	return count;
*/
	bool inserted=false;

	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
		if (s_site_state[k].specieBounded==-1 && s_site_state[k].cell==CellID) {
			inserted=false;
			for (int j=0; j<21 && inserted==false; j++) {   //efficient
				if (memos[r*21+j].val[1]==0 && memos[r*21+j].val[0]==0) {
					inserted=true;
					memos[r*21+j].molID=s_site_state[k].mol;
					memos[r*21+j].val[uno]++;
				}
				else if (memos[r*21+j].molID==s_site_state[k].mol) {
					inserted=true;
					memos[r*21+j].val[uno]++;
				}
			}
			//memo[block][r][s_site_state[k].mol][uno]++;
			count++;
		}
	}
	return count;
}

int CountSiteofSpec2_CPU(site* s_site_state, int i, int CellID,const int* abdS, int uno, int cpu_t) {
	//int order=0;
	unsigned int count=0; //propensity
	/*//site* lst = domLst;
	//if (abdS[i]>1500 || abdS[i+1]>1500) CUPRINTF("%d\n", abdS[i+1]);
	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
	//for (int k=0; k<tot_site ; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].mol==mol) count++;
	}
	return count;
*/

	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
		if (s_site_state[k].specieBounded==-1 && s_site_state[k].cell==CellID) {
			memo2[cpu_t][s_site_state[k].mol][uno]++;
			count++;
		}
	}
	return count;
}

__device__ int CountSiteofSpec_u(site* s_site_state, int i, int CellID, int mol, const int* abdS) {
	//int order=0;
	int count=0; //propensity
	//site* lst = domLst;
	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
		if (s_site_state[k].cell==CellID && s_site_state[k].specie==i && s_site_state[k].bound==NULL && s_site_state[k].mol==mol) count++;
	}
	return count;
}

void chooseSite(int type1, int type2, int* s1, int* s2, float u, site* s_site_state, int CellID, const int* abdS, int mu) {
	unsigned int tot_s1=0;
	unsigned int tot_s2=0;
	//non conto che devo essere nella stessa cella!!!
	//printf("debug abds %d %d\n", abdS[type1],abdS[type1+1]);
	for (int k=abdS[type1]; k<abdS[type1+1]; k++) {
		//printf("\trcell %d\n",s_site_state[k].cell);
		//if (s_site_state[k].cell==CellID) printf("debug 1.1\n");
		if (s_site_state[k].specieBounded==-1 && s_site_state[k].cell==CellID) tot_s1++;
	}
	
	//if (tot_s1==0) CUPRINTF("error! %d - %d\n", type1, type2); 
	int ts1;
	if (tot_s1>0) ts1=(tot_s1-1)*u;
	else ts1=-1;
	//printf("debug tot s1 %d rand %f\n", ts1, u);
	//int tmp=tot_s1;
	tot_s1=0;
	//bool debug=false;
	int first_mol;
	for (int k=abdS[type1]; k<abdS[type1+1]; k++) {
		if (s_site_state[k].specieBounded==-1 && s_site_state[k].cell==CellID) {
			//debug=true;
			//printf("debug2\n");
			if (tot_s1==ts1) { 
				*s1=k; 
				first_mol=s_site_state[k].mol;
			}
			tot_s1++;
		}
	}
	if (*s1==0) printf("errore ts1=%d mu=%d\n", ts1, mu);
	//if (!debug) CUPRINTF("error: u=%f, ts1=%f, tot_s1 %d\n", u, ts1, tmp); //perch� tot_s1, ora tmp, � a 0 e la propensit� non � andata a 0 facendo scegliere questa reazione?!?!?
	
	for (int k=abdS[type2]; k<abdS[type2+1]; k++) {
		if (s_site_state[k].specieBounded==-1 && s_site_state[k].mol!=first_mol && s_site_state[k].cell==CellID) tot_s2++;
	}
	//tmp=tot_s2;
	//int ts2=(tot_s2-1)*u;
	if (tot_s2>0) ts1=(tot_s2-1)*u;
	else ts1=-1;
	tot_s2=0;
	for (int k=abdS[type2]; k<abdS[type2+1]; k++) {
		if (s_site_state[k].specieBounded==-1 && s_site_state[k].mol!=first_mol && s_site_state[k].cell==CellID) {
			if (tot_s2==ts1) *s2=k;
			tot_s2++;
		}
	}
}


void chooseSite_u(int type1, int type2, int* s1, int* s2, float u, site* s_site_state, int CellID, const int* abdS) {
	int tot=0;
	int tot_s2=0;
	int tmp1=0;
	int tmp2=0;
	
	int k,j;
	for (k=abdS[type1]; k<abdS[type1+1]; k++) {
		//if (s_site_state[k].cell==CellID && s_site_state[k].bound!=NULL) {
		if (s_site_state[k].cell==CellID && s_site_state[k].specieBounded==type2) {
			//if (s_site_state[k].bound->specie==type2) tot++;
			tot++;
		}
	}
	float r=u*tot;
	tot=0;
	int flag=1;
	for (k=abdS[type1]; k<abdS[type1+1]; k++) {
		//if (s_site_state[k].cell==CellID && s_site_state[k].bound!=NULL) {
		if (s_site_state[k].cell==CellID && s_site_state[k].specieBounded==type2) {
			//if (s_site_state[k].specieBounded==type2) {
				
				tot++;
				if (tot>=r && flag==1) {
					tmp1=k;
					for (j=abdS[type2]; j<abdS[type2+1]; j++) {
						if(s_site_state[k].bound==&s_site_state[j]) {
							tmp2=j;
							flag=0;
						}
					}
					
					
					
				}
		
			//}
		}
	}
	*s1=tmp1;
	*s2=tmp2;
}

void updateComplex(molecule* s_molecule_state, int c, int cellID,int newc) {
	//int order=0;

	for (int k=0; k<tot_molecule ; k++) {
		if (s_molecule_state[k].complex==c) {
			s_molecule_state[k].complex=newc;
			
			if (s_molecule_state[k].cell!=cellID || s_molecule_state[k].cell!=cellID) {
				printf("writing on the wrong cell2\n");
			}
		}
	}
	//while (lst!=NULL) {
		//if (lst->s.specie==i && lst->s.bound==false) count++;
		//lst=lst->next;
		//if (lst!=NULL) if (lst->s.specie==i && lst->s.bound==false) count++;
	//}
}


void updateMol(molecule* s_molecule_state, site* s_site_state, int cell, int id, int compl, int tot_molecule, int tot_site, int* move) {
	
	
	if (compl!=0) {
		//added recently
		//printf("complex diffusion\n");
		for (int i=0; i<tot_molecule; i++) {
			if (s_molecule_state[i].complex==compl) {
				
				s_molecule_state[i].cell=cell;
				move[i]=-1;
			}
		}	
	}
	else {
		s_molecule_state[id].cell=cell;
		move[id]=-1;
	}
}

__device__ void updateC(molecule* s_molecule_state, int compl, site* s1) {
	
/*	site* s;
	for (int i=0; i<tot_m; i++) {
		if (s_molecule_state[i].compl==compl) {
			for (int j=0; j<s_site_state; j++) {
				if (s_site_state[j].mol==i) {
					s=s_site_state[j];
					while (s->bound!=NULL) {
						s=s->bound;
						s_molecule_state[s->mol]=compl;	
					}
				}
			}
		}
	}
	*/
	/*site* s = s1;
	while (s->bound!=NULL) {
		s=s->bound;
		s_molecule_state[s->mol]=compl;
		
	}*/
}

__device__ void autocomplexation(site* s_site_state, molecule* s_molecule_state, int compl, const int* v, int tot_m, int tot_s, int reacts, curandState* localState) {
	bool next;
	float u;
	//manca da controllare che il site non sia bound
	for (int i=0; i<tot_m; i++) {
		if (s_molecule_state[i].complex==compl) {
			for (int k=0; k<tot_s; k++) {
				if (s_site_state[k].mol==s_molecule_state[i].id && s_site_state[k].bound==NULL) {
					for (int l=0; l<reacts*2; l++) {
						if(s_site_state[k].specie==v[l]) {
							if (l%2==0) next=true;
							else next=false;
							for (int j=0; j<tot_m; j++) {  //allora cerco la seconda specie in una molecola diversa
								if (i!=j && s_molecule_state[j].complex==compl) {
									for (int m=0; m<tot_s; m++) {
										if (s_site_state[m].mol==s_molecule_state[j].id && s_site_state[m].bound==NULL && ((s_site_state[m].specie==v[l+1] && next) || (s_site_state[m].specie==v[l-1] && !next))) {
											u=curand_uniform_double(localState);
											if (u>0.1) {
												s_site_state[m].bound=&s_site_state[k];
												s_site_state[k].bound=&s_site_state[m];
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

// Parse program arguments
void ParseArguments(int argc, char** argv)
{
	for (int i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "--displaystep") == 0 ||
			strcmp(argv[i], "-dispalystep") == 0) 
		{
            displaystep = true;
        }
        if (strcmp(argv[i], "-t") == 0) 
		{
			//if (isdigit(atof(argv[i+1]))) 
            tm = atof(argv[i+1]);
			
        }
        if (strcmp(argv[i], "-l") == 0) 
		{
			if (isdigit(atoi(argv[i+1]))) 
            l = atoi(argv[i+1]);
        }
        if (strcmp(argv[i], "-f") == 0) 
		{
			if (strcmp(argv[i+1], "")!=0) 
            inputfile = argv[i+1];
        }
        if (strcmp(argv[i], "-d") == 0) 
		{
			//printf("entro in D\n");
			//if (isdigit(atoi(argv[i+1]))) 
            devs = atoi(argv[i+1]);
			//printf("device is %d\n", devs);
        }
        if (strcmp(argv[i], "-h") == 0) 
		{
			printf("Welcome to GPU Gillespie Multi-Particle\n usage:\n--displaystep\tshow current state after each diffusion\n-t\tmaximum simulation time\n-l\tlattice size\n-f\tinput file containing reactions and initial quantities\n-h\thelp"); 
			exit(0);
        }
	}
}


void writeState(int tot_molecule, float ts) {
	FILE *pFile;
	//FILE *pFile2;
	if (ts==0.0) {
		pFile = fopen("Trajectory_Molecule.out", "w"); 
	}
	else {
		pFile = fopen("Trajectory_Molecule.out", "a");
	}
	//int debugType[PROT_SPECIES];
	for (int i=0; i<CELLS; i++) { 
		for (int j=0; j<nmol; j++) {
			counter[i][j]=0;
			//debugType[j]=0;
		}
	}
	for (int i=0; i<tot_molecule; i++) {
			counter[molecule_state[i].cell][molecule_state[i].specie]++;
			//debugType[molecule_state[i].specie]++;
	}
	fprintf(pFile,"Abundances at time: %f\n", ts);
	for (int j=0; j<nmol; j++) {
		for (int i=0; i<CELLS; i++) { 
			fprintf(pFile, "%d\t", counter[i][j]);
		}
		//if (ts>=tm) printf("Specie %d: %d\n", j, debugType[j]);
		//fprintf(pFile,"Species %d\n", j);
		/*for (int i=0; i<GRID_H*BLOCK_H; i++) { //row
			for (int k=0; k<GRID_W; k++) { //column
				for (int q=0; q<BLOCK_W; q++) {
					fprintf(pFile, "%d\t", counter[(i*BLOCK_W)%(BLOCK_H*BLOCK_W)+(i/BLOCK_H)*(BLOCK_W*BLOCK_H*GRID_W)+k*THREADS+q][j]);
				}
			}
		} */
		fprintf(pFile,"\n");
	}
	fflush(pFile);
	
	fclose(pFile);
}

void writeCompl(char** map) {
	float* degree=(float*)malloc(sizeof(float)*(complex_count+1));
	for (int i=0; i<=complex_count; i++) {
		degree[i]=0;
	}
	char filename[20];
	resN++;
	printf("printing... %d", resN);
	sprintf(filename, "resultCOMPLEX%d", resN);
	printf("printing... %s", filename);
	FILE *pFi;
	pFi = fopen(filename, "w"); 
	for (int i=0; i<=complex_count; i++) {
		fprintf(pFi, "#%d\n", i);
		int tcell=0;
		for (int j=0; j<tot_molecule; j++) {
			if (molecule_state[j].complex==i) {
				if (i!=0) degree[i]=1.0;
				fprintf(pFi, "%s - %d - %d\n", map[molecule_state[j].specie], molecule_state[j].degree, j);
				//fprintf(pFi, "%d - %d\n",  molecule_state[j].degree, j);
				
				if (tcell!=molecule_state[j].complex && tcell==0) tcell=i;
				else if (tcell!=molecule_state[j].complex) printf("ERROR complex not in the same cell!\n");
			}
		}
		fprintf(pFi, "%f\n", degree[i]);
	}



	fflush(pFi);
	fclose(pFi);
	free(degree);
}