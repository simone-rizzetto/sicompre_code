#ifndef SBMLPC
#define SBMLPC
#include <stdio.h>
#include <stdlib.h>
//#include <cutil_inline.h>
#include "sbmlParserC_compart.h"
#include "GPGMPComplex_compart.cuh"
 
int getIDbyName(const char** , const char* , int);
int parser(char*, site*, molecule* , int* , float* , float* , float* ,int , int, int , char**, char**, int, char**, unsigned short*, int**, int*, int*, int*);
int SBML_getNumSpecies(char* filename);
int SBML_getNumSpeciesTypes(char* filename);
int SBML_getNumSpeciesTypesTot(char* filename);
int SBML_getNumSitesTot(char* filename);
int SBML_getNumReactions(char* filename);

#endif