// simulationCPU.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include "sbmlParser.h"


#define SPECIES 10
#define REACTIONS 10	
#define CELLS 384	//# of subvolumes
#define BLOCKS 12	//a block is executed by a processor, my GPU has 12 processor, so is better choose a multiple of 12
#define THREADS 32	//threads executed in each block, for max occupancy choose a multiple of 32, 32 is the warp size

#define GRID_W 4	//grid width = 6*8 subvolumes
#define GRID_H 3	//grid height = 4*8 subvolumes
#define BLOCK_W 4
#define BLOCK_H 8

//AVL



struct node {
	double t;
	int j;
	int h;
	node* next;
};

typedef struct MyData1 {
	float tmax;
	int i;
} MYDATA1, *PMYDATA1;

typedef struct MyData2 {
	int** s_speciesAbd;
	int* t_speciesAbd;
	int i;
} MYDATA2, *PMYDATA2;
// Variables
HANDLE Thread1[512];
int species;
//bool noprompt = false;
PMYDATA1 pDataArray[CELLS];
PMYDATA2 pDataArray2[CELLS];

//eventQueue* eq; //event queue
int reacts=0;
int nspec=0;
float tm=1.0;
float l=0.4;
bool displaystep=false;
int cells; //number of subvolume in the grid
int* totParticles;
float* c; //vector of rates;
float* d_ts;
int** v; //particle used in reaction, example: v[i][j]==-1 the reaction i consumed 1 particle j;
int** d_v;
int** h_v;
int** speciesAbd; //corresponds to internal state of a LP, on CPU
int** d_speciesAbd; //corresponds to internal state of a LP, on Device
int** h_speciesAbd; //corresponds to internal state of a LP, on Host
double** rates; //lambda value
float* ts; //time of the next diffusion event
int n[SPECIES]; 
float tau[SPECIES]; 

// Functions
void Cleanup(void);
float Exponential(float, float);
float h(int*, float, int*, int);
int Weights(float*, float, int);
void ParseArguments(int, char**);

// Device code

//Direct Method
DWORD WINAPI DM(LPVOID lpParam) {
	//int** s_speciesAbd,int** s_v, const float* s_c, float tmax, float* s_deb, int i
	PMYDATA1 pDataArray1 = (PMYDATA1)lpParam;
	//int species=10;
	//int reactions=10;
	float a0=0;
	float a[REACTIONS]; //10 is the reaction
	//int v[REACTIONS][SPECIES];
	//float c[10];
	float tau;
	int mu;
	float u,u2;
	float t_max=pDataArray1->tmax;
	int CellID=pDataArray1->i;
	
	if (CellID>383||CellID<0) printf("CellID:%d-",CellID);
	srand ( time(NULL) );
		
	float t=0;
	//s_deb[0]=t_max;
	while (t<t_max) {
		a0=0;
		for (int j=0; j<reacts; j++) {
			a[j]=h(speciesAbd[CellID], c[j], v[j], nspec);
			a0+=a[j];
		}

		u=rand() / double(RAND_MAX);
		u2=rand() / double(RAND_MAX);
		tau=Exponential(a0,u);
		mu=Weights(a,a0*u2, reacts);
		if (mu==-1) break;
		for (int i=0; i<nspec; i++) {
			speciesAbd[CellID][i]+=v[mu][i];
		}
		t+=tau;
	}

	return 0; 

}


DWORD WINAPI Diffusion(LPVOID lpParam) {
	PMYDATA2 pDataArray2 = (PMYDATA2)lpParam;
	//float tau[10]; //10 is the number of species
	//float n[10];
	int CellID = pDataArray2->i;
	float res;
	float ts2=*ts;
	int** s_speciesAbd=pDataArray2->s_speciesAbd;
	int tot=0;

	srand ( time(NULL) );
	if (pDataArray2->i==0) res=tau[0]*(n[0]+1);
	float u=0;
	for (int i=0; i<nspec; i++) { //10 is the specie
		if (ts2==tau[i]*n[i]) {
			if (pDataArray2->i==0) n[i]++;
			//s_deb[i]=s_n[i];
			//diffuse specie i
			tot=0;
			
			for (int p=0; p<pDataArray2->t_speciesAbd[i]; p++) {
				u=rand() / double(RAND_MAX);
				if (((pDataArray2->i)>=GRID_W*BLOCK_W)&&u<0.125) {
					//diffusion above
					pDataArray2->s_speciesAbd[CellID-GRID_W*BLOCK_W][i]++;
					
					pDataArray2->s_speciesAbd[CellID][i]--;
				}

				else if ((pDataArray2->i<GRID_W*BLOCK_W*(GRID_H*BLOCK_W-1)) && u>=0.125 && u<0.25) {
					//diffusion below
						pDataArray2->s_speciesAbd[CellID+GRID_W*BLOCK_W][i]++;
					
					pDataArray2->s_speciesAbd[CellID][i]--;
				}

				else if ((pDataArray2->i%(GRID_W*BLOCK_W)!=0) && u>=0.25 && u<0.375) {
					//diffusion left
					pDataArray2->s_speciesAbd[CellID-1][i]++;
					
					pDataArray2->s_speciesAbd[CellID][i]--;
				}

				else if ((pDataArray2->i%(GRID_W*BLOCK_W)!=GRID_W*BLOCK_W-1) && u>=0.375 && u<0.5) {
					//diffusion right
					pDataArray2->s_speciesAbd[CellID+1][i]++;
					pDataArray2->s_speciesAbd[CellID][i]--;
				}
				//else don't diffuse!
			}

		}
		if (pDataArray2->i==0) if (tau[i]*n[i]<res) res=tau[i]*n[i];

	}


	if (pDataArray2->i==0) *ts=res;

		return 0; 
}

// Host code
int main(int argc, char** argv)
{
    time_t seconds;
	seconds = time (NULL);

    printf("Initializing simulation...\n");
    ParseArguments(argc, argv);

	// Initialize variables
	nspec=SBML_getNumSpecies("prova.xml");		//get number of species declared in sbml
	reacts=SBML_getNumReactions("prova.xml");	//get number of reactions declared in sbml


    size_t sizeParticles = SPECIES * sizeof(int);
	speciesAbd=(int**)malloc(sizeof(int*)*CELLS);
	h_speciesAbd=(int**)malloc(sizeof(int*)*CELLS);
	h_v=(int**)malloc(sizeof(int*)*REACTIONS);
	v=(int**)malloc(sizeof(int*)*REACTIONS);
	c=(float*)malloc(sizeof(float)*REACTIONS);

	for (int i=0; i<REACTIONS; i++) { 
		v[i]=(int*)malloc(sizeParticles);
		for (int j=0; j<SPECIES; j++) {
			v[i][j]=0;
		}
	}

	for (int i=0; i<REACTIONS; i++) {
		c[i]=1;
	}

	for (int i=0; i<CELLS; i++) {
		speciesAbd[i]=(int*)malloc(sizeParticles);
		if(speciesAbd[i] == NULL) {
			printf("error");
		}
		for (int j=0; j<SPECIES; j++) {
			speciesAbd[i][j]=0;
		}

	}

	float D[SPECIES];
	for (int i=0; i<SPECIES; i++) {
		D[i]=0.00000001;  //it will be never diffuse!
	}
	char** map;	//mapping number to species type
	map=(char**)malloc(sizeof(char*)*SPECIES);
	
	if (parser("prova.xml", speciesAbd, v, c, D, CELLS, reacts, nspec, map, CELLS/2)<0) exit(0); //if the parsing of sbml retrive a value lower than 0, the sbml is not valid
	

	

	float t_sim=0;
	float t_end=tm;
	ts=(float*)malloc(sizeof(float));
	int dim=2; //dimensions
	for (int i=0; i<SPECIES; i++) {
		n[i]=1;
		//D[i]=0.3;
	}
	for (int i=0; i<SPECIES; i++) {
		tau[i]=l*l/(4*dim*D[i]);
	}
	*ts=tau[0];
	for (int i=0; i<SPECIES; i++) {
		if (tau[i]<*ts) *ts=tau[i];
	}


	DWORD dwThrdId;

	
	FILE *pF2;
	pF2 = fopen("resultCPU.out", "w"); 
	displaystep=false;

	fprintf(pF2, "Abundances at time: %f\n", t_sim);
	for (int j=0; j<nspec; j++) {
		fprintf(pF2, "Species %d\n", j);
		int tot_debug=0;
		for (int i=0; i<CELLS; i++) {
			fprintf(pF2, "%d\t", speciesAbd[i][j]);
		}
		fprintf(pF2, "\n");
	}
	fflush(pF2);


	

    printf("Performing simulation...\n");  //maybe I don't need ts on CPU, I could just keep it on GPU....
	printf("ts = %f\n", *ts);
	while (t_sim<t_end) {

		 

        // Generate unique data for each thread to work with.

		int count=0;
		for (int i=0; i<CELLS; i++) {
			//pDataArray[i] = (PMYDATA1) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,sizeof(MYDATA1));
			pDataArray[i] = (PMYDATA1)malloc(sizeof(MYDATA1));
			if( pDataArray[i] == NULL )
			{
				ExitProcess(2);
			}
			//pDataArray[i]->s_speciesAbd=speciesAbd[i];
			pDataArray[i]->i=i;
			pDataArray[i]->tmax=*ts-t_sim;
			count++;
			Thread1[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)DM, pDataArray[i], 0, &dwThrdId);
		}
		if (count>CELLS) printf("count:%d\n", count);
		WaitForMultipleObjects(CELLS, Thread1, TRUE, INFINITE);
		for (int i=0; i<CELLS; i++) {
			//if (!HeapFree(GetProcessHeap(), HEAP_ZERO_MEMORY,pDataArray[i])) exit(0);
			//free(pDataArray[i]);
			
		}
		
		t_sim=*ts;
		printf("diffusion occurs at time: %f\n", *ts);
		for (int i=0; i<CELLS; i++) {
			//pDataArray2[i] = (PMYDATA2) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,	sizeof(MYDATA2));
			pDataArray2[i] = (PMYDATA2)malloc(sizeof(MYDATA2));
			if( pDataArray2[i] == NULL )
			{
				ExitProcess(2);
			}
			pDataArray2[i]->s_speciesAbd=speciesAbd;
			pDataArray2[i]->t_speciesAbd=(int*)malloc(sizeof(int)*SPECIES);
			for (int j=0; j<SPECIES; j++) pDataArray2[i]->t_speciesAbd[j]=speciesAbd[i][j];
			
			pDataArray2[i]->i=i;
		}
		
		for (int i=0; i<CELLS; i++) {
			Thread1[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Diffusion, pDataArray2[i], 0, &dwThrdId);
			
			//Diffusion(speciesAbd, tau, n, ts, deb, i);
			//*ts=cur;
		}
		WaitForMultipleObjects(CELLS, Thread1, TRUE, INFINITE);
		for (int i=0; i<CELLS; i++) {
			//if (!HeapFree(GetProcessHeap(), HEAP_ZERO_MEMORY,pDataArray2[i])) exit(0);
			//free(pDataArray2[i]);
		}
		if (displaystep) { 
			
			fprintf(pF2, "Abundances at time: %f\n", t_sim);
			for (int j=0; j<nspec; j++) {
				fprintf(pF2, "Species %d\n", j);
				for (int i=0; i<CELLS; i++) {
					fprintf(pF2, "%d\t", speciesAbd[i][j]);
				}
				fprintf(pF2, "\n");
			}
			fflush(pF2);
		}
	}



    printf("Simulation ended, analyzing output...\n");

	fprintf(pF2, "Abundances at time: %f\n", t_sim);
	for (int j=0; j<nspec; j++) {
		fprintf(pF2, "Species %d\n", j);
		for (int i=0; i<CELLS; i++) {
			fprintf(pF2, "%d\t", speciesAbd[i][j]);
		}
		fprintf(pF2, "\n");
	}
	fflush(pF2);
	fclose(pF2);
   
	printf("Final abundances\n");
	int tot, tt=0;
	for (int j=0; j<SPECIES; j++) {
		tot=0;
		for (int i=0; i<CELLS; i++) {
			tot+=speciesAbd[i][j];
		}
		tt+=tot;
		printf("Specie %d, Abd: %d\n", j, tot);
	}
    
	printf("Species tot: %d\n", tt);
	printf("Execution time: %d seconds\n", time(NULL)-seconds);
    Cleanup();
}

void Cleanup(void)
{
    // Free device memory
    /*if (d_A)
        cudaFree(d_A);
    if (d_B)
        cudaFree(d_B);
    if (d_C)
        cudaFree(d_C);

    // Free host memory
    if (h_A)
        free(h_A);
    if (h_B)
        free(h_B);
    if (h_C)
        free(h_C);*/
        
   // cutilSafeCall( cudaThreadExit() );
    
//    if (!noprompt) {
        printf("\nPress ENTER to exit...\n");
        fflush( stdout);
        fflush( stderr);
        getchar();
  //  }

    exit(0);
}

float Exponential(float lambda, float u) {
	//return (log(1.0-u)/(-1*(double)lambda));

	return ((-1)/lambda)*logf(u);
}

int Weights(float* a, float a0, int reacts) {
	//int mu;
	float a1,a2;
	a1=0.0f;
	for (int j=0; j<reacts; j++) {
		if (j==0) a1=0.0f;
		else a1+=a[j-1];
		if (a1<=a0) {
			a2=a1+a[j];
			if (a0<a2) {
				return j;
			}
		}
	}
	//no more reactions possible!
	return -1;
}

float h(int* spec, float c, int* v, int nspec) {
	float p=1.0f; //propensity
	for (int i=0;i<nspec; i++) {
		if (v[i]==-1) p=p*spec[i];
		if (v[i]==-2) p=p*spec[i]*(spec[i]-1);
	}
	if (p<=0.0f) p=0.0f;
	else p=p*c;
	return p;
}

// Parse program arguments
void ParseArguments(int argc, char** argv)
{
   /* for (int i = 0; i < argc; ++i)
        if (strcmp(argv[i], "--noprompt") == 0 ||
			strcmp(argv[i], "-noprompt") == 0) 
		{
//            noprompt = true;
            break;
        }*/
}

// initialize OpenGL
/*
void initGL()
{  
    //glutInit();
    //glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
    //glutInitWindowSize(width, height);
    glutCreateWindow("CUDA Particles");

    glewInit();
    if (!glewIsSupported("GL_VERSION_2_0 GL_VERSION_1_5 GL_ARB_multitexture GL_ARB_vertex_buffer_object")) {
        fprintf(stderr, "Required OpenGL extensions missing.");
        exit(-1);
    }

#if defined (_WIN32)
    if (wglewIsSupported("WGL_EXT_swap_control")) {
        // disable vertical sync
        wglSwapIntervalEXT(0);
    }
#endif

    glEnable(GL_DEPTH_TEST);
    glClearColor(0.25, 0.25, 0.25, 1.0);

    glutReportErrors();
}

void cudaGLInit()
{   
    // use command-line specified CUDA device, otherwise use device with highest Gflops/s
   // if( cutCheckCmdLineFlag(argc, (const char**)argv, "device") ) {
        //cutilDeviceInit(argc, argv);
   // } else {
        cudaGLSetGLDevice( cutGetMaxGflopsDeviceId() );
    //}
}

void initParticleSystem(int gridSize, bool bUseOpenGL)
{
    //psystem = new ParticleSystem(gridSize, bUseOpenGL); 
    //psystem->reset(ParticleSystem::CONFIG_GRID);

    if (bUseOpenGL) {
        //renderer = new ParticleRenderer;
        //renderer->setParticleRadius(psystem->getParticleRadius());
        //renderer->setColorBuffer(psystem->getColorBuffer());
    }

	

    //cutilCheckError(cutCreateTimer(&timer));
}*/