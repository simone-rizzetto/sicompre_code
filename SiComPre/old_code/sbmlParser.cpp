
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sbml/SBMLTypes.h>
#include <string.h>
#include <sbml/SBMLReader.h>
#include <sbml/SBMLDocument.h>
#include <sbml/Model.h>
#include <sbml/UnitDefinition.h>
#include <sbml/units/UnitFormulaFormatter.h>
#include <sbml/units/FormulaUnitsData.h>

int getIDbyName(const char** lst, const char* s, int n) {
	for (int i=0; i<n; i++) {
		if (strcmp(s, lst[i])==0) return i;
	}
	return -1;
}

int SBML_getNumSpecies(char* filename) {
	SBMLDocument_t *d;
	Model_t *m;
	d = readSBML(filename);
	SBMLDocument_printErrors(d, stdout);
	m = SBMLDocument_getModel(d);
	if (m == NULL)
	{
		printf("No model present.");
		return -1;
	}
	int res = Model_getNumSpecies(m);
	SBMLDocument_free(d);
	return res;
}

int SBML_getNumReactions(char* filename) {
	SBMLDocument_t *d;
	Model_t *m;
	d = readSBML(filename);
	SBMLDocument_printErrors(d, stdout);
	m = SBMLDocument_getModel(d);
	if (m == NULL)
	{
		printf("No model present.");
		return -1;
	}
	int res = Model_getNumReactions(m);
	SBMLDocument_free(d);
	return res;
}

int parser(char* filename, int** spec, int** v, float* c, float* D, int cells, int rr, int ss, char** map, int source) {
	SBMLDocument_t *d;

	int errors=0;
	//filename = argv[1];
	Model_t *m;

	unsigned int level, version;


	d = readSBML(filename);
	//d = readSBMLFromFile("prova.xml");

	SBMLDocument_printErrors(d, stdout);

	m = SBMLDocument_getModel(d);

	level   = SBMLDocument_getLevel  (d);
	version = SBMLDocument_getVersion(d);

	printf("\n");
	printf("File: %s (Level %u, version %u)\n", filename, level, version);
/*
	if (m == NULL)
	{
	printf("No model present.");
	return 1;
	}


	
	d     = readSBML(filename);

	errors  = SBMLDocument_getNumErrors(d);
	errors += SBMLDocument_checkConsistency(d);

	printf( "\n" );
	printf( "        filename: %s\n" , filename     );
	printf( "        error(s): %u\n" , errors       );

	if (errors > 0) {
		SBMLDocument_printErrors(d, stdout);
		return -1;
	}
	d        = readSBML(filename);

	SBMLDocument_printErrors(d, stdout);

	m = SBMLDocument_getModel(d);


	printf("\n");
	printf("File: %s (Level %u, version %u)\n", filename, level, version);
*/
	if (m == NULL)
	{
	printf("No model present.");
	return 1;
	}

	printf("         ");
	printf("  model id: %s\n",  Model_isSetId(m) ? Model_getId(m) : "(empty)");

	printf( "functionDefinitions: %d\n",  Model_getNumFunctionDefinitions(m) );
	printf( "    unitDefinitions: %d\n",  Model_getNumUnitDefinitions    (m) );
	printf( "   compartmentTypes: %d\n",  Model_getNumCompartmentTypes   (m) );
	printf( "        specieTypes: %d\n",  Model_getNumSpeciesTypes       (m) );
	printf( "       compartments: %d\n",  Model_getNumCompartments       (m) );
	printf( "            species: %d\n",  Model_getNumSpecies            (m) );
	printf( "         parameters: %d\n",  Model_getNumParameters         (m) );
	printf( " initialAssignments: %d\n",  Model_getNumInitialAssignments (m) );
	printf( "              rules: %d\n",  Model_getNumRules              (m) );
	printf( "        constraints: %d\n",  Model_getNumConstraints        (m) );
	printf( "          reactions: %d\n",  Model_getNumReactions          (m) );
	printf( "             events: %d\n",  Model_getNumEvents             (m) );
	printf( "\n" );

	//ListOfSpeciesTypes* species;

	int spectype=Model_getNumSpecies (m);
	Species_t * sp;
//sp=Model_getSpecies (m, 0);

	UnitDefinition_t * ud;
	//int* Ids;
	const char** Names;
	//Ids=(int*)malloc(sizeof(int)*Model_getNumSpecies(m));
	Names=(const char**)malloc(sizeof(const char*)*Model_getNumSpecies(m));
  unsigned int i,j;
  for (i = 0; i < Model_getNumSpecies(m); i++)
  {
	Parameter* par = Model_getParameter(m, i);
	D[i]=Parameter_getValue(par);
    Species* s;
	s=Model_getSpecies(m, i);
	ud=Species_getDerivedUnitDefinition(s);
	
	//printf("Specie %s\n", Species_getDerivedUnitDefinition(s));
	printf("%s: %f\n", Species_getName (s), Species_getInitialAmount(s));
	Names[i]=Species_getId(s);
	//strcpy(Names[i],Species_getId(s));
	//for (int x=0; x<cells; x++) {
	//	spec[x][i]=Species_getInitialAmount(s)/cells;
	//}
	spec[source][i]=Species_getInitialAmount(s);
  }

  for (i = 0; i < Model_getNumReactions(m); i++)
  {
    Reaction *r = Model_getReaction(m,i);
      
	
	KineticLaw_t *kl = Reaction_getKineticLaw(r);
	const ASTNode_t * ast = KineticLaw_getMath (kl);
	float cc=1;
	for (int k=0; k<KineticLaw_getNumParameters (kl); k++) {
		Parameter_t *par=KineticLaw_getParameter (kl, k);
		cc=cc*Parameter_getValue(par);
	}
	c[i]=cc;
	printf("reaction speed:%f", cc);
   /* if (r->isSetKineticLaw())
    {
      cout << "Kinetic Law: " 
        << UnitDefinition::printUnits(r->getKineticLaw()->getDerivedUnitDefinition()) << endl;
      cout << "        undeclared units: ";
      cout << (r->getKineticLaw()->containsUndeclaredUnits() ? "yes\n" : "no\n");
    }
*/
    for (j = 0; j < Reaction_getNumReactants(r); j++)
    {
      SpeciesReference_t *sr = Reaction_getReactant(r,j);

     // if (SpeciesReference_isSetStoichiometryMath(sr))
     // {
		v[i][getIDbyName(Names, SpeciesReference_getSpecies(sr), spectype)]-=SpeciesReference_getStoichiometry(sr);
		  printf("%f%s  ", SpeciesReference_getStoichiometry(sr), SpeciesReference_getSpecies(sr));

     // }
    }
	printf("->  ");
    for (j = 0; j < Reaction_getNumProducts(r); j++)
    {
      SpeciesReference_t *sr = Reaction_getProduct(r,j);

      //if (sr->isSetStoichiometryMath())
      //{
		v[i][getIDbyName(Names, SpeciesReference_getSpecies(sr), spectype)]+=SpeciesReference_getStoichiometry(sr);
		  printf("%f%s  ", SpeciesReference_getStoichiometry(sr), SpeciesReference_getSpecies(sr));
     // }
    }
	printf("\n");
  }
	
	printf("\n");
	for (i=0; i<spectype; i++) {
		map[i]=(char*)malloc(sizeof(char)*strlen(Names[i]));
		strcpy(map[i],Names[i]);
	}

	SBMLDocument_free(d);
	return 0;

}