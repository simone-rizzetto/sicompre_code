/*
 * Simone Rizzetto - Biological Systems Simulation Complex Version
 * Implementation of Gillespie Multi-Particle/Complex on GPGPU using CUDA 
 * 2010-2011

 What's new? This version allow the definition of specie type. It simulate only complexation and decomplexation of
 molecule. It use the speciesType as a whole molecule and a simple specie as binding site. To perform a classic simulation 
 use the previous version of GPU Gillespie Multi-Particle.

 Assumption:
 1) Unbinding rate between 0 and 1

 TODO list:
 1) Check for events, ex: never can happen that molecule[i].cell=x && site[j].mol==i && site[j].cell!=x

 Current Problems:

 Possible Improvements:
 1) State array divided in molecule instead of species, but be carefull to site id!
 2) Autocomplexation is required? 
 3) Cublas or thrust?
 4) Tesla or GPUcapability>2.0

 Future Works:
 1) Add compartments (Better Many different simulation)
 2) Incorporate Complex modality with normal version 
 3) 2D space to 3D space (I'm not sure that this will change something, except increase complexity...)
 
 Test:
 1) Large protein complex experiment 20 molecules, 60 sites, 6 different domain type, 2 different protein type, 3 reactions
    Results:
	Parameters: time=1.0, lattice size=0.4 - Memory used: 1640 B - Simulation total time: 5 seconds
 2) Large protein complex experiment #2 200 molecules, 600 sites, 6 different domain type, 2 different protein type, 3 reactions
    Results:
	Parameters: time=1.0, lattice size=0.4 - Memory used: 16400 B - Simulation total time: 5 seconds
 3) Largest experiment possible (using real data - whole golgi compartment)
	Parameters: time=1.0, lattice size=0.4 - Memory used: - Simulation total time: 

 */

// Includes
#include <stdio.h>
#include <stdlib.h>
#include <cutil_inline.h>
#include <math.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cuda_runtime_api.h>
#include <time.h>
// Graphics includes
#include <GL/glew.h>
#if defined (_WIN32)
#include <GL/wglew.h>
#endif

#if defined(__APPLE__) || defined(MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <shrUtils.h>
#include <cutil_gl_inline.h>
#include <cuda_gl_interop.h>
#include <cuPrintf.cu>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <ctype.h>
#include "sbmlParserC.h"


//The macro CUPRINTF is defined for architectures
//with different compute capabilities.
#if __CUDA_ARCH__ < 200 	//Compute capability 1.x architectures
#define CUPRINTF cuPrintf
#else						//Compute capability 2.x architectures
#define CUPRINTF(fmt, ...) printf("[%d, %d]:\t" fmt, \
								blockIdx.y*gridDim.x+blockIdx.x,\
								threadIdx.z*blockDim.x*blockDim.y+threadIdx.y*blockDim.x+threadIdx.x,\
								__VA_ARGS__)
#endif


#define SPECIES 642		//number of domains
#define PROT_SPECIES 9	//tipi di molecole
#define NUMSITE 17
#define MOLECULES 441
#define REACTIONS 12	//the reactions number include both complex and decomplex reaction declared in the input file
#define CELLS 1536	//# of subvolumes
#define BLOCKS 24	//a block is executed by a processor, my GPU has 12 processor, so is better choose a multiple of 12
#define THREADS 64	//threads executed in each block, for max occupancy choose a multiple of 32, 32 is the warp size
#define GRID_W 6	//grid width = 6*8 subvolumes
#define GRID_H 4	//grid height = 4*8 subvolumes
#define BLOCK_W 8
#define BLOCK_H 8

//problem how to rapresent state vector of species?!?!?

/* possible solution using CLASSES
Class Prot {
	prot()
	getDom()
	getCompl()
	getSpecie()

	dom* dom_lst
	ushort int compl
	ushort int specie
}

*/

/*
struct molecule {
	unsigned short complex;
	unsigned short specie;
};

struct site {
	unsigned short specie;
	site* bound;  //it is not efficient, I've to store 2 times the same information!!! but it is simpler
	molecule* mol;
};

struct site_list {
	site s;
	site_list* next;
};

struct molecule_list {
	molecule m;
	molecule_list* next;
};



molecule_list* prot_lst[CELLS];
molecule_list* d_prot_lst[CELLS];
molecule_list* h_prot_lst[CELLS];
site_list* dom_lst[CELLS];

site* prova_dom_lst[CELLS];

*/

//SOLUTION 3: structure with state vector equal for each cell
//in DM at the beginning the state vector is scrolled and for each prot in the cell i save the id
//when i compute the propensity ecc i scroll over the only ids found at the prev step (ex. if cellID==site->mol->cell then ...)
//diffusion is very easy




site* site_state;  //potrei usare array di array in modo da dividerli per specie!
molecule* molecule_state;
site* d_site_state;

//in una fase di init ho un __device__ l_site_state[SITE] e lo riempio con site_state, poi faccio la lista e abdcs
__device__ l_site l_site_state[SPECIES];
__device__ l_site* CellList[CELLS];
molecule* d_molecule_state;
__device__ int abdCS[CELLS][NUMSITE+1];

// Variables


int species; //number of species
float tm=0.001; //max time of the simulation
float l=0.4; //lattice size
int cells; //number of subvolume in the grid
float* c; //vector of rates;
float* cu; //vector of unbounding rates;
float* d_c; //device
float* d_cu; //device
float* d_tau; //device (diffusion)
int* d_n; //devicie (diffusion)
int* d_abdS; //
float* deb; //array used for debug
float* d_deb; //device debug
float* d_degree;
float* d_ts; //device time
int* d_max_complex;
int* v; //particle used in reaction, example: v[i][j]==-1 the reaction i consumed 1 particle j;
int* d_v; //device
//site_list** speciesAbd; //corresponds to internal state of a LP, on CPU
//site_list** d_speciesAbd; //corresponds to internal state of a LP, on Device
//site_list** h_speciesAbd; //corresponds to internal state of a LP, on Host
double** rates; //reaction rates
bool displaystep=false; //print intermediate steps in a file
char* inputfile; //sbml file
__device__ int complex_count=0;
__device__ int mutex;
__device__ float a[CELLS][REACTIONS+MOLECULES]; //10 is the reaction
//__device__ unsigned short int memo[MOLECULES][NUMSITE]; //used for memoization it is too large
__device__ unsigned int memo[CELLS][MOLECULES][2]; //used for memoization, it is too large


// Functions
void Cleanup(void);
__device__ float Exponential(float, float);
__device__ float h(site*, molecule*, float, const int*, int, int , int, const int*);
__device__ float hu(site*, molecule*, const float*, const int*, int, int , int, int, const int*);
__device__ int CountSiteofSpec(site*, int, int, int, int, const int*);
__device__ int CountSiteofSpec_u(site*, int, int, int, int, const int*);
__device__ int Weights(float*, float);
__device__ void updateComplex(molecule*, int, int , int, int );
__device__ void chooseSite(int, int, int*, int*, float, int, site*, int, const int*);
__device__ void updateMol(molecule*, site*, int, int, int, int, int, int);
__device__ void autocomplexation(site*, molecule*, int, const int*, int, int, int, curandState*);
__device__ void updateC(molecule*, int);
__device__ int t_abdCS[CELLS][NUMSITE];
void ParseArguments(int, char**);

// Device code



__global__ void CloseSim(int* s_max_complex, site* s_site_state, molecule* s_molecule_state, int tot_molecule, int tot_site) {
	int count=0;
	for (int j=0; j<tot_site; j++) {
		//if (s_site_state[j].bound->bound!=&s_site_state[j]) CUPRINTF("Error during simulation - %d\n", j);
		if (s_site_state[j].bound!=NULL) count++;
	}
	//if (count%2!=0) CUPRINTF("Error during simulation!\n");
	*s_max_complex=complex_count;

	//CUPRINTF("complexes %d\n", *s_max_complex);
}

__global__ void InitSim(site* s_site_state, molecule* s_molecule_state, int tot_molecule, int tot_site) {
	l_site* old;
	bool first;
	for (int i=0; i<CELLS; i++) {
		for (int j=0; j<NUMSITE; j++) {
			abdCS[i][j]=0;
		}
	}	
	for (int i=0; i<CELLS; i++) {
		first=true;
		for (int j=0; j<tot_site; j++) {
		//printf("Site id %d specie %s molecule %d\n", j, mapS[site_state[j].specie], site_state[j].mol);
			if (s_site_state[j].cell==i) {
				l_site_state[j].s=&s_site_state[j];
				//CUPRINTF("%d\n", l_site_state[j].s->specie);
				t_abdCS[i][s_site_state[j].specie]++;
				if (first) {
					CellList[i]=&l_site_state[j];
					first=false;
					old=&l_site_state[j];
				}
				else {
					old->next=&l_site_state[j];
					old=old->next;
				}
			}
		}
	}
/*
	for (int i=0; i<CELLS; i++) {

		if (CellList[i]!=NULL) {
			CUPRINTF("%d\n",i);
			l_site* old;
			old=CellList[i];
			while (old->next!=NULL) {
				CUPRINTF("old->s.specie = %d\n", old->s->specie);
				old=old->next;
			}
			CUPRINTF("old->s.specie = %d\n", old->s->specie);
			//if (old->s.bound==NULL && old->s.mol==mol && old->s.specie==i) count++;
		}
	}
*/
	int countS;
	int x;
	for (int i=0; i<CELLS; i++) {
		x=0;
		countS=0;
		for (x=0; x<NUMSITE; x++) {
			abdCS[i][x]=countS;
			countS+=t_abdCS[i][x];
		}
		abdCS[i][x]=countS;
	}
}

__global__ void ComplexDegree(float* s_degree, site* s_site_state, molecule* s_molecule_state, int tot_molecule, int tot_site) {
	//int s_degree[complex_count];
	int tt;
	for (int i=0; i<complex_count; i++) {
		s_degree[i]=0;
		tt=0;
		for (int j=0; j<tot_molecule; j++) {
			if (s_molecule_state[j].complex==i) {
				tt++;
				for (int k=0; k<tot_site; k++) {
					if (s_site_state[k].mol==j && s_site_state[k].bound!=NULL) s_degree[i]++;
				}
			}
		}
		if (tt>1) s_degree[i]=(s_degree[i]/(tt*(tt-1)));
	}
}
//Direct Method
//__global__ void DM(curandState *state, site* s_domLst, molecule* d_prot_lst,int** s_v, const float* s_c, float tmax, float* s_deb, const int reacts) {

__global__ void DM(curandState *state, site* s_site_state, molecule* s_molecule_state, const int* v, const float* s_c, const float* s_cu, float t_max, int tot_molecule, int tot_site, int reacts, const int* s_abdS) {
	//int species=10;
	//int reactions=10;
	float a0=0;
	
	//__shared__ int v[REACTIONS][SPECIES];
	//CUPRINTF("Value is:\n");
	//float c[10];
	float tau;
	//__shared__ site abd[THREADS][SPECIES];
	int mu=-1;
	float u,u2,u3;
	int CellID=blockDim.x * blockIdx.x + threadIdx.x;
	
	int debug=0;
	int count_prop=0;
	//s_deb[4]++;
	//s_speciesAbd[0][0]++;
	curand_init(clock(), 1, 0, &state[CellID]); //seed, sequence, offset, state
	curandState localState = state[CellID];

	//now abd is a shared variable and is 100 times faster than s_speciesAbd
	//if (threadIdx.x==0) {
	//for (int i=0; i<SPECIES; i++) {
	//	abd[threadIdx.x][i]=s_speciesAbd[CellID][i]; 
	//}
	//}
	/*if (threadIdx.x==0) {
		for (int j=0; j<REACTIONS; j++) {
			for (int i=0; i<SPECIES; i++) {
				v[j][i]=s_v[j][i];
			}
		}
	}
	__syncthreads();*/
	float t=0;
	//s_deb[0]=t_max;
	//float t_max=1;
	//CUPRINTF("Value is:%d\n", aaa);
	int s1,s2;
	int m_inv1, m_inv2;
	bool needlock;
	int tmp_c;
	bool binding=true;
	int x=1;
	if (CellID<MOLECULES) x=MOLECULES/CellID;
/*
	for (int j=CellID*x; j<MOLECULES && j<(CellID+1)*x; j++) {
		for (int k=0; k<NUMSITE; k++) {
			memo[j][k]=CountSiteofSpec(s_site_state, k, CellID, tot_site, j);
			//if (memo[j][k]>0) 
			//CUPRINTF("ok\n");
		}
	}
*/
	int totu=0;
	while (t<t_max) {
		//if (clo1>0) CUPRINTF("time cycle %d\n", clo3-clo1);
		a0=0;
		//binding action propensity
		for (int j=0; j<REACTIONS*2; j+=2) {
			if (mu<REACTIONS && (mu==-1 || mu==(j/2))) {
			a[CellID][j/2]=h(s_site_state, s_molecule_state, s_c[j/2], v, j/2, CellID, tot_site, s_abdS);
			count_prop++;
			}
			a0+=a[CellID][j/2];
			//if (a0>0) CUPRINTF("%d - %f\n",j,a[CellID][j/2]); 
		}
		//if (a0>0) CUPRINTF("time propensity %d\n", clo2-clo1);
		//unbinding action propensity
		float temp=a0;
		for (int j=0; j<MOLECULES; j++) {
			if (s_molecule_state[j].cell==CellID) {
				if (mu==-1 || (mu<REACTIONS && (j==m_inv1 || j==m_inv2))) {
					a[CellID][j+REACTIONS]=hu(s_site_state, s_molecule_state, s_cu, v, j, CellID, tot_site, tot_molecule, s_abdS);
					//if (a[CellID][j+REACTIONS]>0) CUPRINTF("ma entro?!%f\n", a[CellID][j+REACTIONS]);
					count_prop++;
				}
			}
			else
				a[CellID][j+REACTIONS]=0;
			a0+=a[CellID][j+REACTIONS];
		}
		u=curand_uniform_double(&localState);
		u2=curand_uniform_double(&localState);
		u3=curand_uniform_double(&localState);
		while (u==0.0f) u=curand_uniform_double(&localState); 
		tau=Exponential(a0,u);
		if (a0==0) break;
		mu=Weights(a[CellID],a0*u2);
		//CUPRINTF("mu %d\n", mu);
		//if (a[CellID][1]) CUPRINTF("a1 %d\n", a[CellID][1]);
		if (mu>=REACTIONS) {
			//collision
			//CUPRINTF("decomplex\n");
			mu-=REACTIONS;
			totu=0;
			for (int i=0; i<tot_site; i++) {
				if (s_site_state[i].mol==mu) {
					s_site_state[i].bound=NULL;
					totu++;
				}
			}
			/*if (totu>1) {
				updateC(s_molecule_state, s_molecule_state[mu].complex)
			}*/
			s_molecule_state[mu].complex=0;
		}
		else {
			//complexation action
			s1=0;
			s2=0;
			chooseSite(v[mu*2], v[mu*2+1], &s1, &s2, u3, tot_site, s_site_state, CellID, s_abdS);
			s_site_state[s1].bound=&s_site_state[s2];
			s_site_state[s2].bound=&s_site_state[s1];
			needlock=true;
			while (needlock) {
			  if (0==atomicCAS(&mutex, 0, 1)) {
				 // lock
					atomicAdd(&complex_count,1);
					tmp_c=complex_count;
				 // release lock 
				 atomicExch(&mutex, 0);
				 needlock=false;
			   }
			}
			m_inv1=s_site_state[s1].mol;
			m_inv2=s_site_state[s2].mol;
			for (int m=0, int f=0; m<tot_molecule && f<2; m++) {
				if (s_molecule_state[m].id==s_site_state[s1].mol || s_molecule_state[m].id==s_site_state[s2].mol) {
					f++;
					if (s_molecule_state[m].complex!=0) updateComplex(s_molecule_state, s_molecule_state[m].complex, CellID, tot_molecule, tmp_c);
					else s_molecule_state[m].complex=tmp_c;
				}
			}
			//autocomplexation
			//autocomplexation(s_site_state, s_molecule_state, tmp_c, v, tot_molecule, tot_site, reacts, &localState);
		}
		t+=tau;
	}
	//if (debug>1 && t_max==0.4) CUPRINTF("debug %d\n", debug);
}


__global__ void Diffusion(curandState *state, site* s_site_state, molecule* s_molecule_state, float* s_tau, int* s_n, float* s_ts, int tot_molecule, int tot_site) {
//__global__ void Diffusion() {
	//float tau[10]; //10 is the number of species
	//float n[10];
	//CUPRINTF("prova\n");
	float ts=*s_ts;
	
	float res;
	int CellID = blockDim.x * blockIdx.x + threadIdx.x;
	
	curand_init(clock(), 1, 0, &state[CellID]); //seed, sequence, offset, state
	curandState localState = state[CellID];
	

	if (CellID==0) res=s_tau[0]*(s_n[0]+1);
	//CUPRINTF("%d\n", CellID);
	/*__shared__ int diff[BLOCK_W*BLOCK_H][10];
	for (int i=0; i<SPECIES; i++) { //10 is the specie
		if (ts==s_tau[i]*s_n[i]) {
			diff[threadIdx.x][i]=s_speciesAbd[CellID][i];
		}
	}
	//__shared__ int abd[THREADS][SPECIES];
	//s_deb[3]++;
	for (int i=0; i<SPECIES; i++) {
		abd[threadIdx.x][i]=s_speciesAbd[CellID][i]; 
	}*/
	//__syncthreads();
	//int tot=0;
	//
	float u=0;
	
	for (int i=0; i<PROT_SPECIES; i++) { 
		if (ts>=(floor(s_tau[i]*s_n[i]*10000)/10000)) {
			if (CellID==0) {
				//if (ts==0.4) CUPRINTF("i = %d\n", s_n[i]);
				atomicAdd(&s_n[i],1);
				//if (ts==0.4) CUPRINTF("i2 = %d\n", s_n[i]);
			}
			
			for (int p=0; p<tot_molecule; p++) {
				if (s_molecule_state[p].specie==i && s_molecule_state[p].cell==CellID) {
					u=curand_uniform_double(&localState);

					if ((blockIdx.x>=GRID_W || threadIdx.x>=BLOCK_W)&&u<0.125) {
						//diffusion above
						if (threadIdx.x>=BLOCK_W) {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID-BLOCK_W;
							updateMol(s_molecule_state, s_site_state, CellID-BLOCK_W, s_molecule_state[p].id, s_molecule_state[p].complex, tot_site, tot_molecule, CellID);
						}
						else {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID-(GRID_W-1)*THREADS-BLOCK_W;
							updateMol(s_molecule_state, s_site_state, CellID-(GRID_W-1)*THREADS-BLOCK_W, s_molecule_state[p].id, s_molecule_state[p].complex, tot_site, tot_molecule, CellID);
						}
					}

					else if ((blockIdx.x<GRID_W*(GRID_H-1) || threadIdx.x<BLOCK_W*(BLOCK_H-1)) && u>=0.125 && u<0.25) {
						//diffusion below
						if (threadIdx.x<BLOCK_W*(BLOCK_H-1)) {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID+BLOCK_W;
							updateMol(s_molecule_state, s_site_state, CellID+BLOCK_W, s_molecule_state[p].id, s_molecule_state[p].complex, tot_site, tot_molecule, CellID);
						}
						else {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID+(GRID_W-1)*THREADS+BLOCK_W;
							updateMol(s_molecule_state, s_site_state, CellID+(GRID_W-1)*THREADS+BLOCK_W, s_molecule_state[p].id, s_molecule_state[p].complex, tot_site, tot_molecule, CellID);
						}
					}

					else if ((blockIdx.x%GRID_W!=0 || threadIdx.x%BLOCK_W!=0) && u>=0.25 && u<0.375) {
						//diffusion left
						if (threadIdx.x%BLOCK_W!=0) {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID-1;
							updateMol(s_molecule_state, s_site_state, CellID-1, s_molecule_state[p].id, s_molecule_state[p].complex, tot_site, tot_molecule, CellID);
						}
						else {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID-(BLOCK_H-1)*BLOCK_W-1;
							updateMol(s_molecule_state, s_site_state, CellID-(BLOCK_H-1)*BLOCK_W-1, s_molecule_state[p].id, s_molecule_state[p].complex, tot_site, tot_molecule, CellID);
						}
					}

					else if ((blockIdx.x%GRID_W!=GRID_W-1 || threadIdx.x%BLOCK_W!=BLOCK_W-1) && u>=0.375 && u<0.5) {
						//diffusion right
						if (threadIdx.x%BLOCK_W!=BLOCK_W-1) {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID+1;
							updateMol(s_molecule_state, s_site_state, CellID+1, s_molecule_state[p].id, s_molecule_state[p].complex, tot_site, tot_molecule, CellID);
						}
						else {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID+(BLOCK_H-1)*BLOCK_W+1;
							updateMol(s_molecule_state, s_site_state, CellID+(BLOCK_H-1)*BLOCK_W+1, s_molecule_state[p].id, s_molecule_state[p].complex, tot_site, tot_molecule, CellID);
						}
					}
					//s_molecule_state[p].readyToDiffuse=true;
					//else don't diffuse!
				}
			}


		}
		
		if (CellID==0) {
			if (s_tau[i]*s_n[i]<res) res=s_tau[i]*s_n[i];
			if (ts==0.4) CUPRINTF("res %f\n",res);
		}

	}
	if (CellID==0) {
		*s_ts=res;
		
			//if (ts==0.4) CUPRINTF("res final %f\n",res);
	}
}



// Host code
int main(int argc, char** argv)
{
	time_t seconds;
	time_t last;
	
	int num_devices, device;
	cudaGetDeviceCount(&num_devices);
    int max_multiprocessors = 0, max_device = 0;
    for (device = 0; device < num_devices; device++) {
		  cudaDeviceProp properties;
		  cudaGetDeviceProperties(&properties, device);
		  printf("Device Name:\t%s\n", properties.name );
		  printf("Global memory:\t%d\n", properties.totalGlobalMem );
		  printf("Constant memory:\t%d\n", properties.totalConstMem );
		  printf("Warp size:\t%d\n", properties.warpSize );
    }
	//scanf("%d", &max_device);
    cudaSetDevice(0);
	

    printf("Initializing simulation...\n");
    ParseArguments(argc, argv);
	inputfile="peroxisome_biog.xml";
	int reacts=0;
	int nsite=0;
	int nmol=0;
	int tot_site=SPECIES;
	int tot_molecule=4;
	nsite=SBML_getNumSpecies(inputfile);
	tot_molecule=SBML_getNumSpeciesTypesTot(inputfile);
	nmol=SBML_getNumSpeciesTypes(inputfile);
	reacts=SBML_getNumReactions(inputfile);

	curandState *devStates;

	// Initialize variables

    size_t sizeParticles = SPECIES * sizeof(site);
	//eq=(eventQueue*)malloc(sizeQueue);
	//totParticles=(int*)malloc(sizeParticles);

	site_state=(site*)malloc(sizeof(site)*tot_site);
//	l_site_state=(l_site_state**)malloc(sizeof(l_site*)*CELLS);
	molecule_state=(molecule*)malloc(sizeof(molecule)*tot_molecule);
	
	printf("Site state size: %d B\nMolecule state size %d B\n", sizeof(site)*tot_site, sizeof(molecule)*tot_molecule);
	printf("#Sites: %d\t#Molecules: %d\n", tot_site, tot_molecule);
	//speciesAbd=(site_list**)malloc(sizeof(site_list*)*CELLS);
	//h_speciesAbd=(site_list**)malloc(sizeof(site_list*)*CELLS);
	v=(int*)malloc(sizeof(int)*REACTIONS*2);
	c=(float*)malloc(sizeof(float)*REACTIONS);
	cu=(float*)malloc(sizeof(float)*REACTIONS);
	deb=(float*)malloc(sizeof(float)*SPECIES);



	for (int j=0; j<reacts*2; j++) {
		v[j]=0;
		j++;
		v[j]=0;
	}
	
	v[0]=1; v[1]=2; v[2]=3; v[3]=4;   //la prima reazione � tra le specie 1 e 2, la seconda tra la 3 e la 4

	for (int i=0; i<reacts; i++) {
		c[i]=1;
	}


	float* D = (float*)malloc(sizeof(float)*nmol);
	for (int i=0; i<nmol; i++) {
		D[i]=0.00001;
	}
	//D[0]=2;
	char** map;
	char** mapS;
	map=(char**)malloc(sizeof(char*)*nmol);
	mapS=(char**)malloc(sizeof(char*)*tot_site); //da cambiare in nsite
	
	if (parser(inputfile, site_state, molecule_state, v, c, cu, D, CELLS, reacts, tot_site, map, mapS, CELLS/2-THREADS*GRID_W/2)<0) exit(0);
	
//	for (int i=0; i<tot_molecule; i++) {
//		printf("Molecule id %d specie %s\n", molecule_state[i].id, map[molecule_state[i].specie]);
//	}
	/*l_site_state* old;
	int abdCS[CELLS][NUMSITE];
	for (int i=0; i<CELLS; i++) {
		for (int j=0; j<NUMSITE; j++) {
			abdCS[i][j]=0;
		}
	}	
	for (int i=0; i<CELLS; i++) {
		first=true;
		for (int j=0; j<tot_site; j++) {
		//printf("Site id %d specie %s molecule %d\n", j, mapS[site_state[j].specie], site_state[j].mol);
			if (site_state[j].cell==i) {
				l_site_state* newl=(l_site_state)malloc(sizeof(l_site_state))
				newl->s=site_state[j];
				abdCS[i][site_state[j].specie]++;
				if (first) {
					l_site_state[i]=newl;
					first=false;
				}
				else {
					old.next=newl;
				}
				old=newl;
			}
		}
	}
*/
/*
	for (int j=0; j<reacts; j++) {
		//printf("Reaction %d species %s - %s, binding %f, unbounding %f\n", j, mapS[v[j*2]], mapS[v[j*2+1]], c[j], cu[j]);
		printf("Reaction %d species %d - %d, binding %f, unbounding %f\n", j, v[j*2], v[j*2+1], c[j], cu[j]);
	}

*/
	float t_sim=0;
	float t_end=tm;
	float* ts; //time of the next diffusion event
	ts=(float*)malloc(sizeof(float));
	int* n=(int*)malloc(sizeof(int)*nmol);
	float* tau=(float*)malloc(sizeof(float)*nmol);
	int dim=2; //dimension
	for (int i=0; i<nmol; i++) {
		n[i]=1;
		//D[i]=0.3;
	}
	for (int i=0; i<nmol; i++) {
		tau[i]=l*l/(2*dim*D[i]);
	}
	*ts=tau[0];
	for (int i=0; i<nmol; i++) {
		if (tau[i]<*ts) *ts=tau[i];
	}

	//count species
	int* t_abdS=(int*)malloc(sizeof(int)*(nsite));
	int* abdS=(int*)malloc(sizeof(int)*(nsite+1));
	for (int i=0; i<nsite; i++) {
		t_abdS[i]=0;
		abdS[i]=0;
	}
	abdS[nsite]=0;
	for (int i=0; i<tot_site; i++) {
		t_abdS[site_state[i].specie]++;
	}
	int countS=0;
	int x=0;
	for (x=0; x<nsite; x++) {
		abdS[x]=countS;
		countS+=t_abdS[x];
	}
	abdS[x]=countS;
	//for (int i=0; i<nsite+1; i++) {
	//	printf("%d: %d\n", i, abdS[i]);
	//}

	cutilSafeCall(cudaMalloc((void **)&devStates, CELLS * sizeof(curandState)));
	cutilSafeCall( cudaMalloc((void**)&d_site_state, sizeof(site)*tot_site) );
	cutilSafeCall( cudaMalloc((void**)&d_molecule_state, sizeof(molecule)*tot_molecule) );
    cutilSafeCall( cudaMalloc((void**)&d_v, sizeof(int)*reacts*2) );
    cutilSafeCall( cudaMalloc((void**)&d_c, sizeof(float)*reacts) );
    cutilSafeCall( cudaMalloc((void**)&d_cu, sizeof(float)*reacts) );
    cutilSafeCall( cudaMalloc((void**)&d_n, sizeof(int)*nmol) );
    cutilSafeCall( cudaMalloc((void**)&d_tau, sizeof(float)*nmol) );
    cutilSafeCall( cudaMalloc((void**)&d_abdS, sizeof(int)*(nsite+1)) );
    cutilSafeCall( cudaMalloc((void**)&d_ts, sizeof(float)) );
    cutilSafeCall( cudaMalloc((void**)&d_max_complex, sizeof(float)) );

	
	cutilSafeCall( cudaMemcpy(d_site_state, site_state, sizeof(site)*tot_site, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_molecule_state, molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyHostToDevice) );

	cutilSafeCall( cudaMemcpy(d_v, v, sizeof(int)*reacts*2, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_c, c, sizeof(float)*reacts, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_cu, cu, sizeof(float)*reacts, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_tau, tau, sizeof(float)*nmol, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_abdS, abdS, sizeof(int)*(nsite+1), cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_n, n, sizeof(int)*nmol, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_ts, ts, sizeof(float), cudaMemcpyHostToDevice) );
	

	
 
	cudaPrintfInit();

	FILE *pF2;
	pF2 = fopen("resultCOMPLEX.out", "w"); 
/*	displaystep=true;
	//if (displaystep) {

	//FILE *pF3;
	//pF3 = fopen("plot.r", "w"); //opening in read mode
	//fprintf(pF3, "setwd(\"C:\Users\Simone\")\n");
	//fclose(pF3);
	fprintf(pF2, "Abundances at time: %f\n", t_sim);
	for (int j=0; j<SPECIES; j++) {
		fprintf(pF2, "Species %d\n", j);
		int tot_debug=0;
		for (int i=0; i<CELLS; i++) {
			tot_debug+=speciesAbd[i][j];
			//printf("%d: %d\t", i, speciesAbd[i][j]);
			//fprintf(pF2, "%d:%d\t", j, speciesAbd[i][j]);
			fprintf(pF2, "%d\t", speciesAbd[i][j]);
		}
		if (j==0) printf("tot_debug:%d\n", tot_debug);
		fprintf(pF2, "\n");
	}
	fflush(pF2);
	//}

*/


	for (int i=0; i<tot_molecule; i++) {
		for (int j=0; j<tot_site; j++) {
			if (site_state[j].cell!=molecule_state[i].cell && site_state[j].mol==i) printf("error cell property violated on %s - %s!\t%d - %d\n", map[molecule_state[i].specie], mapS[site_state[j].specie], molecule_state[i].cell, site_state[j].cell);
		}
	}

	printf("Attenzione, la simulazione potrebbe richiedere diverse ore, nelle quali il pc rimarr� inutilizzabile.\nPremere INVIO per continuare\n");
	InitSim<<<1,1>>>(d_site_state, d_molecule_state, tot_molecule, tot_site);
    fflush( stdout);
	getchar();
	seconds = time (NULL);
    printf("Performing simulation...\n");
	//size_t shr_mem = sizeof(int)*nspec*reacts;
	while (t_sim<t_end) {
		last = time (NULL);
		//DM<<<24, 64, shr_mem>>>(devStates, d_speciesAbd, d_v, d_c, *ts-t_sim, d_deb, reacts);
		DM<<<24, 64>>>(devStates, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, tot_molecule, tot_site, reacts, d_abdS);
		cutilSafeCall(cudaThreadSynchronize());
		
		printf("DM time: %d seconds\n", time(NULL)-last);
		last = time (NULL);
		t_sim=*ts;
		*ts+=0.15;
		printf("diffusion occurs at time: %f\n", *ts);
		//Diffusion<<<24, 64>>>(devStates, d_site_state, d_molecule_state, d_tau, d_n, d_ts, tot_molecule, tot_site);
		//Diffusion<<<24, 64>>>();
		cutilSafeCall(cudaThreadSynchronize());
		
		printf("Diff time: %d seconds\n", time(NULL)-last);
		//cutilSafeCall( cudaMemcpy(ts, d_ts, sizeof(float), cudaMemcpyDeviceToHost) );
	}


cudaPrintfDisplay(stdout, true);
	cudaPrintfEnd();

    cutilCheckMsg("kernel error");
#ifdef _DEBUG
    cutilSafeCall( cudaThreadSynchronize() );
#endif


	int* max_complex;
	max_complex=(int*)malloc(sizeof(int));
	CloseSim<<<1,1>>>(d_max_complex, d_site_state, d_molecule_state, tot_molecule, tot_site);
	
	cutilSafeCall( cudaMemcpy(site_state, d_site_state, sizeof(site)*tot_site, cudaMemcpyDeviceToHost) );
	cutilSafeCall( cudaMemcpy(molecule_state, d_molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyDeviceToHost) );

    printf("Simulation ended, analyzing output...\n");

	cutilSafeCall( cudaMemcpy(max_complex, d_max_complex, sizeof(int), cudaMemcpyDeviceToHost) );
	//*max_complex=1;
	printf("#complex %d\n", *max_complex);
	printf("Final abundances\n");


	float* degree=(float*)malloc(sizeof(float)*(*max_complex));
	for (int i=0; i<(*max_complex); i++) {
		degree[i]=0;
	}

    cutilSafeCall( cudaMalloc((void**)&d_degree, sizeof(float)*(*max_complex)) );
	cutilSafeCall( cudaMemcpy(d_degree, degree, sizeof(float)*(*max_complex), cudaMemcpyHostToDevice) );
	ComplexDegree<<<1,1>>>(d_degree, d_site_state, d_molecule_state, tot_molecule, tot_site);
	cutilSafeCall( cudaMemcpy(degree, d_degree, sizeof(float)*(*max_complex), cudaMemcpyDeviceToHost) );


    for (int j=0; j<tot_molecule; j++) {
		//printf("molecule %d: %s %d\n", molecule_state[j].id, map[molecule_state[j].specie], molecule_state[j].complex);
	}
	for (int i=0; i<*max_complex; i++) {
		fprintf(pF2, "#%d\n", i);
		for (int j=0; j<tot_molecule; j++) {
			if (molecule_state[j].complex==i) {
				fprintf(pF2, "%s\n", map[molecule_state[j].specie]);
				
			}
		}
		fprintf(pF2, "%f\n", degree[i]);
	}


	for (int i=0; i<tot_molecule; i++) {
		for (int j=0; j<tot_site; j++) {
			if (site_state[j].cell!=molecule_state[i].cell && site_state[j].mol==i) printf("error cell property violated on %s - %s!\t%d - %d\n", map[molecule_state[i].specie], mapS[site_state[j].specie], molecule_state[i].cell, site_state[j].cell);
		}
	}

	printf("\n");

	for (int j=0; j<tot_site; j++) {
		//printf("site %d: %s %d %d %d\n", j, mapS[site_state[j].specie], site_state[j].mol, (site_state[j].bound!=NULL?1:0), site_state[j].cell);
	}
	
	printf("Execution time: %d seconds\n", time(NULL)-seconds);
	fprintf(pF2, "Execution time: %d seconds\n", time(NULL)-seconds);
	fflush(pF2);
	fclose(pF2);
    Cleanup();
}

void Cleanup(void)
{
    // Free device memory
   /* if (d_speciesAbd)
        cudaFree(d_speciesAbd);

    if (d_c)
        cudaFree(d_c);

    if (d_v)
        cudaFree(d_v);
    // Free host memory
	
	for (int i=0; i<CELLS; i++) {
		if (h_speciesAbd[i])
			free(h_speciesAbd[i]);
	}
	
	for (int i=0; i<REACTIONS; i++) {
		if (h_v[i]) free(h_v[i]);
	}*/

    cutilSafeCall( cudaThreadExit() );
    

    fflush( stdout);
    printf("\nPress ENTER to exit...\n");
    fflush( stdout);
    fflush( stderr);
    getchar();

    exit(0);
}

__device__ float Exponential(float lambda, float u) {
	return ((-1)/lambda)*logf(u);
}

__device__ int Weights(float* a, float a0) {
	//int mu;
	float a1,a2;
	//for (mu=0; mu<10; mu++) {
		a1=0;
		for (int j=0; j<REACTIONS+MOLECULES; j++) {
			if (j==0) a1=0;
			else a1+=a[j-1];
			if (a1<=a0) {
				a2=a1+a[j];
				//for (int j=0; j<mu; j++) a2+=a[j];
				if (a0<a2) {
					//printf ("a1:%f, a0:%f, a2:%f\nmu:%d, a[mu]:%f\n", a1, a0, a2, j, a[j]);
					return j;
				}
			}
		}
	//}
	//no more reactions possible!
	return SPECIES-1;
}

__device__ float h(site* s_site_state, molecule* s_molecule_state, float c, const int* v, int i, int CellID, int tot_site, const int* abdS) {
	/*
	dimensione accettabile REACTIONS*2*CELLS 
	memo[MOLECULES][NUMSITE]; fatto all'inizio 

	*/
	//int order=0;
	float p=0; //propensity
	int tmp=0;
	//errore dopo la prima reazione rimane a 0!!!
	int deb=0;
	int deb2=0;
	bool debug=false;
	//if (i==1 && CellID==578) CUPRINTF("debug: %d %d\n", v[i*2], v[i*2+1]);
	//int uno=0;
	//int due=0;
	for (int j=0; j<MOLECULES; j++) {
		if (s_molecule_state[j].cell==CellID) {
			memo[CellID][j][0]=CountSiteofSpec(s_site_state, v[i*2], CellID, tot_site, j, abdS);
			//if (i==1 && memo[CellID][j][0]>0) uno++;
			memo[CellID][j][1]=CountSiteofSpec(s_site_state, v[i*2+1], CellID, tot_site, j, abdS);
			//if (i==1 && memo[CellID][j][1]>0) due++;
		} else {
			memo[CellID][j][0]=0;
			memo[CellID][j][1]=0;
		}
	}

	//if (i==1 && uno>0) CUPRINTF("debug4: uno %d, due %d\n", uno, due);
	/* too slow
	posso usare un array a parte che tiene sempre in memoria l'abd di ogni specie, ma c'� il CellID che continua a cambiare...
	quindi potrei fare cos�:
	1) contare tutto a inizio ciclo
		- memo[CellID][j] e poi faccio tmp+=memo[CellID][v[i*2]]
	2) modificare l'array globale durante la diffusion
	*/
	for (int j=0; j<MOLECULES; j++) {
		tmp=0;
		if (s_molecule_state[j].cell==CellID) {
			for (int k=0; k<MOLECULES; k++) {
				if (j!=k) { tmp+=memo[CellID][k][1];}
				//if (j!=k) { tmp+=memo[k][v[i*2]];}
			}
			p+=tmp*memo[CellID][j][0];
		}
		//p+=tmp*memo[j][v[i*2+1]];
	}
	
	//if (i==1 && CellID==578) CUPRINTF("debug2: %f\n",p);
	//ma non lo conto due volte? forse no
	/*inizio debug

	if (i==2 && (deb>0 || deb2>0)) {
		CUPRINTF("error2! %d - %d\t%f\n", deb, deb2, p);
		if (debug) CUPRINTF("entrato!\n");
	}

	int tot_s1=0;
	for (int k=0; k<tot_site; k++) {
		if (s_site_state[k].specie==v[i] && !s_site_state[k].bound && s_site_state[k].cell==CellID) tot_s1++;
	}
	if (tot_s1==0 && p!=0) CUPRINTF("error! %f\n", p);
	fine debug*/

	if (p<=0) p=0;
	else p=p*c;
	return p;
}


__device__ float hu(site* s_site_state, molecule* s_molecule_state, const float* c, const int* v, int i, int CellID, int tot_site, int tot_mol, const int* abdS) {
	
	float p=1;
	bool found;
	bool never=true;
	int k;
	
	for (int j=0; j<tot_site; j++) {
		if (s_site_state[j].mol==i && s_site_state[j].bound!=NULL) {
			//CUPRINTF("debug2!\n");
			never=false;
			found=false;
			for (k=0; k<REACTIONS*2 && !found; k+=2) {
				if (v[k]==s_site_state[j].specie && v[k+1]==s_site_state[j].bound->specie) found=true;
			}
			p*=c[k];
		}
	}
	if (never) {
		p=0;
		//CUPRINTF("debug!\n");
	} else {
		//CUPRINTF("debug!\n");
	}

	return p*10;
}

__device__ int CountSiteofSpec(site* s_site_state, int i, int CellID, int tot_site, int mol, const int* abdS) {
	int count=0; //propensity
	//for (int k=abdS[i]; k<abdS[i+1] ; k++) {
		//if (s_site_state[k].bound==NULL && s_site_state[k].mol==mol) count++;
	//}
	
	if (CellList[CellID]!=NULL) {
		l_site* old;
		old=CellList[CellID];
		int k;
		/*for (k=1; k<abdCS[CellID][i]; k++) {
			old=old->next;
		}
		//CUPRINTF("h: %d - %d\n", k, abdCS[CellID][i+1]);
		while (old->next!=NULL && k<abdCS[CellID][i+1]) {*/
		while (old->next!=NULL) {
			k++;
			CUPRINTF("old->s.specie = %d\n", old->s->specie);
			if (old->s->bound==NULL && old->s->mol==mol && old->s->specie==i) count++;
			old=old->next;
		}
		if (old->s->bound==NULL && old->s->mol==mol && old->s->specie==i) count++;
	}
	return count;
}


__device__ int CountSiteofSpec_u(site* s_site_state, int i, int CellID, int tot_site, int mol, const int* abdS) {
	//int order=0;
	int count=0; //propensity
	//site* lst = domLst;
	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
		if (s_site_state[k].cell==CellID && s_site_state[k].specie==i && s_site_state[k].bound==NULL && s_site_state[k].mol==mol) count++;
	}
	return count;
}

__device__ void chooseSite(int type1, int type2, int* s1, int* s2, float u, int tot_site, site* s_site_state, int CellID, const int* abdS) {
	int tot_s1=0;
	int tot_s2=0;
	//non conto che devo essere nella stessa cella!!!
	for (int k=abdS[type1]; k<abdS[type1+1]; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].cell==CellID) tot_s1++;
	}

	//if (tot_s1==0) CUPRINTF("error! %d - %d\n", type1, type2); 
	int ts1=(tot_s1-1)*u;
	int tmp=tot_s1;
	tot_s1=0;
	bool debug=false;
	int first_mol;
	for (int k=abdS[type1]; k<abdS[type1+1]; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].cell==CellID) {
			debug=true;
			if (tot_s1==ts1) { 
				*s1=k; 
				first_mol=s_site_state[k].mol;
			}
			tot_s1++;
		}
	}
	//if (!debug) CUPRINTF("error: u=%f, ts1=%f, tot_s1 %d\n", u, ts1, tmp); //perch� tot_s1, ora tmp, � a 0 e la propensit� non � andata a 0 facendo scegliere questa reazione?!?!?
	
	for (int k=abdS[type2]; k<abdS[type2+1]; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].mol!=first_mol && s_site_state[k].cell==CellID) tot_s2++;
	}
	tmp=tot_s2;
	int ts2=(tot_s2-1)*u;
	tot_s2=0;
	for (int k=abdS[type2]; k<abdS[type2+1]; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].mol!=first_mol && s_site_state[k].cell==CellID) {
			if (tot_s2==ts2) *s2=k;
			tot_s2++;
		}
	}
}

__device__ void updateComplex(molecule* s_molecule_state, int c, int cellID, int tot, int newc) {
	//int order=0;

	for (int k=0; k<tot ; k++) {
		if (s_molecule_state[k].complex==c) s_molecule_state[k].complex=newc;
	}
	//while (lst!=NULL) {
		//if (lst->s.specie==i && lst->s.bound==false) count++;
		//lst=lst->next;
		//if (lst!=NULL) if (lst->s.specie==i && lst->s.bound==false) count++;
	//}
}


__device__ void updateMol(molecule* s_molecule_state, site* s_site_state, int cell, int id, int compl, int tot, int tot_m, int CellID) {
	bool diffuse=true;
	for (int i=0; i<tot_m && diffuse; i++) {
		if (s_molecule_state[i].complex==compl && s_molecule_state[i].id!=id) diffuse=s_molecule_state[i].readyToDiffuse;
	}
	l_site* old;
	l_site* old2;
	l_site* prec;
	bool first;
	if (diffuse) {
		for (int i=0; i<tot_m; i++) {
			if (s_molecule_state[i].complex==compl) {
				/*
				if (CellList[CellID]!=NULL) {
					old=CellList[CellID];
					prec=CellList[CellID];
					while (old->next!=NULL) {
						if (old->s.mol==s_molecule_state[i].id) {
							//sposta il puntatore;
							prec->next=old->next;
							//metti old nella nuova lista
							if (CellList[cell]!=NULL) {
								int k=0;
								old2=CellList[cell];
								while (old2->next!=NULL && k<abdCS[cell][i+1]) {
									k++;
									old2=old2->next;
								}
								old->next=old2->next;
								old2->next=old;
								abdCS[cell][old->s.specie]++;
								abdCS[CellID][old->s.specie]--;
								if (old2->s.specie!=old->s.specie) CUPRINTF("ERROR!\n");
							}
							else {
								CellList[cell]=old;
								abdCS[cell][old->s.specie]++;
								abdCS[CellID][old->s.specie]--;
							}
							//aggiorna s
							old->s.cell=cell;
						}		
						old=old->next;
						prec=old;
					}
					
					if (old->s.mol==s_molecule_state[i].id) {
						//sposta il puntatore;
						prec->next=old->next;
						//metti old nella nuova lista
						if (CellList[cell]!=NULL) {
							int k=0;
							for (k=0; k<abdCS[cell][i]; k++) {
							
							}
							old2=CellList[cell];
							while (old2->next!=NULL && k<abdCS[cell][i+1]) {
								k++;
								old2=old2->next;
							}
							old->next=old2->next;
							old2->next=old;
							abdCS[cell][old->s.specie]++;
							abdCS[CellID][old->s.specie]--;
							if (old2->s.specie!=old->s.specie) CUPRINTF("ERROR!\n");
						}
						else {
							CellList[cell]=old;
							abdCS[cell][old->s.specie]++;
							abdCS[CellID][old->s.specie]--;
						}
						//aggiorna s
						old->s.cell=cell;
					}		
				}
*/
				/*for (int k=0; k<tot ; k++) {
					if (s_site_state[k].mol==s_molecule_state[i].id) {
						s_site_state[k].cell=cell;
						find=l_site_state[CellID];

					}
				}*/
				s_molecule_state[i].cell=cell;
			}
		}
	}
}

__device__ void updateC(molecule* s_molecule_state, int compl, site* s1) {
	site* s = s1;
	/*while (s->bound!=NULL) {
		s=s->bound;
		s_molecule_state[s->mol]=compl;
		
	}*/
}

__device__ void autocomplexation(site* s_site_state, molecule* s_molecule_state, int compl, const int* v, int tot_m, int tot_s, int reacts, curandState* localState) {
	bool next;
	float u;
	//manca da controllare che il site non sia bound
	for (int i=0; i<tot_m; i++) {
		if (s_molecule_state[i].complex==compl) {
			for (int k=0; k<tot_s; k++) {
				if (s_site_state[k].mol==s_molecule_state[i].id && s_site_state[k].bound==NULL) {
					for (int l=0; l<reacts*2; l++) {
						if(s_site_state[k].specie==v[l]) {
							if (l%2==0) next=true;
							else next=false;
							for (int j=0; j<tot_m; j++) {  //allora cerco la seconda specie in una molecola diversa
								if (i!=j && s_molecule_state[j].complex==compl) {
									for (int m=0; m<tot_s; m++) {
										if (s_site_state[m].mol==s_molecule_state[j].id && s_site_state[m].bound==NULL && ((s_site_state[m].specie==v[l+1] && next) || (s_site_state[m].specie==v[l-1] && !next))) {
											u=curand_uniform_double(localState);
											if (u>0.1) {
												s_site_state[m].bound=&s_site_state[k];
												s_site_state[k].bound=&s_site_state[m];
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

// Parse program arguments
void ParseArguments(int argc, char** argv)
{
	for (int i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "--displaystep") == 0 ||
			strcmp(argv[i], "-dispalystep") == 0) 
		{
            displaystep = true;
        }
        if (strcmp(argv[i], "-t") == 0) 
		{
			if (isdigit(atoi(argv[i+1]))) 
            tm = atoi(argv[i+1]);
        }
        if (strcmp(argv[i], "-l") == 0) 
		{
			if (isdigit(atoi(argv[i+1]))) 
            l = atoi(argv[i+1]);
        }
        if (strcmp(argv[i], "-f") == 0) 
		{
			if (strcmp(argv[i+1], "")!=0) 
            inputfile = argv[i+1];
        }
        if (strcmp(argv[i], "-h") == 0) 
		{
			printf("Welcome to GPU Gillespie Multi-Particle\n usage:\n--displaystep\tshow current state after each diffusion\n-t\tmaximum simulation time\n-l\tlattice size\n-f\tinput file containing reactions and initial quantities\n-h\thelp"); 
			exit(0);
        }
	}
}
