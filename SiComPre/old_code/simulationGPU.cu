/*
 * Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/* Vector addition: C = A + B.
 *
 * This sample is a very basic sample that implements element by element
 * vector addition. It is the same as the sample illustrating Chapter 3
 * of the programming guide with some additions like error checking.
 *
 */

// Includes
#include <stdio.h>
#include <stdlib.h>
#include <cutil_inline.h>
#include <math.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cuda_runtime_api.h>
#include <time.h>
#include "eventQueue.h"
// Graphics includes
#include <GL/glew.h>
#if defined (_WIN32)
#include <GL/wglew.h>
#endif

#if defined(__APPLE__) || defined(MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <shrUtils.h>
#include <cutil_gl_inline.h>
#include <cuda_gl_interop.h>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <ctype.h>
//#include "cuPrintf.cu"
#define SPECIES 10
#define REACTIONS 10	
#define CELLS 1536	//# of subvolumes
#define BLOCKS 24	//a block is executed by a processor, my GPU has 12 processor, so is better choose a multiple of 12
#define THREADS 64	//threads executed in each block, for max occupancy choose a multiple of 32, 32 is the warp size
#define GRID_W 6	//grid width = 6*8 subvolumes
#define GRID_H 4	//grid height = 4*8 subvolumes
#define BLOCK_W 8
#define BLOCK_H 8
//AVL


struct node {
	double t;
	int j;
	int h;
	node* next;
};


// Variables

int species;
//bool noprompt = false;
int tm=100;
float l=1; //lattice size
//eventQueue* eq; //event queue
int cells; //number of subvolume in the grid
int* totParticles;
float* c; //vector of rates;
float* d_c;
float* d_tau;
int* d_n;
float* deb;
float* d_deb;
float* d_ts;
int** v; //particle used in reaction, example: v[i][j]==-1 the reaction i consumed 1 particle j;
int** d_v;
int** h_v;
int** speciesAbd; //corresponds to internal state of a LP, on CPU
int** d_speciesAbd; //corresponds to internal state of a LP, on Device
int** h_speciesAbd; //corresponds to internal state of a LP, on Host
double** rates; //lambda value
bool displaystep=false;
char* inputfile;

//__shared__ int abd[SPECIES];
//__shared__ int v_shr[REACTIONS][SPECIES]; //10 is the species

// Functions
void Cleanup(void);
__device__ float Exponential(float, float);
__device__ float h(int*, float, int*);
__device__ int Weights(float*, float);
void ParseArguments(int, char**);
void initGL();
void cudaGLInit();
void initParticleSystem(int , bool);

// Device code

//Direct Method
__global__ void DM(curandState *state, int** s_speciesAbd,int** s_v, const float* s_c, float tmax, float* s_deb, const int reacts) {
	//int species=10;
	//int reactions=10;
	float a0=0;
	__shared__ float a[THREADS][REACTIONS]; //10 is the reaction
	__shared__ int v[REACTIONS][SPECIES];
	//float c[10];
	float tau;
	__shared__ int abd[THREADS][SPECIES];
	int mu;
	float u,u2;
	float t_max=tmax;
	int CellID=blockDim.x * blockIdx.x + threadIdx.x;
	
	

	//s_deb[4]++;
	//s_speciesAbd[0][0]++;
	curand_init(clock(), 1, 0, &state[CellID]); //seed, sequence, offset, state
	curandState localState = state[CellID];

	//now abd is a shared variable and is 100 times faster than s_speciesAbd
	//if (threadIdx.x==0) {
	for (int i=0; i<SPECIES; i++) {
		abd[threadIdx.x][i]=s_speciesAbd[CellID][i]; 
	}
	//}
	if (threadIdx.x==0) {
		for (int j=0; j<REACTIONS; j++) {
			for (int i=0; i<SPECIES; i++) {
				v[j][i]=s_v[j][i];
			}
		}
	}
	__syncthreads();
	float t=0;
	//s_deb[0]=t_max;
	while (t<t_max) {
		
		a0=0;
		for (int j=0; j<reacts; j++) {
			a[threadIdx.x][j]=h(abd[threadIdx.x], s_c[j], v[j]);
			//a[j]=h(s_speciesAbd[CellID], s_c[j], s_v[j]);
			if (a[threadIdx.x][j]<0) s_deb[0]=10;
			a0+=a[threadIdx.x][j];
		}
		u=curand_uniform_double(&localState);
		u2=curand_uniform_double(&localState);
		tau=Exponential(a0,u);
		mu=Weights(a[threadIdx.x],a0*u2);
		
		
		for (int i=0; i<SPECIES; i++) {
			abd[threadIdx.x][i]+=v[mu][i];
			//if (s_speciesAbd[CellID][i]==0) s_deb[0]=1;
			//s_speciesAbd[CellID][i]+=s_v[mu][i];
			//if (s_speciesAbd[CellID][i]<0 && s_deb[0]==1) s_deb[1]=s_speciesAbd[CellID][i];
		}
		t+=tau;
		
	}
	
	for (int i=0; i<SPECIES; i++) {
		s_speciesAbd[CellID][i]=abd[threadIdx.x][i]; 
	}
	/*for (int j=0; j<reactions; j++) {
		for (int i=0; i<species; i++) {
			s_v[j][i]=v[j][i];
		}
		s_c[j]=c[j];
	}*/
	
}


__global__ void Diffusion(curandState *state, int** s_speciesAbd, float* s_tau, int* s_n, float* s_ts, float* s_deb, const int reacts) {
	//float tau[10]; //10 is the number of species
	//float n[10];
	int CellID = blockDim.x * blockIdx.x + threadIdx.x;
	__shared__ int abd[THREADS][SPECIES];
	float res=*s_ts;
	float ts=*s_ts;
	//s_deb[3]++;
	for (int i=0; i<SPECIES; i++) {
		abd[threadIdx.x][i]=s_speciesAbd[CellID][i]; 
	}
	__syncthreads();
	int tot=0;
	curand_init(clock(), 1, 0, &state[CellID]); //seed, sequence, offset, state
	curandState localState = state[CellID];
	float u=0;
	for (int i=0; i<SPECIES; i++) { //10 is the specie
		if (ts==s_tau[i]*s_n[i]) {
			if (CellID==0) atomicAdd(&s_n[i],1);
			//s_deb[i]=s_n[i];
			//diffuse specie i
			tot=0;
			
			for (int p=0; p<s_speciesAbd[CellID][i]; p++) {
				u=curand_uniform_double(&localState);
				if ((blockIdx.x>=GRID_W || threadIdx.x>=BLOCK_W)&&u<0.125) {
					//diffusion above
					if (threadIdx.x>=BLOCK_W) s_speciesAbd[CellID-BLOCK_W][i]++;
					else s_speciesAbd[CellID-(GRID_W-1)*THREADS-BLOCK_W][i]++;
					s_speciesAbd[CellID][i]--;
				}

				else if ((blockIdx.x<GRID_W*(GRID_H-1) || threadIdx.x<BLOCK_W*(BLOCK_H-1)) && u>=0.125 && u<0.25) {
					//diffusion below
					if (threadIdx.x<BLOCK_W*(BLOCK_H-1)) s_speciesAbd[CellID+BLOCK_W][i]++;
					else s_speciesAbd[CellID+(GRID_W-1)*THREADS+BLOCK_W][i]++;
					s_speciesAbd[CellID][i]--;
					//tot++;
				}

				else if ((blockIdx.x%GRID_W==0 || threadIdx.x%BLOCK_W!=0) && u>=0.25 && u<0.375) {
					//diffusion left
					if (threadIdx.x%BLOCK_W!=0) s_speciesAbd[CellID-1][i]++;
					else s_speciesAbd[CellID-(BLOCK_H-1)*BLOCK_W-1][i]++;
					s_speciesAbd[CellID][i]--;
					//tot++;
				}

				else if ((blockIdx.x%GRID_W==GRID_W-1 || threadIdx.x%BLOCK_W!=BLOCK_W-1) && u>=0.375 && u<0.5) {
					//diffusion right
					if (threadIdx.x%BLOCK_W!=BLOCK_W-1) s_speciesAbd[CellID+1][i]++;
					else s_speciesAbd[CellID+(BLOCK_H-1)*BLOCK_W+1][i]++;
					s_speciesAbd[CellID][i]--;
				}
				//else don't diffuse!!!
			}

		}
		if (s_tau[i]*s_n[i]<res) res=s_tau[i]*s_n[i];

	}
	//s_deb[0]=res;
	//s_deb[1]=ts;
	if (CellID==0) *s_ts+=res;
		//atomicAdd(s_ts,res);
}



// Host code
int main(int argc, char** argv)
{
    printf("Initializing simulation...\n");
    ParseArguments(argc, argv);
	time_t seconds;

	seconds = time (NULL);


	curandState *devStates;

	// Initialize variables
	//cudaPrintfInit();
	//cells=32*16;
	//species=10;
    size_t sizeQueue = CELLS * sizeof(eventQueue);
    size_t sizeParticles = SPECIES * sizeof(int);
	//eq=(eventQueue*)malloc(sizeQueue);
	totParticles=(int*)malloc(sizeParticles);
	speciesAbd=(int**)malloc(sizeof(int*)*CELLS);
	h_speciesAbd=(int**)malloc(sizeof(int*)*CELLS);
	h_v=(int**)malloc(sizeof(int*)*REACTIONS);
	v=(int**)malloc(sizeof(int*)*REACTIONS); //10 is the number of reactions
	c=(float*)malloc(sizeof(float)*REACTIONS);
	deb=(float*)malloc(sizeof(float)*SPECIES);

	for (int i=0; i<REACTIONS; i++) { //10 is againt the number of reactions
		v[i]=(int*)malloc(sizeParticles);
		for (int j=0; j<SPECIES; j++) {
			v[i][j]=0;
		}
	}

	for (int j=0; j<SPECIES; j++) {
		//totParticles[j]=20*CELLS;
		totParticles[j]=0;
	}

	for (int i=0; i<REACTIONS; i++) {
		c[i]=1;
	}

	for (int i=0; i<CELLS; i++) {
		speciesAbd[i]=(int*)malloc(sizeParticles);
		if(speciesAbd[i] == NULL) {
			printf("error");
		}
		for (int j=0; j<SPECIES; j++) {
			speciesAbd[i][j]=totParticles[j]/CELLS;
		}

	}


	//declaration of reactions
	/*v[0][0]=-1;
	v[0][1]=-1;
	v[0][2]=1;
	//v[0][0]=1;
	//v[9][1]=1;
	
	v[1][2]=-1;
	v[1][3]=-1;
	v[1][4]=1;

	v[2][4]=-1;
	v[2][5]=-1;
	v[2][6]=1;
	//v[2][0]=1;
	
	v[3][6]=-1;
	v[3][7]=-1;
	v[3][8]=1;
	
	v[4][8]=-1;
	v[5][9]=1;
	
	v[6][9]=-1;
	v[6][0]=1;
	
	v[7][1]=1;

	v[8][3]=1;
	
	v[9][5]=1;
	v[9][7]=1;*/
	/**/
	//end of reactions

    // Allocate particles in device memory

	//printf("Output: %d", speciesAbd[0][0]);
	//if (inputfile!=NULL) {
		FILE *pF; 
		char pBuff[3000];
		int in = 0;
		int nn=0;
		int tmp;
		int f=0,nf=0;
		char temp[60];
		int ab;
		int reacts=0;
		 
		pF = fopen("prova.react", "r"); //opening in read mode
		while(fgets(pBuff , 30000 , pF)) {
			if(strcmp(pBuff,"")!=0) {
				int len = strlen(pBuff);
				//pBuff[dbLen-1] = '\0';
				//printf("%s::%d\n", pBuff,len);
				if (pBuff[0]=='r') {
					in=4;
					while(pBuff[in]!=';'&&in<3000) {
						
						printf("run%d\n", in);
						nn=0;
						while(pBuff[in]!=':'&&in<3000) {
							printf("run%c\n", pBuff[in]);
							temp[nn]=pBuff[in];
							in++;
							nn++;
						}
						temp[nn]='\0';
						sscanf(temp, "%d", &ab);
						printf("%s %d\n", temp, ab);
						in++;
						nn=0;
						while(pBuff[in]!='|'&&in<3000) {
							temp[nn]=pBuff[in];
							in++;
							nn++;
						}
						temp[nn]='\0';
						sscanf(temp, "%d", &tmp);
						printf("%s %d\n", temp, ab);
						for (int i=0; i<CELLS; i++) {
							speciesAbd[i][ab]=tmp/CELLS;
						}
						in++;
					}

				} else {
					in=0;
					nn=0;
					while(pBuff[in]!='+'&&pBuff[in]!='>'&&in<30) {
						temp[nn]=pBuff[in];
						in++;
						nn++;
					}
					temp[nn]='\0';
					sscanf(temp, "%d", &tmp);
					printf("%s %d\n", temp, tmp);
					v[reacts][tmp]-=1;
					if (pBuff[in]=='+') {
						nn=0;
						in++;
						while(pBuff[in]!='+'&&pBuff[in]!='>'&&in<30) {
							temp[nn]=pBuff[in];
							in++;
							nn++;
						}
						temp[nn]='\0';
						printf("%s\n", temp);
						sscanf(temp, "%d", &tmp);
						v[reacts][tmp]-=1;
					}
					in++;
					nn=0;
					while(pBuff[in]!=';'&&in<30) {
						temp[nn]=pBuff[in];
						in++;
						nn++;
					}
					temp[nn]='\0';
					printf("%s\n", temp);
					sscanf(temp, "%d", &tmp);
					v[reacts][tmp]+=1;
					if (in==30) {
						printf("syntax error at line:%d", reacts+1);
					}
					reacts++;
				}
			}
		}
	//}

	for (int i=0; i<SPECIES; i++) {
		for (int j=0; j<reacts; j++) {
			printf("%d", v[i][j]);
		}
		printf("\n");
	}
    // Invoke kernel
    //int threadsPerBlock = 256;
    //int blocksPerGrid = (N + threadsPerBlock - 1) / threadsPerBlock;

	float t_sim=0;
	float t_end=tm;
	float* ts; //time of the next diffusion event
	ts=(float*)malloc(sizeof(float));
	int n[SPECIES]; 
	float tau[SPECIES]; 
	float D[SPECIES];
	int dim=2; //dimension
	for (int i=0; i<SPECIES; i++) {
		n[i]=1;
		D[i]=0.3;
	}
	for (int i=0; i<SPECIES; i++) {
		tau[i]=l*l/(2*dim*D[i]);
		printf("tau = %f\n", tau[i]);
	}
	*ts=tau[0];
	for (int i=0; i<SPECIES; i++) {
		if (tau[i]<*ts) *ts=tau[i];
	}


	
	initGL();
    cudaGLInit();

	cutilSafeCall( cudaMalloc((void**)&d_speciesAbd, sizeof(int*)*CELLS) );
    cutilSafeCall( cudaMalloc((void**)&d_v, sizeof(int*)*REACTIONS) );
    cutilSafeCall( cudaMalloc((void**)&d_c, sizeof(float)*REACTIONS) );
    cutilSafeCall( cudaMalloc((void**)&d_deb, sizeof(float)*SPECIES) );
    cutilSafeCall( cudaMalloc((void**)&d_n, sizeof(int)*SPECIES) );
    cutilSafeCall( cudaMalloc((void**)&d_tau, sizeof(float)*SPECIES) );
    cutilSafeCall( cudaMalloc((void**)&d_ts, sizeof(float)) );
	for (int i=0; i<CELLS; i++) {
		cutilSafeCall( cudaMalloc((void**)&h_speciesAbd[i], sizeParticles) );
	}
	for (int i=0; i<REACTIONS; i++) { //10 is the reactions #
		cutilSafeCall( cudaMalloc((void**)&h_v[i], sizeParticles) );
	}

    // Copy vectors from host memory to device memory

	cutilSafeCall( cudaMemcpy(d_speciesAbd, h_speciesAbd, sizeof(int*)*CELLS, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_v, h_v, sizeof(int*)*REACTIONS, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_c, c, sizeof(float)*REACTIONS, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_tau, tau, sizeof(float)*SPECIES, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_n, n, sizeof(int)*SPECIES, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_ts, ts, sizeof(float), cudaMemcpyHostToDevice) );
	for (int i=0; i<CELLS; i++) {
		cutilSafeCall( cudaMemcpy(h_speciesAbd[i], speciesAbd[i], sizeParticles, cudaMemcpyHostToDevice) );
	}

	for (int i=0; i<SPECIES; i++) {
		cutilSafeCall( cudaMemcpy(h_v[i], v[i], sizeParticles, cudaMemcpyHostToDevice) );
	}

displaystep=true;

	FILE *pF2;
if (displaystep) {
	pF2 = fopen("result.out", "w"); //opening in read mode
}

    printf("Performing simulation...\n");  //maybe I don't need ts on CPU, I could just keep it on GPU....
	printf("ts = %f\n", *ts);
	while (t_sim<t_end) {
		DM<<<24, 64>>>(devStates, d_speciesAbd, d_v, d_c, *ts-t_sim, d_deb, reacts);
		cudaThreadSynchronize();
		t_sim=*ts;
		Diffusion<<<24, 64>>>(devStates, d_speciesAbd, d_tau, d_n, d_ts, d_deb, reacts);
		printf("diffusion occurs at time: %f\n", *ts);
		cudaThreadSynchronize();
		cutilSafeCall( cudaMemcpy(ts, d_ts, sizeof(float), cudaMemcpyDeviceToHost) );
		if (displaystep) {
			for (int i=0; i<CELLS; i++) {
				cutilSafeCall( cudaMemcpy(speciesAbd[i], h_speciesAbd[i], sizeParticles, cudaMemcpyDeviceToHost) );
			}
			fprintf(pF2, "Abundances at time: %f\n", t_sim);
			for (int j=0; j<SPECIES; j++) {
				fprintf(pF2, "Species %d\n", j);
				for (int i=0; i<CELLS; i++) {
					//printf("%d: %d\t", i, speciesAbd[i][j]);
					//fprintf(pF2, "%d:%d\t", j, speciesAbd[i][j]);
					fprintf(pF2, "%d\t", speciesAbd[i][j]);
				}
				fprintf(pF2, "\n");
			}
			fflush(pF2);
		}
		//retrive ts from GPU
		//*ts=*ts+1; //wrong
	}

	if (displaystep) fclose(pF2);
	//SubSim<<<16, 32>>>(devStates, d_speciesAbd, d_v, d_c, d_deb);

	//cudaPrintfDisplay(stdout, true);
	//cudaPrintfEnd();

    cutilCheckMsg("kernel launch failure");
#ifdef _DEBUG
    cutilSafeCall( cudaThreadSynchronize() );
#endif

    printf("Simulation ended, analyzing output...\n");

	for (int i=0; i<CELLS; i++) {
		cutilSafeCall( cudaMemcpy(speciesAbd[i], h_speciesAbd[i], sizeParticles, cudaMemcpyDeviceToHost) );
	}

	cutilSafeCall( cudaMemcpy(deb, d_deb, sizeof(float)*SPECIES, cudaMemcpyDeviceToHost) );
	
   
	printf("Final abundances\n");
	int tot, tt=0;
	for (int j=0; j<SPECIES; j++) {
		tot=0;
		for (int i=0; i<CELLS; i++) {
			tot+=speciesAbd[i][j];
		}
		tt+=tot;
		printf("Specie %d, Abd: %d\n", j, tot);
		printf("debug %d: %f\n", j, deb[j]);
	}
    
	printf("Species tot: %d\n", tt);
	printf("Execution time: %d seconds\n", time(NULL)-seconds);
    Cleanup();
}

void Cleanup(void)
{
    // Free device memory
    /*if (d_A)
        cudaFree(d_A);
    if (d_B)
        cudaFree(d_B);
    if (d_C)
        cudaFree(d_C);

    // Free host memory
    if (h_A)
        free(h_A);
    if (h_B)
        free(h_B);
    if (h_C)
        free(h_C);*/
        
    cutilSafeCall( cudaThreadExit() );
    
//    if (!noprompt) {
        printf("\nPress ENTER to exit...\n");
        fflush( stdout);
        fflush( stderr);
        getchar();
  //  }

    exit(0);
}

__device__ float Exponential(float lambda, float u) {
	//return (log(1.0-u)/(-1*(double)lambda));

	return ((-1)/lambda)*logf(u);
}

__device__ int Weights(float* a, float a0) {
	//int mu;
	float a1,a2;
	//for (mu=0; mu<10; mu++) {
		a1=0;
		for (int j=0; j<REACTIONS; j++) {
			if (j==0) a1=0;
			else a1+=a[j-1];
			if (a1<=a0) {
				a2=a1+a[j];
				//for (int j=0; j<mu; j++) a2+=a[j];
				if (a0<a2) {
					//printf ("a1:%f, a0:%f, a2:%f\nmu:%d, a[mu]:%f\n", a1, a0, a2, j, a[j]);
					return j;
				}
			}
		}
	//}
	//no more reactions possible!
	return SPECIES-1;
}

__device__ float h(int* spec, float c, int* v) {
	int order=0;
	float p=1; //propensity
	for (int i=0;i<10; i++) {
		if (v[i]==-1) p=p*spec[i];
		if (v[i]==-2) p=p*spec[i]*(spec[i]-1);
	}
	if (p<=0) p=0;
	else p=p*c;
	return p;
}

// Parse program arguments
void ParseArguments(int argc, char** argv)
{
	for (int i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "--displaystep") == 0 ||
			strcmp(argv[i], "-dispalystep") == 0) 
		{
            displaystep = true;
        }
        if (strcmp(argv[i], "-t") == 0) 
		{
			if (isdigit(atoi(argv[i+1]))) 
            tm = atoi(argv[i+1]);
        }
        if (strcmp(argv[i], "-l") == 0) 
		{
			if (isdigit(atoi(argv[i+1]))) 
            l = atoi(argv[i+1]);
        }
        if (strcmp(argv[i], "-f") == 0) 
		{
			if (strcmp(argv[i+1], "")!=0) 
            inputfile = argv[i+1];
        }
        if (strcmp(argv[i], "-h") == 0) 
		{
			printf("Welcome to GPU Gillespie Multi-Particle\n usage:\n--displaystep\tshow current state after each diffusion\n-t\tmaximum simulation time\n-l\tlattice size\n-f\tinput file containing reactions and initial quantities\n-h\thelp"); 
			exit(0);
        }
	}
}

// initialize OpenGL
void initGL()
{  
    //glutInit();
    //glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
    //glutInitWindowSize(width, height);
    glutCreateWindow("CUDA Particles");

    glewInit();
    if (!glewIsSupported("GL_VERSION_2_0 GL_VERSION_1_5 GL_ARB_multitexture GL_ARB_vertex_buffer_object")) {
        fprintf(stderr, "Required OpenGL extensions missing.");
        exit(-1);
    }

#if defined (_WIN32)
    if (wglewIsSupported("WGL_EXT_swap_control")) {
        // disable vertical sync
        wglSwapIntervalEXT(0);
    }
#endif

    glEnable(GL_DEPTH_TEST);
    glClearColor(0.25, 0.25, 0.25, 1.0);

    glutReportErrors();
}

void cudaGLInit()
{   
    // use command-line specified CUDA device, otherwise use device with highest Gflops/s
   // if( cutCheckCmdLineFlag(argc, (const char**)argv, "device") ) {
        //cutilDeviceInit(argc, argv);
   // } else {
        cudaGLSetGLDevice( cutGetMaxGflopsDeviceId() );
    //}
}

void initParticleSystem(int gridSize, bool bUseOpenGL)
{
    //psystem = new ParticleSystem(gridSize, bUseOpenGL); 
    //psystem->reset(ParticleSystem::CONFIG_GRID);

    if (bUseOpenGL) {
        //renderer = new ParticleRenderer;
        //renderer->setParticleRadius(psystem->getParticleRadius());
        //renderer->setColorBuffer(psystem->getColorBuffer());
    }

	

    //cutilCheckError(cutCreateTimer(&timer));
}

void display() {


}