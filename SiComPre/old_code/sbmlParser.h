#ifndef SBMLP
#define SBMLP
#include <stdio.h>
#include <stdlib.h>
#include "sbmlParser.h"
/*
struct {
	char* name;
	int id;
} specMap;

int specMap_getId(specMap);
char* specMap_getName();*/

int getIDbyName(const char** , const char* , int);
int parser(char*, int** , int** , float* , float* , int , int, int , char**, int);
int SBML_getNumSpecies(char* filename);
int SBML_getNumReactions(char* filename);

#endif