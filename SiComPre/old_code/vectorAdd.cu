/*
 * Simone Rizzetto - Simulation of System Biology project
 * Implementation of Gillespie Multi-Particle on GPGPU using cuda
 * 2010-2011


 Improvements:
 Stream:
	cudaStream_t stream1, stream2;
	cudaStreamCreate(&stream1);
	cudaStreamCreate(&stream2);
	cudaMemcpyAsync( dst, src, size, dir, stream1 );
	kernel<<<grid, block, 0, stream2>>>(�);

	Now the execution time of each cycle is
	Max(DM, mem transfer) Usually mem transfer
	+
	Max(FileWrite, Diffusion) Usually diffusion (CPU and GPU work simultaneusly)
	=
	mem transfer + diffusion -> inhomogenous is no more a problem and it is possible to use global memory in DM without any slowdown

  Threads synchronization:
	Break kernel in 2 and use cudaThreadSynchronize()
 */

// Includes
#include <stdio.h>
#include <stdlib.h>
#include <cutil_inline.h>
#include <math.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cuda_runtime_api.h>
#include <time.h>
// Graphics includes
#include <GL/glew.h>
#if defined (_WIN32)
#include <GL/wglew.h>
#endif

#if defined(__APPLE__) || defined(MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <shrUtils.h>
#include <cutil_gl_inline.h>
#include <cuda_gl_interop.h>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <ctype.h>
#include "sbmlParser.h"

#define SPECIES 22
#define REACTIONS 30	
#define CELLS 1536/2	//# of subvolumes
#define BLOCKS 12	//a block is executed by a processor, my GPU has 12 processor, so is better choose a multiple of 12
#define THREADS 64	//threads executed in each block, for max occupancy choose a multiple of 32, 32 is the warp size
#define GRID_W 6/2	//grid width = 6*8 subvolumes
#define GRID_H 4	//grid height = 4*8 subvolumes
#define BLOCK_W 8
#define BLOCK_H 8




// Variables


float tm=3; //max time of the simulation
float l=0.4; //lattice size
float* c; //vector of rates;
float* d_c; //device
float* d_tau; //device (diffusion)
int* d_n; //devicie (diffusion)
float* d_ts; //device time
int** v; //particle used in reaction, example: v[i][j]==-1 the reaction i consumed 1 particle j;
int** d_v; //device
int** h_v; //host 
int** speciesAbd; //corresponds to internal state of a LP, on CPU
int** d_speciesAbd; //corresponds to internal state of a LP, on Device
int** h_speciesAbd; //corresponds to internal state of a LP, on Host
double** rates; //reaction rates
bool displaystep=false; //print intermediate steps in a file
char* inputfile; //sbml file
__device__ bool sync[BLOCKS]; //variable for blocks synchronization

// Functions
void Cleanup(void);
__device__ float Exponential(float, float);
__device__ float h(int*, float, int*, int );
__device__ int Weights(float*, float, int , int);
void ParseArguments(int, char**);

// Device code

//Direct Method
__global__ void DM(curandState *state, int** s_speciesAbd, int** s_v, const float* s_c, float tmax, int reacts, int nspec) {

	float a0=0;
	__shared__ float a[THREADS][REACTIONS]; 
	__shared__ int v[REACTIONS][SPECIES];

	float tau;
	__shared__ int abd[THREADS][SPECIES];
	int mu;
	float u,u2;
	float t_max=tmax;
	int CellID=blockDim.x * blockIdx.x + threadIdx.x;
	
	
	curand_init(clock(), 1, 0, &state[CellID]); //seed, sequence, offset, state
	curandState localState = state[CellID];

	//now abd is a shared variable and is 100 times faster than s_speciesAbd
	for (int i=0; i<nspec; i++) {
		abd[threadIdx.x][i]=s_speciesAbd[CellID][i]; 
	}

	if (threadIdx.x==0) {
		for (int j=0; j<REACTIONS; j++) {
			for (int i=0; i<nspec; i++) {
				v[j][i]=s_v[j][i];
			}
		}
	}
	__syncthreads();
	float t=0;
	while (t<t_max) {
		
		a0=0;
		for (int j=0; j<reacts; j++) {
			a[threadIdx.x][j]=h(abd[threadIdx.x], s_c[j], v[j], nspec);
			a0+=a[threadIdx.x][j];
		}
		u=curand_uniform_double(&localState);
		while (u==0.0f) u=curand_uniform_double(&localState); 
		u2=curand_uniform_double(&localState);
		tau=Exponential(a0,u);
		mu=Weights(a[threadIdx.x],a0*u2, nspec, reacts);
		if (mu==-1) break;
		
		for (int i=0; i<nspec; i++) {
			abd[threadIdx.x][i]+=v[mu][i];
		}
		t+=tau;
		
	}
	
	for (int i=0; i<nspec; i++) {
		s_speciesAbd[CellID][i]=abd[threadIdx.x][i]; 
	}
	
}


__global__ void Diffusion(curandState *state, int** s_speciesAbd,const float* s_tau, int* s_n, float* s_ts, const int reacts, float time, int nspec) {

	int CellID = blockDim.x * blockIdx.x + threadIdx.x;
	float res;
	float ts=time;
	
	if (CellID==0) res=s_tau[0]*(s_n[0]+1);
	__shared__ int diff[BLOCK_W*BLOCK_H][SPECIES];
	for (int i=0; i<nspec; i++) { 
		if (ts==s_tau[i]*s_n[i]) {
			diff[threadIdx.x][i]=s_speciesAbd[CellID][i];
		}
	}

	//sync between blocks 
	__syncthreads();
	sync[blockIdx.x]=true;
	bool s=true;
	for (int i=0; i<BLOCKS; i++) s=sync[i] && s;
	while (!s) for (int i=0; i<BLOCKS; i++) s=sync[i] && s;
	//sync end


	curand_init(clock(), 1, 0, &state[CellID]); //seed, sequence, offset, state
	curandState localState = state[CellID];
	float u=0;
	for (int i=0; i<nspec; i++) { 

		if (ts==s_tau[i]*s_n[i]) {
			
			if (CellID==0) {
				atomicAdd(&s_n[i],1);
			}
			//diffuse specie i

			for (int p=0; p<diff[threadIdx.x][i]; p++) {
				u=curand_uniform_double(&localState);
				if ((blockIdx.x>=GRID_W || threadIdx.x>=BLOCK_W)&&u<0.125) {
					//diffusion above
					if (threadIdx.x>=BLOCK_W) {
						atomicAdd(&s_speciesAbd[CellID-BLOCK_W][i],1);
					}
					else {
						atomicAdd(&s_speciesAbd[CellID-(GRID_W-1)*THREADS-BLOCK_W][i],1);
					}
					atomicAdd(&s_speciesAbd[CellID][i],-1);
				}

				else if ((blockIdx.x<GRID_W*(GRID_H-1) || threadIdx.x<BLOCK_W*(BLOCK_H-1)) && u>=0.125 && u<0.25) {
					//diffusion below
					if (threadIdx.x<BLOCK_W*(BLOCK_H-1)) {
						atomicAdd(&s_speciesAbd[CellID+BLOCK_W][i],1);
					}
					else {
						atomicAdd(&s_speciesAbd[CellID+(GRID_W-1)*THREADS+BLOCK_W][i],1);
					}
					atomicAdd(&s_speciesAbd[CellID][i],-1);
				}

				else if ((blockIdx.x%GRID_W!=0 || threadIdx.x%BLOCK_W!=0) && u>=0.25 && u<0.375) {
					//diffusion left
					if (threadIdx.x%BLOCK_W!=0) {
						atomicAdd(&s_speciesAbd[CellID-1][i],1);
					}
					else {
						atomicAdd(&s_speciesAbd[CellID-(BLOCK_H-1)*BLOCK_W-1][i],1);
					}
					atomicAdd(&s_speciesAbd[CellID][i],-1);
				}

				else if ((blockIdx.x%GRID_W!=GRID_W-1 || threadIdx.x%BLOCK_W!=BLOCK_W-1) && u>=0.375 && u<0.5) {
					//diffusion right
					if (threadIdx.x%BLOCK_W!=BLOCK_W-1) {
						atomicAdd(&s_speciesAbd[CellID+1][i],1);
					}
					else {
						atomicAdd(&s_speciesAbd[CellID+(BLOCK_H-1)*BLOCK_W+1][i],1);
					}
					atomicAdd(&s_speciesAbd[CellID][i],-1);
				}
				//else don't diffuse!
			}

		}
		

		if (CellID==0) {
			if (s_tau[i]*s_n[i]<res) res=s_tau[i]*s_n[i];
		}

	}

	if (CellID==0) *s_ts=res;
}



// Host code
int main(int argc, char** argv)
{
	time_t seconds;
	seconds = time (NULL);

	printf("BLOCKS %d\n", BLOCKS);
    printf("Initializing simulation...\n");
    ParseArguments(argc, argv);
	printf("lattice size:%f\nMaximum simulation time:%f\n", l, tm);
	// Initialize variables
	int reacts=0;
	int nspec=0;
	inputfile="mapk.xml";
	nspec=SBML_getNumSpecies(inputfile);		//get number of species declared in sbml
	reacts=SBML_getNumReactions(inputfile);	//get number of reactions declared in sbml

	curandState *devStates;	//needed for initialize random number generator

    size_t sizeParticles = SPECIES * sizeof(int);
	speciesAbd=(int**)malloc(sizeof(int*)*CELLS);
	h_speciesAbd=(int**)malloc(sizeof(int*)*CELLS);
	h_v=(int**)malloc(sizeof(int*)*REACTIONS);
	v=(int**)malloc(sizeof(int*)*REACTIONS);
	c=(float*)malloc(sizeof(float)*REACTIONS);

	for (int i=0; i<REACTIONS; i++) { 
		v[i]=(int*)malloc(sizeParticles);
		for (int j=0; j<SPECIES; j++) {
			v[i][j]=0;
		}
	}

	for (int i=0; i<REACTIONS; i++) {
		c[i]=1;
	}

	for (int i=0; i<CELLS; i++) {
		speciesAbd[i]=(int*)malloc(sizeParticles);
		if(speciesAbd[i] == NULL) {
			printf("error");
		}
		for (int j=0; j<SPECIES; j++) {
			speciesAbd[i][j]=0;
		}

	}

	float D[SPECIES];
	for (int i=0; i<SPECIES; i++) {
		D[i]=0.00000001;  //it will be never diffuse!
	}
	char** map;	//mapping number to species type
	map=(char**)malloc(sizeof(char*)*SPECIES);
	
	if (parser(inputfile, speciesAbd, v, c, D, CELLS, reacts, nspec, map, CELLS/2-THREADS*GRID_W/2)<0) exit(0); //if the parsing of sbml retrive a value lower than 0, the sbml is not valid
	

	float t_sim=0;
	float t_end=tm;
	float* ts; //time of the next diffusion event
	ts=(float*)malloc(sizeof(float));
	int n[SPECIES]; 
	float tau[SPECIES]; 
	int dim=2; //dimensions
	for (int i=0; i<SPECIES; i++) {
		n[i]=1;
		//D[i]=0.3;
	}
	for (int i=0; i<SPECIES; i++) {
		tau[i]=l*l/(4*dim*D[i]);
	}
	*ts=tau[0];
	for (int i=0; i<SPECIES; i++) {
		if (tau[i]<*ts) *ts=tau[i];
	}

	//allocation of memory on the GPU device
	cutilSafeCall( cudaMalloc((void**)&d_speciesAbd, sizeof(int*)*CELLS) );
    cutilSafeCall( cudaMalloc((void**)&d_v, sizeof(int*)*REACTIONS) );
    cutilSafeCall( cudaMalloc((void**)&d_c, sizeof(float)*REACTIONS) );
    cutilSafeCall( cudaMalloc((void**)&d_n, sizeof(int)*SPECIES) );
    cutilSafeCall( cudaMalloc((void**)&d_tau, sizeof(float)*SPECIES) );
    cutilSafeCall( cudaMalloc((void**)&d_ts, sizeof(float)) );
	for (int i=0; i<CELLS; i++) {
		cutilSafeCall( cudaMalloc((void**)&h_speciesAbd[i], sizeParticles) );
	}
	for (int i=0; i<REACTIONS; i++) { //10 is the reactions #
		cutilSafeCall( cudaMalloc((void**)&h_v[i], sizeParticles) );
	}

    // Copy values from host memory (CPU) to device memory (GPU)

	cutilSafeCall( cudaMemcpy(d_speciesAbd, h_speciesAbd, sizeof(int*)*CELLS, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_v, h_v, sizeof(int*)*REACTIONS, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_c, c, sizeof(float)*REACTIONS, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_tau, tau, sizeof(float)*SPECIES, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_n, n, sizeof(int)*SPECIES, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_ts, ts, sizeof(float), cudaMemcpyHostToDevice) );
	for (int i=0; i<CELLS; i++) {
		cutilSafeCall( cudaMemcpy(h_speciesAbd[i], speciesAbd[i], sizeParticles, cudaMemcpyHostToDevice) );
	}

	for (int i=0; i<SPECIES; i++) {
		cutilSafeCall( cudaMemcpy(h_v[i], v[i], sizeParticles, cudaMemcpyHostToDevice) );
	}

	//results file
	FILE *pF2;
	pF2 = fopen("result.out", "w"); 
	//displaystep=true;

	fprintf(pF2, "Abundances at time: %f\n", t_sim);
	//int stampa[GRID_H*BLOCK_H][GRID_W*BLOCK_W];
	//int cell;
	for (int j=0; j<nspec; j++) {
		fprintf(pF2, "Species %d\n", j);
		int tot_debug=0;
		//for (int i=0; i<CELLS; i++) {
		//	fprintf(pF2, "%d\t", speciesAbd[i][j]);
		//}

		for (int i=0; i<GRID_H*BLOCK_H; i++) { //row
			for (int k=0; k<GRID_W; k++) { //column
				for (int q=0; q<BLOCK_W; q++) {
					fprintf(pF2, "%d\t", speciesAbd[(i*BLOCK_W)%(BLOCK_H*BLOCK_W)+(i/BLOCK_H)*(BLOCK_W*BLOCK_H*GRID_W)+k*THREADS+q][j]);
//					printf("%d\t", (i*BLOCK_W)%(BLOCK_H*BLOCK_W)+(i/BLOCK_H)*(BLOCK_W*BLOCK_H*GRID_W)+k*THREADS+q);
				}
			}
		} 


		fprintf(pF2, "\n");
	}
	fflush(pF2);

    printf("Performing simulation...\n");
	while (t_sim<t_end) {
		DM<<<BLOCKS, THREADS>>>(devStates, d_speciesAbd, d_v, d_c, *ts-t_sim, reacts, nspec); //direct method kernel
		//add streams
		CUDA_SAFE_CALL(cudaThreadSynchronize());
		t_sim=*ts;
		Diffusion<<<BLOCKS, THREADS>>>(devStates, d_speciesAbd, d_tau, d_n, d_ts, reacts, *ts, nspec);	//diffusion kernel
		CUDA_SAFE_CALL(cudaThreadSynchronize()); //move this after file writing
		cutilSafeCall( cudaMemcpy(ts, d_ts, sizeof(float), cudaMemcpyDeviceToHost) );
		if (displaystep) { //is parameter is set then it write data on the device. It makes the simulation much slower!
			for (int i=0; i<CELLS; i++) {
				cutilSafeCall( cudaMemcpy(speciesAbd[i], h_speciesAbd[i], sizeParticles, cudaMemcpyDeviceToHost) );
			}
			fprintf(pF2, "Abundances at time: %f\n", t_sim);
			for (int j=0; j<nspec; j++) {
				fprintf(pF2, "Species %d\n", j);
				//for (int i=0; i<CELLS; i++) {
				//	fprintf(pF2, "%d\t", speciesAbd[i][j]);
				//}
				
				for (int i=0; i<GRID_H*BLOCK_H; i++) { //row
					for (int k=0; k<GRID_W; k++) { //column
						for (int q=0; q<BLOCK_W; q++) {
							fprintf(pF2, "%d\t", speciesAbd[(i*BLOCK_W)%(BLOCK_H*BLOCK_W)+(i/BLOCK_H)*(BLOCK_W*BLOCK_H*GRID_W)+k*THREADS+q][j]);
						}
					}
				} 

				fprintf(pF2, "\n");
			}
			fflush(pF2);
		}
	}


    cutilCheckMsg("kernel launch failure");
#ifdef _DEBUG
    cutilSafeCall( cudaThreadSynchronize() );
#endif

    printf("Simulation ended, analyzing output...\n");

	for (int i=0; i<CELLS; i++) {
		cutilSafeCall( cudaMemcpy(speciesAbd[i], h_speciesAbd[i], sizeParticles, cudaMemcpyDeviceToHost) );
	}
	
	CUT_CHECK_ERROR("Execution failed!\n");

	fprintf(pF2, "Abundances at time: %f\n", t_sim);
	for (int j=0; j<nspec; j++) {
		fprintf(pF2, "Species %d\n", j);
		//for (int i=0; i<CELLS; i++) {
		//	fprintf(pF2, "%d\t", speciesAbd[i][j]);
		//}
		
		for (int i=0; i<GRID_H*BLOCK_H; i++) { //row
			for (int k=0; k<GRID_W; k++) { //column
				for (int q=0; q<BLOCK_W; q++) {
					fprintf(pF2, "%d\t", speciesAbd[(i*BLOCK_W)%(BLOCK_H*BLOCK_W)+(i/BLOCK_H)*(BLOCK_W*BLOCK_H*GRID_W)+k*THREADS+q][j]);
				}
			}
		} 

		fprintf(pF2, "\n");
	}
	fflush(pF2);
	fclose(pF2);
	printf("Final abundances\n");
	int tot, tt=0;
	for (int j=0; j<nspec; j++) {
		tot=0;
		for (int i=0; i<CELLS; i++) {
			tot+=speciesAbd[i][j];
		}
		tt+=tot;
		printf("Specie %s, Abd: %d\n", map[j], tot);
	}
    
	printf("Species tot: %d\n", tt);
	printf("Execution time: %d seconds\n", time(NULL)-seconds);
    Cleanup();
}

void Cleanup(void)
{
    // Free device memory
    if (d_speciesAbd)
        cudaFree(d_speciesAbd);

    if (d_c)
        cudaFree(d_c);

    if (d_v)
        cudaFree(d_v);
    // Free host memory
	/*
	for (int i=0; i<CELLS; i++) {
		if (h_speciesAbd[i])
			free(h_speciesAbd[i]);
	}
	
	for (int i=0; i<REACTIONS; i++) {
		if (h_v[i]) free(h_v[i]);
	}*/

    cutilSafeCall( cudaThreadExit() );
    

    printf("\nPress ENTER to exit...\n");
    fflush( stdout);
    fflush( stderr);
    getchar();

    exit(0);
}

__device__ float Exponential(float lambda, float u) {
	return ((-1)/lambda)*logf(u);
}

__device__ int Weights(float* a, float a0, int nspec, int reacts) {
	
	float a1,a2;
	a1=0.0f;
	for (int j=0; j<reacts; j++) {
		if (j==0) a1=0.0f;
		else a1+=a[j-1];
		if (a1<=a0) {
			a2=a1+a[j];
			if (a0<a2) {
				return j;
			}
		}
	}
	//no more reactions possible!
	return -1;
}

__device__ float h(int* spec, float c, int* v, int nspec) {
	//int order=0;
	float p=1.0f; //propensity
	for (int i=0;i<nspec; i++) {
		if (v[i]==-1) p=p*spec[i];
		if (v[i]==-2) p=p*spec[i]*(spec[i]-1);
	}
	if (p<=0.0f) p=0.0f;
	else p=p*c;
	return p;
}

// Parse program arguments
void ParseArguments(int argc, char** argv)
{
	for (int i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "--displaystep") == 0 ||
			strcmp(argv[i], "-dispalystep") == 0) 
		{
            displaystep = true;
        }
        if (strcmp(argv[i], "-t") == 0) 
		{
			//if (isdigit(atoi(argv[i+1]))) 
            //tm = atoi(argv[i+1]);
			sscanf(argv[i+1],"%f", &tm);
			//printf("tm:%d", tm);
        }
        if (strcmp(argv[i], "-l") == 0) 
		{
			//if (isdigit(atoi(argv[i+1]))) 
            //l = atoi(argv[i+1]);
			sscanf(argv[i+1],"%f", &l);
			//printf("l:%d", l);
        }
        if (strcmp(argv[i], "-f") == 0) 
		{
			if (strcmp(argv[i+1], "")!=0) 
            inputfile = argv[i+1];
        }
        if (strcmp(argv[i], "-h") == 0) 
		{
			printf("Welcome to GPU Gillespie Multi-Particle\n usage:\n--displaystep\tshow current state after each diffusion\n-t\tmaximum simulation time\n-l\tlattice size\n-f\tinput file containing reactions and initial quantities\n-h\thelp"); 
			exit(0);
        }
	}
}
