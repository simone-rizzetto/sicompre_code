/*
 * Simone Rizzetto - Biological Systems Simulation Complex Version
 * Implementation of Gillespie Multi-Particle/Complex on GPGPU using CUDA 
 * 2010-2011

 What's new? This version allow the definition of specie type. It simulate only complexation and decomplexation of
 molecule. It use the speciesType as a whole molecule and a simple specie as binding site. To perform a classic simulation 
 use the previous version of GPU Gillespie Multi-Particle.

 Assumption:
 1) Unbinding rate between 0 and 1

 TODO list:
 1) Check for events, ex: never can happen that molecule[i].cell=x && site[j].mol==i && site[j].cell!=x

 Current Problems:

 Possible Improvements:
 1) State array divided in molecule instead of species, but be carefull to site id!
 2) Autocomplexation is required? 
 3) Cublas or thrust?
 4) Tesla or GPUcapability>2.0

 Future Works:
 1) Incorporate Complex modality with normal version 
 2) 2D space to 3D space (I'm not sure that this will change something, except increase complexity...)
 
 Test:
 1) Large protein complex experiment 20 molecules, 60 sites, 6 different domain type, 2 different protein type, 3 reactions
    Results:
	Parameters: time=1.0, lattice size=0.4 - Memory used: 1640 B - Simulation total time: 5 seconds
 2) Large protein complex experiment #2 200 molecules, 600 sites, 6 different domain type, 2 different protein type, 3 reactions
    Results:
	Parameters: time=1.0, lattice size=0.4 - Memory used: 16400 B - Simulation total time: 5 seconds
 3) Largest experiment possible (using real data - whole golgi compartment)
	Parameters: time=1.0, lattice size=0.4 - Memory used: - Simulation total time: 

 */

// Includes
#include <stdio.h>
#include <stdlib.h>
#include <cutil_inline.h>
#include <math.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cuda_runtime_api.h>
#include <time.h>
// Graphics includes
#include <GL/glew.h>
#if defined (_WIN32)
#include <GL/wglew.h>
#endif

#if defined(__APPLE__) || defined(MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <shrUtils.h>
#include <cutil_gl_inline.h>
#include <cuda_gl_interop.h>
#include <cuPrintf.cu>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <ctype.h>
#include "sbmlParserC.h"
#include "GPGMPComplex.cuh"


//The macro CUPRINTF is defined for architectures
//with different compute capabilities.
#if __CUDA_ARCH__ < 200 	//Compute capability 1.x architectures
#define CUPRINTF cuPrintf
#else						//Compute capability 2.x architectures
#define CUPRINTF(fmt, ...) printf("[%d, %d]:\t" fmt, \
								blockIdx.y*gridDim.x+blockIdx.x,\
								threadIdx.z*blockDim.x*blockDim.y+threadIdx.y*blockDim.x+threadIdx.x,\
								__VA_ARGS__)
#endif



/*#define SPECIES 386	
#define PROT_SPECIES 21
#define NUMSITE 26
#define MOLECULES 334
#define REACTIONS 21	
#define SPECIES 206821	
#define PROT_SPECIES 460
#define NUMSITE 8008
#define MOLECULES 12620
#define REACTIONS 7515 //the reactions number include both complex and decomplex reaction declared in the input file
/*#define SPECIES 32997	 //cytoplasm
#define PROT_SPECIES 815
#define NUMSITE 804
#define MOLECULES 22127
#define REACTIONS 1380*/
/*#define SPECIES 27535	 //nucleous
#define PROT_SPECIES 842
#define NUMSITE 884
#define MOLECULES 17885
#define REACTIONS 2304*/
#define SPECIES 18931	
#define PROT_SPECIES 281
#define NUMSITE 259
#define MOLECULES 5780
#define REACTIONS 262
#define CELLS 8192	//# of subvolumes
#define BLOCKS 128	//a block is executed by a processor, my GPU has 12 processor, so is better choose a multiple of 12
#define THREADS 64	//threads executed in each block, for max occupancy choose a multiple of 32, 32 is the warp size
#define GRID_W 16	//grid width = 6*8 subvolumes
#define GRID_H 8	//grid height = 4*8 subvolumes
#define BLOCK_W 8
#define BLOCK_H 8


//problem how to rapresent state vector of species?!?!?

/* possible solution using CLASSES
Class Prot {
	prot()
	getDom()
	getCompl()
	getSpecie()

	dom* dom_lst
	ushort int compl
	ushort int specie
}

*/

/*
struct molecule {
	unsigned short complex;
	unsigned short specie;
};

struct site {
	unsigned short specie;
	site* bound;  //it is not efficient, I've to store 2 times the same information!!! but it is simpler
	molecule* mol;
};

struct site_list {
	site s;
	site_list* next;
};

struct molecule_list {
	molecule m;
	molecule_list* next;
};



molecule_list* prot_lst[CELLS];
molecule_list* d_prot_lst[CELLS];
molecule_list* h_prot_lst[CELLS];
site_list* dom_lst[CELLS];

site* prova_dom_lst[CELLS];

*/

//SOLUTION 3: structure with state vector equal for each cell
//in DM at the beginning the state vector is scrolled and for each prot in the cell i save the id
//when i compute the propensity ecc i scroll over the only ids found at the prev step (ex. if cellID==site->mol->cell then ...)
//diffusion is very easy




site* site_state;  //potrei usare array di array in modo da dividerli per specie!
molecule* molecule_state;
site* d_site_state;
molecule* d_molecule_state;

// Variables


int* d_cb;
int* d_cun;
int species; //number of species
float tm=0.0050; //max time of the simulation
float l=0.1; //lattice size
int cells; //number of subvolume in the grid
float* c; //vector of rates;
float* cu; //vector of unbounding rates;
float* d_c; //device
float* d_cu; //device
float* d_tau; //device (diffusion)
int* d_n; //devicie (diffusion)
int* d_abdS; //
float* deb; //array used for debug
float* d_deb; //device debug
float* d_degree;
float* d_ts; //device time
float* d_tot_propensity;
int* d_max_complex;
int* v; //particle used in reaction, example: v[i][j]==-1 the reaction i consumed 1 particle j;
int* d_v; //device
//site_list** speciesAbd; //corresponds to internal state of a LP, on CPU
//site_list** d_speciesAbd; //corresponds to internal state of a LP, on Device
//site_list** h_speciesAbd; //corresponds to internal state of a LP, on Host
double** rates; //reaction rates
bool displaystep=false; //print intermediate steps in a file
char* inputfile; //sbml file
int counter[CELLS][PROT_SPECIES];
__device__ int complex_count=0;
__device__ int mutex;
//__device__ float a[CELLS][REACTIONS+MOLECULES]; //10 is the reaction
__device__ float a[CELLS][REACTIONS*2]; //10 is the reaction
//__shared__ float a[THREADS][REACTIONS];
//__device__ unsigned short int memo[MOLECULES][NUMSITE]; //used for memoization it is too large
__device__ unsigned short memo[CELLS][MOLECULES][2]; //used for memoization, it is too large
//__device__ int memDecomplex[CELLS][MOLECULES];


// Functions
void Cleanup(void);
void writeState(int, float );
__device__ float Exponential(float, float);
__device__ float h(site*, molecule*, float, const int*, int, int , const int*);
//__device__ float hu(site*, molecule*, const float*, const int*, int, int , int, const int*);
__device__ float hu(site*, molecule*, float, const int*, int, int , const int*);
__device__ int CountSiteofSpec(site*, int, int, const int*, int);
__device__ int CountSiteofSpec2(site*, int, int, const int*, int);
__device__ int CountSiteofSpec_u(site*, int, int, int, const int*);
__device__ void chooseSite_u(int, int, int*, int*, float, site*, int, const int*);
__device__ int Weights(float*, float);
__device__ void updateComplex(molecule*, int, int , int );
__device__ void chooseSite(int, int, int*, int*, float, site*, int, const int*);
__device__ void updateMol(molecule*, site*, int, int, int);
__device__ void autocomplexation(site*, molecule*, int, const int*, int, int, int, curandState*);
__device__ void updateC(molecule*, int, site*);
__shared__ float a0[THREADS];
__shared__ short mu[THREADS];

__shared__ unsigned short m_inv1[THREADS], m_inv2[THREADS];
void ParseArguments(int, char**);

// Device code



__global__ void CloseSim(int* s_max_complex, site* s_site_state, molecule* s_molecule_state, int tot_molecule, int tot_site) {
	int count=0;
	//for (int j=0; j<tot_site; j++) {
		//if (s_site_state[j].bound->bound!=&s_site_state[j]) CUPRINTF("Error during simulation - %d\n", j);
		//if (s_site_state[j].bound!=NULL) count++;
	//}
	//if (count%2!=0) CUPRINTF("Error during simulation!\n");
	*s_max_complex=complex_count;

	//CUPRINTF("complexes %d\n", *s_max_complex);
}

__global__ void ComplexDegree(float* s_degree, site* s_site_state, molecule* s_molecule_state, int tot_molecule, int tot_site) {
	//int s_degree[complex_count];
	int tt;
	for (int i=0; i<complex_count; i++) {
		s_degree[i]=0;
		tt=0;
		for (int j=0; j<tot_molecule; j++) {
			if (s_molecule_state[j].complex==i) {
				tt++;
				for (int k=0; k<tot_site; k++) {
					if (s_site_state[k].mol==j && s_site_state[k].bound!=NULL) s_degree[i]++;
				}
			}
		}
		if (tt>1) s_degree[i]=(s_degree[i]/(tt*(tt-1)));
	}
}
//Direct Method
//__global__ void DM(curandState *state, site* s_domLst, molecule* d_prot_lst,int** s_v, const float* s_c, float tmax, float* s_deb, const int reacts) {

__global__ void DM(curandState *state, site* s_site_state, molecule* s_molecule_state, const int* v, const float* s_c, const float* s_cu, const float t_max, const int* s_abdS, int* s_cb, int* s_cun, float* tot_propensity) {
	//int species=10;
	//int reactions=10;
	//__shared__ int v[REACTIONS][SPECIES];
	//CUPRINTF("Value is:\n");
	//float c[10];
	//float tau;
	//__shared__ site abd[THREADS][SPECIES];
	float u;
	short CellID=blockDim.x * blockIdx.x + threadIdx.x;
	
	
	//int debug=0;
	//short count_prop=0;
	//s_deb[4]++;
	//s_speciesAbd[0][0]++;
	curand_init(clock(), 1, 0, &state[CellID]); //seed, sequence, offset, state
	curandState localState = state[CellID];

	//now abd is a shared variable and is 100 times faster than s_speciesAbd
	//if (threadIdx.x==0) {
	//for (int i=0; i<SPECIES; i++) {
	//	abd[threadIdx.x][i]=s_speciesAbd[CellID][i]; 
	//}
	//}
	/*if (threadIdx.x==0) {
		for (int j=0; j<REACTIONS; j++) {
			for (int i=0; i<SPECIES; i++) {
				v[j][i]=s_v[j][i];
			}
		}
	}
	__syncthreads();*/
	float t=0;
	//s_deb[0]=t_max;
	//float t_max=1;
	//CUPRINTF("Value is:%d\n", aaa);
	int s1,s2;
	bool needlock;
	unsigned int tmp_c;
	//bool binding=true;
	//int x=1;
	//int id_old;
	//if (CellID<MOLECULES) x=MOLECULES/CellID;
/*
	for (int j=CellID*x; j<MOLECULES && j<(CellID+1)*x; j++) {
		for (int k=0; k<NUMSITE; k++) {
			memo[j][k]=CountSiteofSpec(s_site_state, k, CellID, tot_site, j);
			//if (memo[j][k]>0) 
			//CUPRINTF("ok\n");
		}
	}
*/
	//int totu=0;
	mu[threadIdx.x]=-2;
	while (t<t_max) {
		a0[threadIdx.x]=0;
		//binding action propensity
		if (mu[threadIdx.x]>=REACTIONS) mu[threadIdx.x]-=REACTIONS;
		for (int j=0; j<REACTIONS*2; j+=2) {
			//if (mu[threadIdx.x]<REACTIONS && (mu[threadIdx.x]==-2 || mu[threadIdx.x]==(j/2))) {
			if (mu[threadIdx.x]==-2 || ( v[mu[threadIdx.x]*2]==v[j] || v[mu[threadIdx.x]*2+1]==v[j] || v[mu[threadIdx.x]*2]==v[j+1] || v[mu[threadIdx.x]*2+1]==v[j+1])) {
				//mu[threadIdx.x]==(j/2) || mu[threadIdx.x]==(j/2+REACTIONS)) {
					//a0[threadIdx.x]+=a[CellID][j/2]=h(s_site_state, s_molecule_state, s_c[j/2], v, j/2, CellID, s_abdS);
					a0[threadIdx.x]+=a[CellID][j/2]=h(s_site_state, s_molecule_state, s_c[j/2], v, j/2, CellID, s_abdS);
					//a0[threadIdx.x]+=a[CellID][j/2];
					
						
			}
			//a[CellID][j/2]=0;
			//a0[CellID]+=a[CellID][j/2];
		}
		//unbinding action propensity
		//float temp=a0[CellID];
		/*for (int j=0; j<MOLECULES; j++) {
			if (s_molecule_state[j].cell==CellID) {
				if (mu==-1 || (mu<REACTIONS && (j==m_inv1 || j==m_inv2))) {*/
		//int debug=a0[threadIdx.x];
		__syncthreads();
		if (t==0) {
			if (threadIdx.x==0) {
				for (int j=0; j<blockDim.x; j++) 
					tot_propensity[blockIdx.x]+=a0[j]; //debug
			}
		}

		for (int j=0; j<REACTIONS*2; j+=2) {
			//if (mu[threadIdx.x]==-2 || mu[threadIdx.x]==(j/2) || mu[threadIdx.x]==(j/2+REACTIONS)) {
			if (mu[threadIdx.x]==-2 || ( v[mu[threadIdx.x]*2]==v[j] || v[mu[threadIdx.x]*2+1]==v[j] || v[mu[threadIdx.x]*2]==v[j+1] || v[mu[threadIdx.x]*2+1]==v[j+1])) {
				a0[threadIdx.x]+=a[CellID][j/2+REACTIONS]=hu(s_site_state, s_molecule_state, s_cu[j/2], v, j/2, CellID, s_abdS);
				
					//a[CellID][j+REACTIONS]=hu(s_site_state, s_molecule_state, s_cu, v, j, CellID, tot_site, tot_molecule, s_abdS);
			}
		}

		
		__syncthreads();
		if (t==0) {
			if (threadIdx.x==0) {
				float tmp=tot_propensity[blockIdx.x];
				tot_propensity[blockIdx.x]=0;
				for (int j=0; j<blockDim.x; j++) 
					tot_propensity[blockIdx.x]+=a0[j]; //debug
				tot_propensity[blockIdx.x]-=tmp;
			}
		}
				/*
					//if (a[CellID][j+REACTIONS]>0) CUPRINTF("ma entro?!%f\n", a[CellID][j+REACTIONS]);
					count_prop++;
					
				}
			}
			else
				a[CellID][j+REACTIONS]=0;
			a0+=a[CellID][j+REACTIONS];
		}*/
		//tau=Exponential(a0[CellID],u);
		if (a0[threadIdx.x]==0) break;
		u=curand_uniform_double(&localState);
		mu[threadIdx.x]=Weights(a[CellID],a0[threadIdx.x]*u);
		
		//if (a0[threadIdx.x]>0) CUPRINTF("� maggiore %d %f %f\n", mu[threadIdx.x], a0[threadIdx.x],a0[threadIdx.x]*u);
		//CUPRINTF("mu %d\n", mu);
		//if (a[CellID][1]) CUPRINTF("a1 %d\n", a[CellID][1]);
		
		if (mu[threadIdx.x]>=REACTIONS) {
			//collision
			//CUPRINTF("decomplex\n");
			atomicAdd(s_cun,1);

			//int 
			s1=0;
			//int 
			s2=0;
			u=curand_uniform_double(&localState);
			chooseSite_u(v[(mu[threadIdx.x]-REACTIONS)*2], v[(mu[threadIdx.x]-REACTIONS)*2+1], &s1, &s2, u, s_site_state, CellID, s_abdS);
			//chooseSite_u(v[mu[threadIdx.x]*2], v[mu[threadIdx.x]*2+1], &s1, &s2, u, s_site_state, CellID, s_abdS);
			
			s_site_state[s1].bound=NULL;
			s_site_state[s2].bound=NULL;
			
			
			bool bounded1=false;
			bool bounded2=false;
			for (int s=0; s<SPECIES && (!bounded1||!bounded2); s++) {
				if (s_site_state[s].mol==s_site_state[s1].mol && s_site_state[s].bound!=NULL) bounded1=true;
				if (s_site_state[s].mol==s_site_state[s2].mol && s_site_state[s].bound!=NULL) bounded2=true;
			}
			int complex=0;
			needlock=true;
			for (int m=0; m<MOLECULES; m++) {
				if (s_molecule_state[m].id==s_site_state[s1].mol || s_molecule_state[m].id==s_site_state[s2].mol) {
					//if (pDataArray1->molecule_state[m].degree==0) printf("error unbinding impossible: reaction %d, mol %d\n", mu, m);
					s_molecule_state[m].degree--;
				}
			}
			if (bounded1) {
				for (int m=0; m<MOLECULES; m++) {
					if (s_molecule_state[m].id==s_site_state[s1].mol) {
						complex=s_molecule_state[m].complex;
					}
				}
				while (needlock) {
				  if (0==atomicCAS(&mutex, 0, 1)) {
					 // lock
						atomicAdd(&complex_count,1);
						tmp_c=complex_count;
					 // release lock 
					 atomicExch(&mutex, 0);
					 needlock=false;
				   }
				}
				for (int m=0; m<MOLECULES; m++) {
					if (s_molecule_state[m].complex==complex) {
						s_molecule_state[m].complex=tmp_c;
					}
				}
			}
			else {
				
				for (int m=0; m<MOLECULES; m++) {
					if (s_molecule_state[m].id==s_site_state[s1].mol) {
						s_molecule_state[m].complex=0;
					}
				}
			}
			if (bounded2) {
				for (int m=0; m<MOLECULES; m++) {
					if (s_molecule_state[m].id==s_site_state[s2].mol) {
						complex=s_molecule_state[m].complex;
					}
				}
				while (needlock) {
				  if (0==atomicCAS(&mutex, 0, 1)) {
					 // lock
						atomicAdd(&complex_count,1);
						tmp_c=complex_count;
					 // release lock 
					 atomicExch(&mutex, 0);
					 needlock=false;
				   }
				}
				for (int m=0; m<MOLECULES; m++) {
					if (s_molecule_state[m].complex==complex) {
						s_molecule_state[m].complex=tmp_c;
					}
				}
			}
			else {
				
				for (int m=0; m<MOLECULES; m++) {
					if (s_molecule_state[m].id==s_site_state[s2].mol) {
						s_molecule_state[m].complex=0;
					}
				}
			}


			/* - modo semplice -
			*	elimino bound
			*	salvo id complesso
			*	tolgo la proteina dal complesso (metto compl=0)
			*	take new complex id
			*	for each protein where compl=id_complesso_old
			*		set it to new_complex_id
			*		autocomplexation
			*/
			/*
			mu-=REACTIONS;
			totu=0;
			int newc;
			site* s1;
			int countD=0;
			
			for (int i=0; i<tot_site; i++) {
				if (s_site_state[i].mol==mu) {
					if (s_site_state[i].bound!=NULL) {
						s_site_state[i].bound->bound=NULL;
						memDecomplex[CellID][countD]=s_site_state[i].bound->mol;
						countD++;
						s_site_state[i].bound=NULL;
					}
				}
			}*/
			/*memDecomplex[CellID][countD]=-1;
			int iter=0;
			id_old=s_molecule_state[mu].complex;
			s_molecule_state[mu].complex=0;
			int j;
			while (memDecomplex[CellID][iter]!=-1) {
				j=memDecomplex[CellID][iter];
				updateComplex(s_molecule_state, s_molecule_state[j].complex, CellID, tot_molecule, id_old);
				iter++;
			}
*/
			/*
			
			//ABSOLUTLY NOT EFFICIENT!!!
			
			id_old=s_molecule_state[mu].complex;
			s_molecule_state[mu].complex=0;
			
			needlock=true;
			while (needlock) {
			  if (0==atomicCAS(&mutex, 0, 1)) {
				 // lock
					atomicAdd(&complex_count,1);
					tmp_c=complex_count;
				 // release lock 
				 atomicExch(&mutex, 0);
				 needlock=false;
			   }
			}

			for (int j=0; j<tot_molecule; j++) {
				if (s_molecule_state[j].complex==id_old) {
					s_molecule_state[j].complex=tmp_c;
					updateComplex(s_molecule_state, s_molecule_state[j].complex, CellID, tot_molecule, tmp_c);
				}
			}*/
		}
		else {
			
			atomicAdd(s_cb,1);
			//complexation action
			//CUPRINTF("complexation\n");
			s1=0;
			s2=0;
			u=curand_uniform_double(&localState);
			chooseSite(v[mu[threadIdx.x]*2], v[mu[threadIdx.x]*2+1], &s1, &s2, u, s_site_state, CellID, s_abdS);
			s_site_state[s1].bound=&s_site_state[s2];
			s_site_state[s2].bound=&s_site_state[s1];
			needlock=true;
			while (needlock) {
			  if (0==atomicCAS(&mutex, 0, 1)) {
				 // lock
					atomicAdd(&complex_count,1);
					tmp_c=complex_count;
				 // release lock 
				 atomicExch(&mutex, 0);
				 needlock=false;
			   }
			}
			m_inv1[threadIdx.x]=s_site_state[s1].mol;
			m_inv2[threadIdx.x]=s_site_state[s2].mol;
			for (int m=0, short f=0; m<MOLECULES && f<2; m++) {
				//if (s_molecule_state[m].id==s_site_state[s1].mol || s_molecule_state[m].id==s_site_state[s2].mol) {
				if (s_molecule_state[m].id==m_inv1[threadIdx.x] || s_molecule_state[m].id==m_inv2[threadIdx.x]) {
					f++;
					s_molecule_state[m].degree++;
					if (s_molecule_state[m].complex!=0) updateComplex(s_molecule_state, s_molecule_state[m].complex, CellID, tmp_c);
					else s_molecule_state[m].complex=tmp_c;
				}
			}
			//autocomplexation
			//autocomplexation(s_site_state, s_molecule_state, tmp_c, v, tot_molecule, tot_site, reacts, &localState);
		}
		//t+=tau;
		u=curand_uniform_double(&localState);
		while (u==0.0f) u=curand_uniform_double(&localState); 
		t+=Exponential(a0[threadIdx.x],u);
	}
	//if (debug>1 && t_max==0.4) CUPRINTF("debug %d\n", debug);
		
}


__global__ void Diffusion(curandState *state, site* s_site_state, molecule* s_molecule_state, float* s_tau, int* s_n, float* s_ts) {
//__global__ void Diffusion() {
	//float tau[10]; //10 is the number of species
	//float n[10];
	//CUPRINTF("prova\n");
	float ts=*s_ts;
	
	float res;
	int CellID = blockDim.x * blockIdx.x + threadIdx.x;
	
	curand_init(clock(), 1, 0, &state[CellID]); //seed, sequence, offset, state
	curandState localState = state[CellID];
	

	if (CellID==0) res=s_tau[0]*(s_n[0]+1);
	//CUPRINTF("%d\n", CellID);
	/*__shared__ int diff[BLOCK_W*BLOCK_H][10];
	for (int i=0; i<SPECIES; i++) { //10 is the specie
		if (ts==s_tau[i]*s_n[i]) {
			diff[threadIdx.x][i]=s_speciesAbd[CellID][i];
		}
	}
	//__shared__ int abd[THREADS][SPECIES];
	//s_deb[3]++;
	for (int i=0; i<SPECIES; i++) {
		abd[threadIdx.x][i]=s_speciesAbd[CellID][i]; 
	}*/
	//__syncthreads();
	//int tot=0;
	//
	float u=0;
	
	for (int i=0; i<PROT_SPECIES; i++) { 
		if (ts>=(floor(s_tau[i]*s_n[i]*10000)/10000)) {
			if (CellID==0) {
				//if (ts==0.4) CUPRINTF("i = %d\n", s_n[i]);
				atomicAdd(&s_n[i],1);
				//if (ts==0.4) CUPRINTF("i2 = %d\n", s_n[i]);
			}
			
			for (int p=0; p<MOLECULES; p++) {
				//if (s_molecule_state[p].specie==i && s_molecule_state[p].cell==CellID && !s_molecule_state[p].readyToDiffuse) 
				if (s_molecule_state[p].specie==i && s_molecule_state[p].cell==CellID) {
					u=curand_uniform_double(&localState);

					//if ((blockIdx.x>=GRID_W || threadIdx.x>=BLOCK_W)&&u<0.125) {
					if ((blockIdx.x>=GRID_W || threadIdx.x>=BLOCK_W)&&u<0.250) {
						//diffusion above
						if (threadIdx.x>=BLOCK_W) {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID-BLOCK_W;
							updateMol(s_molecule_state, s_site_state, CellID-BLOCK_W, s_molecule_state[p].id, s_molecule_state[p].complex);
						}
						else {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID-(GRID_W-1)*THREADS-BLOCK_W;
							updateMol(s_molecule_state, s_site_state, CellID-(GRID_W-1)*THREADS-BLOCK_W, s_molecule_state[p].id, s_molecule_state[p].complex);
						}
					}

					//else if ((blockIdx.x<GRID_W*(GRID_H-1) || threadIdx.x<BLOCK_W*(BLOCK_H-1)) && u>=0.125 && u<0.25) {
					else if ((blockIdx.x<GRID_W*(GRID_H-1) || threadIdx.x<BLOCK_W*(BLOCK_H-1)) && u>=0.25 && u<0.5) {
						//diffusion below
						if (threadIdx.x<BLOCK_W*(BLOCK_H-1)) {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID+BLOCK_W;
							updateMol(s_molecule_state, s_site_state, CellID+BLOCK_W, s_molecule_state[p].id, s_molecule_state[p].complex);
						}
						else {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID+(GRID_W-1)*THREADS+BLOCK_W;
							updateMol(s_molecule_state, s_site_state, CellID+(GRID_W-1)*THREADS+BLOCK_W, s_molecule_state[p].id, s_molecule_state[p].complex);
						}
					}

					//else if ((blockIdx.x%GRID_W!=0 || threadIdx.x%BLOCK_W!=0) && u>=0.25 && u<0.375) {
					else if ((blockIdx.x%GRID_W!=0 || threadIdx.x%BLOCK_W!=0) && u>=0.5 && u<0.75) {
						//diffusion left
						if (threadIdx.x%BLOCK_W!=0) {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID-1;
							updateMol(s_molecule_state, s_site_state, CellID-1, s_molecule_state[p].id, s_molecule_state[p].complex);
						}
						else {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID-(BLOCK_H-1)*BLOCK_W-1;
							updateMol(s_molecule_state, s_site_state, CellID-(BLOCK_H-1)*BLOCK_W-1, s_molecule_state[p].id, s_molecule_state[p].complex);
						}
					}

					else if ((blockIdx.x%GRID_W!=GRID_W-1 || threadIdx.x%BLOCK_W!=BLOCK_W-1) && u>=0.75) {
						//diffusion right
						if (threadIdx.x%BLOCK_W!=BLOCK_W-1) {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID+1;
							updateMol(s_molecule_state, s_site_state, CellID+1, s_molecule_state[p].id, s_molecule_state[p].complex);
						}
						else {
							s_molecule_state[p].readyToDiffuse=true;
							//s_molecule_state[p].cell=CellID+(BLOCK_H-1)*BLOCK_W+1;
							updateMol(s_molecule_state, s_site_state, CellID+(BLOCK_H-1)*BLOCK_W+1, s_molecule_state[p].id, s_molecule_state[p].complex);
						}
					}
					//s_molecule_state[p].readyToDiffuse=true;
					//else don't diffuse!
				}
			}


		}
		
		if (CellID==0) {
			if (s_tau[i]*s_n[i]<res) res=s_tau[i]*s_n[i];
			//if (ts==0.4) CUPRINTF("res %f\n",res);
		}

	}
	if (CellID==0) {
		*s_ts=res;
		
			//if (ts==0.4) CUPRINTF("res final %f\n",res);
	}
}



// Host code
int main(int argc, char** argv)
{
	time_t seconds;
	time_t last;
	srand(time(NULL));
	printf("size of site %d\n", sizeof(site));
	int num_devices, device;
	cudaGetDeviceCount(&num_devices);
    int max_multiprocessors = 0, max_device = 0;
    for (device = 0; device < num_devices; device++) {
		  cudaDeviceProp properties;
		  cudaGetDeviceProperties(&properties, device);
		  printf("Device Name:\t%s\n", properties.name );
		  printf("Global memory:\t%d\n", properties.totalGlobalMem );
		  printf("Constant memory:\t%d\n", properties.totalConstMem );
		  printf("Warp size:\t%d\n", properties.warpSize );
    }
	//scanf("%d", &max_device);
    cudaSetDevice(0);
	

    printf("Initializing simulation...\n");
    ParseArguments(argc, argv);
	inputfile="Vacuole_noPFX.xml"; //GO_peroxisome.xml prot.xml GO_golgi GO_mitochondrion GO_golgi apc golgi_withPFX
	//inputfile="nucleous_noPFX.xml";
	int reacts=0;
	int nsite=0;
	int nmol=0;
	int tot_site=SPECIES;
	int tot_molecule=4;
	nsite=SBML_getNumSpecies(inputfile);
	tot_molecule=SBML_getNumSpeciesTypesTot(inputfile);
	nmol=SBML_getNumSpeciesTypes(inputfile);
	reacts=SBML_getNumReactions(inputfile);

	curandState *devStates;

	// Initialize variables

    size_t sizeParticles = SPECIES * sizeof(site);
	//eq=(eventQueue*)malloc(sizeQueue);
	//totParticles=(int*)malloc(sizeParticles);

	site_state=(site*)malloc(sizeof(site)*tot_site);
	molecule_state=(molecule*)malloc(sizeof(molecule)*tot_molecule);
	
	printf("Site state size: %d B\nMolecule state size %d B\n", sizeof(site)*tot_site, sizeof(molecule)*tot_molecule);
	printf("#Sites: %d\t#Molecules: %d\n", tot_site, tot_molecule);
	//speciesAbd=(site_list**)malloc(sizeof(site_list*)*CELLS);
	//h_speciesAbd=(site_list**)malloc(sizeof(site_list*)*CELLS);
	v=(int*)malloc(sizeof(int)*REACTIONS*2);
	c=(float*)malloc(sizeof(float)*REACTIONS);
	cu=(float*)malloc(sizeof(float)*REACTIONS);
	//deb=(float*)malloc(sizeof(float)*SPECIES);



	for (int j=0; j<reacts*2; j++) {
		v[j]=0;
		j++;
		v[j]=0;
	}
	

	for (int i=0; i<reacts; i++) {
		c[i]=1;
	}


	float* D = (float*)malloc(sizeof(float)*nmol);
	for (int i=0; i<nmol; i++) {
		D[i]=0.00001;
	}

	char** map;
	char** mapS;
	map=(char**)malloc(sizeof(char*)*nmol);
	mapS=(char**)malloc(sizeof(char*)*tot_site); //da cambiare in nsite
	
	if (parser(inputfile, site_state, molecule_state, v, c, cu, D, CELLS, reacts, tot_site, map, mapS, CELLS/2-THREADS*GRID_W/2)<0) exit(0);
	
/*	for (int i=0; i<tot_molecule; i++) {
		printf("Molecule id %d specie %s\n", molecule_state[i].id, map[molecule_state[i].specie]);
	}
	
	for (int j=0; j<tot_site; j++) {
		printf("Site id %d specie %s molecule %d\n", j, mapS[site_state[j].specie], site_state[j].mol);
	}

	for (int j=0; j<reacts; j++) {
		//printf("Reaction %d species %s - %s, binding %f, unbounding %f\n", j, mapS[v[j*2]], mapS[v[j*2+1]], c[j], cu[j]);
		printf("Reaction %d species %d - %d, binding %f, unbounding %f\n", j, v[j*2], v[j*2+1], c[j], cu[j]);
	}

*/

	float t_sim=0;
	float t_end=tm;
	float* ts; //time of the next diffusion event
	ts=(float*)malloc(sizeof(float));
	int* n=(int*)malloc(sizeof(int)*nmol);
	float* tau=(float*)malloc(sizeof(float)*nmol);
	int dim=2; //dimension
	for (int i=0; i<nmol; i++) {
		n[i]=1;
		//D[i]=0.3;
	}
	for (int i=0; i<nmol; i++) {
		tau[i]=l*l/(2*dim*D[i]);
	}
	*ts=tau[0];
	for (int i=0; i<nmol; i++) {
		if (tau[i]<*ts) *ts=tau[i];
	}

	//count species
	int* t_abdS=(int*)malloc(sizeof(int)*(nsite));
	int* abdS=(int*)malloc(sizeof(int)*(nsite+1));
	for (int i=0; i<nsite; i++) {
		t_abdS[i]=0;
		abdS[i]=0;
	}
	abdS[nsite]=0;
	for (int i=0; i<tot_site; i++) {
		t_abdS[site_state[i].specie]++;
	}
	int countS=0;
	int x=0;
	for (x=0; x<nsite; x++) {
		abdS[x]=countS;
		countS+=t_abdS[x];
	}
	abdS[x]=countS;
	//for (int i=0; i<nsite+1; i++) {
	//	printf("%d: %d\n", i, abdS[i]);
	//}
	cutilSafeCall(cudaMalloc((void **)&devStates, CELLS * sizeof(curandState)));
    cutilSafeCall( cudaMalloc((void**)&d_v, sizeof(int)*reacts*2) );
    cutilSafeCall( cudaMalloc((void**)&d_c, sizeof(float)*reacts) );
    cutilSafeCall( cudaMalloc((void**)&d_cu, sizeof(float)*reacts) );
    cutilSafeCall( cudaMalloc((void**)&d_n, sizeof(int)*nmol) );
    cutilSafeCall( cudaMalloc((void**)&d_tau, sizeof(float)*nmol) );
    cutilSafeCall( cudaMalloc((void**)&d_abdS, sizeof(int)*(nsite+1)) );
    cutilSafeCall( cudaMalloc((void**)&d_ts, sizeof(float)) );
    cutilSafeCall( cudaMalloc((void**)&d_max_complex, sizeof(int)) );
	cutilSafeCall( cudaMalloc((void**)&d_site_state, sizeof(site)*tot_site) );
	cutilSafeCall( cudaMalloc((void**)&d_molecule_state, sizeof(molecule)*tot_molecule) );
    cutilSafeCall( cudaMalloc((void**)&d_cun, sizeof(int)) );
	cutilSafeCall( cudaMalloc((void**)&d_cb, sizeof(int)) );
	cutilSafeCall( cudaMalloc((void**)&d_tot_propensity, sizeof(float)*BLOCKS) );

	
	cutilSafeCall( cudaMemcpy(d_site_state, site_state, sizeof(site)*tot_site, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_molecule_state, molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyHostToDevice) );

	cutilSafeCall( cudaMemcpy(d_v, v, sizeof(int)*reacts*2, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_c, c, sizeof(float)*reacts, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_cu, cu, sizeof(float)*reacts, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_tau, tau, sizeof(float)*nmol, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_abdS, abdS, sizeof(int)*(nsite+1), cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_n, n, sizeof(int)*nmol, cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_ts, ts, sizeof(float), cudaMemcpyHostToDevice) );
	
	cudaPrintfInit();

	FILE *pF2;
	pF2 = fopen("resultCOMPLEX.out", "w"); 


	printf("BLOCKS %d\n", BLOCKS);
	printf("Attenzione, la simulazione potrebbe richiedere diverse ore, nelle quali il pc rimarr� inutilizzabile.\nPremere INVIO per continuare\n");
	
    fflush( stdout);
	getchar();
	seconds = time (NULL);
    printf("Performing simulation...\n");
	//size_t shr_mem = sizeof(int)*nspec*reacts;
	writeState(tot_molecule, t_sim);
	int write=0;
	int* max_complex;
	max_complex=(int*)malloc(sizeof(int));
	int* cun;
	cun=(int*)malloc(sizeof(int));
	int* cb;
	cb=(int*)malloc(sizeof(int));
	*cb=0;
	*cun=0;
	float* tot_propensity;
	tot_propensity=(float*)malloc(sizeof(float)*BLOCKS);
	cutilSafeCall( cudaMemcpy(d_cun, cun, sizeof(int), cudaMemcpyHostToDevice) );
	cutilSafeCall( cudaMemcpy(d_cb, cb, sizeof(int), cudaMemcpyHostToDevice) );
	while (t_sim<t_end) {
		last = time (NULL);
		
		for (int i=0; i<BLOCKS; i++) 
			tot_propensity[i]=0;
		cutilSafeCall( cudaMemcpy(d_tot_propensity, tot_propensity, sizeof(float)*BLOCKS, cudaMemcpyHostToDevice) );
		//DM<<<24, 64, shr_mem>>>(devStates, d_speciesAbd, d_v, d_c, *ts-t_sim, d_deb, reacts);
		DM<<<BLOCKS, THREADS>>>(devStates, d_site_state, d_molecule_state, d_v, d_c, d_cu, *ts-t_sim, d_abdS, d_cb, d_cun,d_tot_propensity);
		cutilSafeCall(cudaThreadSynchronize());
		
		///*if (write%10==0) {
			cutilSafeCall(cudaMemcpy(molecule_state, d_molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyDeviceToHost));
			cutilSafeCall( cudaMemcpy(max_complex, d_max_complex, sizeof(int), cudaMemcpyDeviceToHost) );
			cutilSafeCall( cudaMemcpy(cb, d_cb, sizeof(int), cudaMemcpyDeviceToHost) );
			cutilSafeCall( cudaMemcpy(cun, d_cun, sizeof(int), cudaMemcpyDeviceToHost) );
			cutilSafeCall( cudaMemcpy(tot_propensity, d_tot_propensity, sizeof(float)*BLOCKS, cudaMemcpyDeviceToHost) );
		//}*/
		

		printf("DM time: %d seconds\n", time(NULL)-last);
		float deb_prop=0;
		for (int i=0; i<BLOCKS; i++) 
			deb_prop+=tot_propensity[i];
		printf("tot propensity = %f\n", deb_prop);
		last = time (NULL);
		t_sim=*ts;
		//*ts+=0.015;
		printf("diffusion occurs at time: %f\n", *ts);
		Diffusion<<<BLOCKS, THREADS>>>(devStates, d_site_state, d_molecule_state, d_tau, d_n, d_ts);
		/*if (write%10==0) {
			writeState(tot_molecule, t_sim);
			if (write>0) fprintf(pF2, ",");
			else fprintf(pF2, "{{");*/
			int tot_degree=0;
			for (int j=0; j<tot_molecule; j++) {
				tot_degree+=molecule_state[j].degree;
			}
			printf("degree %d, cb = %d, cun = %d, compl = %d\n", tot_degree,*cb,*cun, *max_complex);
/*
		}*/
		write++;
		//Diffusion<<<24, 64>>>();
		cutilSafeCall(cudaThreadSynchronize());
		
		printf("Diff time: %d seconds\n", time(NULL)-last);
		cutilSafeCall( cudaMemcpy(ts, d_ts, sizeof(float), cudaMemcpyDeviceToHost) );
	}


cudaPrintfDisplay(stdout, true);
	cudaPrintfEnd();

    cutilCheckMsg("kernel error");
#ifdef _DEBUG
    cutilSafeCall( cudaThreadSynchronize() );
#endif


	CloseSim<<<1,1>>>(d_max_complex, d_site_state, d_molecule_state, tot_molecule, tot_site);
	
	cutilSafeCall( cudaMemcpy(site_state, d_site_state, sizeof(site)*tot_site, cudaMemcpyDeviceToHost) );
	cutilSafeCall( cudaMemcpy(molecule_state, d_molecule_state, sizeof(molecule)*tot_molecule, cudaMemcpyDeviceToHost) );

    printf("Simulation ended, analyzing output...\n");

	cutilSafeCall( cudaMemcpy(max_complex, d_max_complex, sizeof(int), cudaMemcpyDeviceToHost) );
	//*max_complex=1;
	printf("#complex %d\n", *max_complex);
	printf("Final abundances\n");


	float* degree=(float*)malloc(sizeof(float)*(*max_complex));
	for (int i=0; i<=(*max_complex); i++) {
		degree[i]=0;
	}

    cutilSafeCall( cudaMalloc((void**)&d_degree, sizeof(float)*(*max_complex)) );
	cutilSafeCall( cudaMemcpy(d_degree, degree, sizeof(float)*(*max_complex), cudaMemcpyHostToDevice) );
	ComplexDegree<<<1,1>>>(d_degree, d_site_state, d_molecule_state, tot_molecule, tot_site);
	cutilSafeCall( cudaMemcpy(degree, d_degree, sizeof(float)*(*max_complex), cudaMemcpyDeviceToHost) );
	writeState(tot_molecule, t_sim);

    for (int j=0; j<tot_molecule; j++) {
		//printf("molecule %d: %s %d\n", molecule_state[j].id, map[molecule_state[j].specie], molecule_state[j].complex);
	}
	fprintf(pF2, "}}");
	for (int i=0; i<=*max_complex; i++) {
		fprintf(pF2, "\n#%d\n", i);
		for (int j=0; j<tot_molecule; j++) {
			if (molecule_state[j].complex==i) {
				fprintf(pF2, "%s - %d\n", map[molecule_state[j].specie], molecule_state[j].degree);
				
			}
		}
		fprintf(pF2, "%f\n", degree[i]);
	}


	for (int i=0; i<tot_molecule; i++) {
		for (int j=0; j<tot_site; j++) {
			if (site_state[j].cell!=molecule_state[i].cell && site_state[j].mol==i) printf("error cell property violated on %s - %s!\t%d - %d\n", map[molecule_state[i].specie], mapS[site_state[j].specie], molecule_state[i].cell, site_state[j].cell);
		}
	}

	printf("\n");

	for (int j=0; j<tot_site; j++) {
		//printf("site %d: %s %d %d %d\n", j, mapS[site_state[j].specie], site_state[j].mol, (site_state[j].bound!=NULL?1:0), site_state[j].cell);
	}
	
	printf("Execution time: %d seconds\n", time(NULL)-seconds);
	fprintf(pF2, "Execution time: %d seconds\n", time(NULL)-seconds);
	fflush(pF2);
	fclose(pF2);
    Cleanup();
}

void Cleanup(void)
{
    // Free device memory
   /* if (d_speciesAbd)
        cudaFree(d_speciesAbd);

    if (d_c)
        cudaFree(d_c);

    if (d_v)
        cudaFree(d_v);
    // Free host memory
	
	for (int i=0; i<CELLS; i++) {
		if (h_speciesAbd[i])
			free(h_speciesAbd[i]);
	}
	
	for (int i=0; i<REACTIONS; i++) {
		if (h_v[i]) free(h_v[i]);
	}*/

    cutilSafeCall( cudaThreadExit() );
    

    fflush( stdout);
    printf("\nPress ENTER to exit...\n");
    fflush( stdout);
    fflush( stderr);
    getchar();

    exit(0);
}

__device__ float Exponential(float lambda, float u) {
	return ((-1)/lambda)*logf(u);
}

//__device__ int Weights(float* a, int CellID) {
__device__ int Weights(float* a, float n) {
	//int mu;
	float a1;
		//,a2;
	//for (mu=0; mu<10; mu++) {
		a1=0;
		//for (int j=0; j<REACTIONS+MOLECULES; j++) {
		for (int j=0; j<REACTIONS*2; j++) {
			if (j==0) a1=0;
			else a1+=a[j-1];
			//if (a1<=a0[threadIdx.x]) {
			if (a1<=n) {
				//a2=a1+a[j];
				//for (int j=0; j<mu; j++) a2+=a[j];
				//if (a0[CellID]<a2) {
				//if (a0[threadIdx.x]<a1+a[j]) {
				if (n<a1+a[j]) {
					//printf ("a1:%f, a0:%f, a2:%f\nmu:%d, a[mu]:%f\n", a1, a0, a2, j, a[j]);
					return j;
				}
			}
		}
	//}
	//no more reactions possible!
	return SPECIES-1;
}

__device__ float h(site* s_site_state, molecule* s_molecule_state, float c, const int* v, int i, int CellID, const int* abdS) {
	/*
	dimensione accettabile REACTIONS*2*CELLS 
	memo[MOLECULES][NUMSITE]; fatto all'inizio 

	*/
	//int order=0;
	float p=0; //propensity
	//int tmp=0;
	//errore dopo la prima reazione rimane a 0!!!
	//int deb=0;
	//int deb2=0;
	//bool debug=false;
	//if (i==1 && CellID==578) CUPRINTF("debug: %d %d\n", v[i*2], v[i*2+1]);
	//int uno=0;
	//int due=0;
	/*for (int j=0; j<MOLECULES; j++) {
		if (s_molecule_state[j].cell==CellID) {
			memo[CellID][j][0]=CountSiteofSpec(s_site_state, v[i*2], CellID, tot_site, j, abdS);
			//if (i==1 && memo[CellID][j][0]>0) uno++;
			memo[CellID][j][1]=CountSiteofSpec(s_site_state, v[i*2+1], CellID, tot_site, j, abdS);
			//if (i==1 && memo[CellID][j][1]>0) due++;
		} else {
			memo[CellID][j][0]=0;
			memo[CellID][j][1]=0;
		}
	}*/

	
	for (int j=0; j<MOLECULES; j++) {
		if (s_molecule_state[j].cell==CellID) memo[CellID][j][0]=0;
		//memo[CellID][j][1]=0;
	}

	unsigned int tmp1=CountSiteofSpec(s_site_state, v[i*2], CellID, abdS, 0);
	
		
	/*for (int j=0; j<MOLECULES; j++) {
		//tmp1+=memo[CellID][k][0];
		tmp2+=memo[CellID][j][1];
	}*/
	
	if (tmp1!=0) {
		for (int j=0; j<MOLECULES; j++) {
			memo[CellID][j][1]=0;
		}
		unsigned int tmp2=CountSiteofSpec2(s_site_state, v[i*2+1], CellID, abdS, 1);
		if (tmp2!=0) {
			for (int j=0; j<MOLECULES; j++) {
					//if (s_molecule_state[j].cell==CellID && s_molecule_state[j].degree<6) {
				if (memo[CellID][j][0]!=0) {
					if (s_molecule_state[j].cell==CellID) {
						p+=memo[CellID][j][0]*(tmp2-memo[CellID][j][1]);
						//p+=__mul24(memo[CellID][j][0],(tmp2-memo[CellID][j][1]));
					}
				}
			}
		}
	}

	/*for (int j=0; j<MOLECULES; j++) {
		tmp=0;
		if (s_molecule_state[j].cell==CellID) {
			for (int k=0; k<MOLECULES; k++) {
				if (j!=k && s_molecule_state[k].cell==CellID) { tmp+=memo[CellID][k][1];}
				//if (j!=k) { tmp+=memo[k][v[i*2]];}
			}
			p+=tmp*memo[CellID][j][0];
		}
		//p+=tmp*memo[j][v[i*2+1]];
	}*/
	

	if (p<=0) p=0;
	else p=p*c;
	return p;
}


//__device__ float hu(site* s_site_state, molecule* s_molecule_state, const float* c, const int* v, int i, int CellID, const int* abdS) {
__device__ float hu(site* s_site_state, molecule* s_molecule_state, float c, const int* v, int i, int CellID, const int* abdS) {
	/*
	float p=1;
	bool found;
	bool never=true;
	unsigned short int k;
	
	for (int j=0; j<SPECIES; j++) {
		if (s_site_state[j].mol==i && s_site_state[j].bound!=NULL) {
			//CUPRINTF("debug2!\n");
			never=false;
			found=false;
			for (k=0; k<REACTIONS*2 && !found; k+=2) {
				if (v[k]==s_site_state[j].specie && v[k+1]==s_site_state[j].bound->specie) found=true;
			}
			p*=c[k];
		}
	}
	if (never) {
		p=0;
		//CUPRINTF("debug!\n");
	}
	
	return p*10;*/

	float p=0; //propensity
	bool debug=false;

	for (int  j=abdS[v[i*2]]; j<abdS[v[i*2]+1]; j++) {
		if (s_site_state[j].cell==CellID && s_site_state[j].bound!=NULL) {
			if (s_site_state[j].bound->specie==v[i*2+1]) p++;
		}
	}

	if (p<=0) p=0;
	else p=p*c;
	return p*0.0001; 
	//return 0;
}

__device__ int CountSiteofSpec(site* s_site_state, int i, int CellID, const int* abdS, int uno) {
	//int order=0;
	//int count=0; //propensity
	/*//site* lst = domLst;
	//if (abdS[i]>1500 || abdS[i+1]>1500) CUPRINTF("%d\n", abdS[i+1]);
	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
	//for (int k=0; k<tot_site ; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].mol==mol) count++;
	}
	return count;

	for (int j=0; j<MOLECULES; j++) {
		memo[CellID][j][uno]=0;
	}
	*/

	unsigned int count=0; 
	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].cell==CellID) {
			memo[CellID][s_site_state[k].mol][uno]++;
			count++;
		}
	}
	return count;
}
__device__ int CountSiteofSpec2(site* s_site_state, int i, int CellID,const int* abdS, int uno) {
	//int order=0;
	unsigned int count=0; //propensity
	/*//site* lst = domLst;
	//if (abdS[i]>1500 || abdS[i+1]>1500) CUPRINTF("%d\n", abdS[i+1]);
	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
	//for (int k=0; k<tot_site ; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].mol==mol) count++;
	}
	return count;
*/

	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].cell==CellID) {
			memo[CellID][s_site_state[k].mol][uno]++;
			count++;
		}
	}
	return count;
}

__device__ int CountSiteofSpec_u(site* s_site_state, int i, int CellID, int mol, const int* abdS) {
	//int order=0;
	int count=0; //propensity
	//site* lst = domLst;
	for (int k=abdS[i]; k<abdS[i+1] ; k++) {
		if (s_site_state[k].cell==CellID && s_site_state[k].specie==i && s_site_state[k].bound==NULL && s_site_state[k].mol==mol) count++;
	}
	return count;
}

__device__ void chooseSite(int type1, int type2, int* s1, int* s2, float u, site* s_site_state, int CellID, const int* abdS) {
	unsigned int tot_s1=0;
	unsigned int tot_s2=0;
	//non conto che devo essere nella stessa cella!!!
	for (int k=abdS[type1]; k<abdS[type1+1]; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].cell==CellID) tot_s1++;
	}

	//if (tot_s1==0) CUPRINTF("error! %d - %d\n", type1, type2); 
	unsigned int ts1=(tot_s1-1)*u;
	//int tmp=tot_s1;
	tot_s1=0;
	//bool debug=false;
	int first_mol;
	for (int k=abdS[type1]; k<abdS[type1+1]; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].cell==CellID) {
			//debug=true;
			if (tot_s1==ts1) { 
				*s1=k; 
				first_mol=s_site_state[k].mol;
			}
			tot_s1++;
		}
	}
	//if (!debug) CUPRINTF("error: u=%f, ts1=%f, tot_s1 %d\n", u, ts1, tmp); //perch� tot_s1, ora tmp, � a 0 e la propensit� non � andata a 0 facendo scegliere questa reazione?!?!?
	
	for (int k=abdS[type2]; k<abdS[type2+1]; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].mol!=first_mol && s_site_state[k].cell==CellID) tot_s2++;
	}
	//tmp=tot_s2;
	//int ts2=(tot_s2-1)*u;
	ts1=(tot_s2-1)*u;
	tot_s2=0;
	for (int k=abdS[type2]; k<abdS[type2+1]; k++) {
		if (s_site_state[k].bound==NULL && s_site_state[k].mol!=first_mol && s_site_state[k].cell==CellID) {
			if (tot_s2==ts1) *s2=k;
			tot_s2++;
		}
	}
}


__device__ void chooseSite_u(int type1, int type2, int* s1, int* s2, float u, site* s_site_state, int CellID, const int* abdS) {
	int tot=0;
	int tot_s2=0;
	int tmp1=0;
	int tmp2=0;
	
	int k,j;
	for (k=abdS[type1]; k<abdS[type1+1]; k++) {
		if (s_site_state[k].cell==CellID && s_site_state[k].bound!=NULL) {
			if (s_site_state[k].bound->specie==type2) tot++;
		}
	}
	float r=u*tot;
	tot=0;
	int flag=1;
	for (k=abdS[type1]; k<abdS[type1+1]; k++) {
		if (s_site_state[k].cell==CellID && s_site_state[k].bound!=NULL) {
			if (s_site_state[k].bound->specie==type2) {
				
				tot++;
				if (tot>=r && flag==1) {
					tmp1=k;
					for (j=abdS[type2]; j<abdS[type2+1]; j++) {
						if(s_site_state[k].bound==&s_site_state[j]) {
							tmp2=j;
							flag=0;
						}
					}
					
					
				}
		
			}
		}
	}
	*s1=tmp1;
	*s2=tmp2;
}

__device__ void updateComplex(molecule* s_molecule_state, int c, int cellID,int newc) {
	//int order=0;

	for (int k=0; k<MOLECULES ; k++) {
		if (s_molecule_state[k].complex==c) s_molecule_state[k].complex=newc;
	}
	//while (lst!=NULL) {
		//if (lst->s.specie==i && lst->s.bound==false) count++;
		//lst=lst->next;
		//if (lst!=NULL) if (lst->s.specie==i && lst->s.bound==false) count++;
	//}
}


__device__ void updateMol(molecule* s_molecule_state, site* s_site_state, int cell, int id, int compl) {
	/*bool diffuse=true;
	for (int i=0; i<MOLECULES && diffuse; i++) {
		if (s_molecule_state[i].complex==compl && s_molecule_state[i].id!=id) diffuse=s_molecule_state[i].readyToDiffuse;
	}
	if (diffuse) {
		for (int i=0; i<MOLECULES; i++) {
			if (s_molecule_state[i].complex==compl) {
				for (int k=0; k<SPECIES ; k++) {
					if (s_site_state[k].mol==s_molecule_state[i].id) s_site_state[k].cell=cell;
				}
				s_molecule_state[i].cell=cell;
			}
		}
	}*/
	bool diffuse=true;
	
	if (compl!=0) {
		for (int i=0; i<MOLECULES && diffuse; i++) {
			if (s_molecule_state[i].complex==compl && s_molecule_state[i].id!=id) diffuse=s_molecule_state[i].readyToDiffuse;
		}

		if (diffuse) {
			for (int i=0; i<MOLECULES; i++) {
				if (s_molecule_state[i].complex==compl) {
					for (int k=0; k<SPECIES ; k++) {
						if (s_site_state[k].mol==s_molecule_state[i].id) s_site_state[k].cell=cell;
					}
					s_molecule_state[i].cell=cell;
				}
			}
		}	
	}
	else {
		for (int i=0; i<MOLECULES; i++) {
			if (s_molecule_state[i].id==id) {
				for (int k=0; k<SPECIES ; k++) {
					if (s_site_state[k].mol==s_molecule_state[i].id) s_site_state[k].cell=cell;
				}
				s_molecule_state[i].cell=cell;
			}
		}
	}
}

__device__ void updateC(molecule* s_molecule_state, int compl, site* s1) {
	
/*	site* s;
	for (int i=0; i<tot_m; i++) {
		if (s_molecule_state[i].compl==compl) {
			for (int j=0; j<s_site_state; j++) {
				if (s_site_state[j].mol==i) {
					s=s_site_state[j];
					while (s->bound!=NULL) {
						s=s->bound;
						s_molecule_state[s->mol]=compl;	
					}
				}
			}
		}
	}
	*/
	/*site* s = s1;
	while (s->bound!=NULL) {
		s=s->bound;
		s_molecule_state[s->mol]=compl;
		
	}*/
}

__device__ void autocomplexation(site* s_site_state, molecule* s_molecule_state, int compl, const int* v, int tot_m, int tot_s, int reacts, curandState* localState) {
	bool next;
	float u;
	//manca da controllare che il site non sia bound
	for (int i=0; i<tot_m; i++) {
		if (s_molecule_state[i].complex==compl) {
			for (int k=0; k<tot_s; k++) {
				if (s_site_state[k].mol==s_molecule_state[i].id && s_site_state[k].bound==NULL) {
					for (int l=0; l<reacts*2; l++) {
						if(s_site_state[k].specie==v[l]) {
							if (l%2==0) next=true;
							else next=false;
							for (int j=0; j<tot_m; j++) {  //allora cerco la seconda specie in una molecola diversa
								if (i!=j && s_molecule_state[j].complex==compl) {
									for (int m=0; m<tot_s; m++) {
										if (s_site_state[m].mol==s_molecule_state[j].id && s_site_state[m].bound==NULL && ((s_site_state[m].specie==v[l+1] && next) || (s_site_state[m].specie==v[l-1] && !next))) {
											u=curand_uniform_double(localState);
											if (u>0.1) {
												s_site_state[m].bound=&s_site_state[k];
												s_site_state[k].bound=&s_site_state[m];
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

// Parse program arguments
void ParseArguments(int argc, char** argv)
{
	for (int i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "--displaystep") == 0 ||
			strcmp(argv[i], "-dispalystep") == 0) 
		{
            displaystep = true;
        }
        if (strcmp(argv[i], "-t") == 0) 
		{
			if (isdigit(atoi(argv[i+1]))) 
            tm = atoi(argv[i+1]);
        }
        if (strcmp(argv[i], "-l") == 0) 
		{
			if (isdigit(atoi(argv[i+1]))) 
            l = atoi(argv[i+1]);
        }
        if (strcmp(argv[i], "-f") == 0) 
		{
			if (strcmp(argv[i+1], "")!=0) 
            inputfile = argv[i+1];
        }
        if (strcmp(argv[i], "-h") == 0) 
		{
			printf("Welcome to GPU Gillespie Multi-Particle\n usage:\n--displaystep\tshow current state after each diffusion\n-t\tmaximum simulation time\n-l\tlattice size\n-f\tinput file containing reactions and initial quantities\n-h\thelp"); 
			exit(0);
        }
	}
}


void writeState(int tot_molecule, float ts) {
	FILE *pFile;
	FILE *pFile2;
	if (ts==0.0) {
		pFile = fopen("Trajectory_Molecule.out", "w"); 
	}
	else {
		pFile = fopen("Trajectory_Molecule.out", "a");
	}
	int debugType[PROT_SPECIES];
	for (int i=0; i<CELLS; i++) { 
		for (int j=0; j<PROT_SPECIES; j++) {
			counter[i][j]=0;
			debugType[j]=0;
		}
	}
	for (int i=0; i<tot_molecule; i++) {
			counter[molecule_state[i].cell][molecule_state[i].specie]++;
			debugType[molecule_state[i].specie]++;
	}
	fprintf(pFile,"Abundances at time: %f\n", ts);
	for (int j=0; j<PROT_SPECIES; j++) {
		//if (ts>=tm) printf("Specie %d: %d\n", j, debugType[j]);
		fprintf(pFile,"Species %d\n", j);
		for (int i=0; i<GRID_H*BLOCK_H; i++) { //row
			for (int k=0; k<GRID_W; k++) { //column
				for (int q=0; q<BLOCK_W; q++) {
					fprintf(pFile, "%d\t", counter[(i*BLOCK_W)%(BLOCK_H*BLOCK_W)+(i/BLOCK_H)*(BLOCK_W*BLOCK_H*GRID_W)+k*THREADS+q][j]);
				}
			}
		} 
		fprintf(pFile,"\n");
	}
	fflush(pFile);
	
	fclose(pFile);
}
