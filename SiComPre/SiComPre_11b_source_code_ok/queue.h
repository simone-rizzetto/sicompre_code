#ifndef QUEUE_H
#define QUEUE_H

// dichiarazioni per la gestine della coda di interi

#include <iostream>

struct node {
	int val;
	node * next;
};

struct queue 
{
  node * tail;
  node * head;
};


enum retval { FAIL, OK };

void init (queue* q);
void initM (queue* q);
retval enqueue(queue* q, int);
retval dequeue(queue* q, int*);
bool emptyp(queue* q );
void print (queue* q);
retval enqueueM(queue* q, int n);
bool inQueue(queue* q, int n);
void deleteM(queue* q);

#endif




