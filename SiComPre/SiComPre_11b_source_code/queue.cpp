
#include <stdio.h>
#include "queue.h"





//queue Q;
//queue M;


bool emptyp (queue* Q) 
{
  return (Q->head == NULL);
}

bool emptypM (queue* M) 
{
  return (M->head == NULL);
}

void init(queue* Q)
{
  Q->head = NULL;
}

retval enqueue(queue* Q, int n)
{
// memo: "new (nothrow) ..." restituisce NULL
// senza generare eccezioni se l'allocazione non va a buon fine
  //node * np = new (nothrow) node;
  node * np = (node*)malloc(sizeof(node));
  if (np==NULL)
    return FAIL;
  np->val=n;
  np->next=NULL;
  if (emptyp(Q)) 
    Q->head=Q->tail=np;
  else {
    Q->tail->next=np;
    Q->tail=np;
  }
  return OK;
}

retval dequeue(queue* Q, int* n)
{ 
  node * first;
  if (emptyp(Q))
    return FAIL;
  first = Q->head;
  *n = first->val;
  Q->head = Q->head->next;
  delete first; 
  return OK;
}

void print(queue* Q) 
{ 
  if (!emptyp(Q)) {
    node * first=Q->head;
    do {
      //cout << first->val << endl;
      first = first->next;
    } while (first != NULL);
  }
}

void initM(queue* M)
{
  M->head = NULL;
}

retval enqueueM(queue* M, int n)
{
// memo: "new (nothrow) ..." restituisce NULL
// senza generare eccezioni se l'allocazione non va a buon fine
  //node * np = new (nothrow) node;
  node * np = (node*)malloc(sizeof(node));
  if (np==NULL)
    return FAIL;
  np->val=n;
  np->next=NULL;
  if (emptypM(M)) 
    M->head=M->tail=np;
  else {
    M->tail->next=np;
    M->tail=np;
  }
  return OK;
}

bool inQueue(queue* M, int n) {

	node* scroll=M->head;
	while (scroll!=NULL) {
		if (scroll->val==n) return true;
		else scroll=scroll->next;
	}
	return false;
}

void deleteM(queue* M) {

	node* scroll=M->head;
	node* tmp;
	while (scroll!=NULL) {
		tmp=scroll;
		scroll=scroll->next;
	}
}
